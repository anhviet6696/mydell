create database Massage
use Massage
go

create table Service(id int identity(1,1),ServiceName nvarchar(100),content nvarchar(max),price decimal (6,2),time nvarchar(100),picture varchar(max))
go
insert into Service (ServiceName,content,price,time,picture) 
values ('Swedish massage','For this massage, you�ll remove your clothes, though you may choose to keep your underwear on. You�ll be covered with a sheet while lying on the massage table. The massage therapist will move the sheet to uncover areas that they are actively working on',
12.55,'20 minutes','http://cdn.simplesite.com/i/ef/3d/283445307041725935/i283445314524568923._szw1280h1280_.jpg')
insert into Service (ServiceName,content,price,time,picture) 
values ('Hot stone massage','During a hot stone massage, heated stones are placed on different areas around your whole body. Your therapist may hold a stone as they massage different parts of your body using Swedish massage techniques using gentle pressure. Sometimes cold stones are also used',
12.55,'20 minutes','http://cdn.simplesite.com/i/ef/3d/283445307041725935/i283445314491420783._szw1280h1280_.jpg')
insert into Service (ServiceName,content,price,time,picture) 
values ('Aromatherapy massage','Aromatherapy massages combine soft, gentle pressure with the use of essential oils. Your massage therapist will usually decide which essential oils to use, but you can let them know if you have a preference. Essential oils are diluted before being applied to the skin.',
12.55,'20 minutes','http://cdn.simplesite.com/i/ef/3d/283445307041725935/i283445314489630203._szw1280h1280_.jpg')
insert into Service (ServiceName,content,price,time,picture) 
values ('Deep tissue massage','Deep tissue massage uses more pressure than a Swedish massage. It�s a good option if you have chronic muscle problems, such as soreness, injury, or imbalance. It can help relieve tight muscles, chronic muscle pain, and anxiety',
12.55,'20 minutes','http://cdn.simplesite.com/i/ef/3d/283445307041725935/i283445314489630203._szw1280h1280_.jpg')

create table Course(date datetime default(getdate()),courseName nvarchar(100), content nvarchar(Max))
go
insert into Course( courseName,content) values('Introductory Massage',
'This subject is an excellent introduction to the basic concepts of massage including posture, draping, positioning, hygiene and hand techniques. It is a great way of helping you to decide on your dream career - it can really be an inspiration! Or, you may just want to learn something new to practice on family and friends!')
insert into Course( courseName,content) values('Certificate Course',
'We use a unique flexible training system to maximise the value you get from your learning. As well as meeting all government guidelines, our training allows you to learn at your own pace by choosing part-time, full-time or correspondence')
go
create table Contact (tel varchar(11),email varchar(100),content varchar(max),address varchar(100),city varchar(100), country varchar(100),oHours varchar(100))
go
insert into Contact  (tel ,email,content ,address ,city , country ,oHours) values
('123456789','viet@gmail.com','We are committed to your well-being. If you have any comments, questions or concerns feel free to contact us',
'02 Tien Son Da nang','Da Nang','Viet Nam','All time 24/7')
create table Employees (empName varchar(100), position varchar(100), info varchar(max),picture varchar(max))
go

insert into Employees(empName,position, info,picture)
values ('Lee Byung-Hun', 'Masseur and sports masseur','Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.','https://i.pinimg.com/originals/a9/b6/7c/a9b67cf51d7bda1f37caa72574267e31.jpg')
insert into Employees(empName,position, info,picture)
values ('Bi Rain', 'Masseur and sports masseur','Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.','https://2sao.vietnamnetjsc.vn/images/2018/11/29/08/46/birain.jpg')

