package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class Welcome_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_forEach_var_items;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_c_forEach_var_items = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_c_forEach_var_items.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write('\n');
      out.write('\n');
      model.Service service = null;
      synchronized (session) {
        service = (model.Service) _jspx_page_context.getAttribute("service", PageContext.SESSION_SCOPE);
        if (service == null){
          service = new model.Service();
          _jspx_page_context.setAttribute("service", service, PageContext.SESSION_SCOPE);
        }
      }
      out.write('\n');
      out.write('\n');
      model.Contact contact = null;
      synchronized (session) {
        contact = (model.Contact) _jspx_page_context.getAttribute("contact", PageContext.SESSION_SCOPE);
        if (contact == null){
          contact = new model.Contact();
          _jspx_page_context.setAttribute("contact", contact, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\n");
      out.write("\n");
      out.write("\n");
      model.Course course = null;
      synchronized (session) {
        course = (model.Course) _jspx_page_context.getAttribute("course", PageContext.SESSION_SCOPE);
        if (course == null){
          course = new model.Course();
          _jspx_page_context.setAttribute("course", course, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<!-- saved from url=(0038)http://www.simplesite.com/us-123health -->\n");
      out.write("<html lang=\"en-US\" class=\"\"><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Welcome - www.simplesite.com/us-123health</title>\n");
      out.write("        <meta property=\"og:site_name\" content=\"Aromatherapy - Massage\">\n");
      out.write("        <meta property=\"article:publisher\" content=\"https://www.facebook.com/simplesite\">\n");
      out.write("        <meta property=\"og:locale\" content=\"en-US\">\n");
      out.write("        <meta property=\"og:url\" content=\"http://www.simplesite.com/us-123health/\">\n");
      out.write("        <meta property=\"og:title\" content=\"Aromatherapy Means &quot;Treatment Using Scents&quot;\">\n");
      out.write("        <meta property=\"og:description\" content=\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. - Hot Stone Massage - A Var...\">\n");
      out.write("        <meta property=\"og:image\" content=\"http://cdn.simplesite.com/i/ef/3d/283445307041725935/i283445314524568867._szw1280h1280_.jpg\">\n");
      out.write("        <meta property=\"og:updated_time\" content=\"2017-01-04T04:31:20.7519500+00:00\">\n");
      out.write("        <meta property=\"og:type\" content=\"website\">\n");
      out.write("        <meta name=\"robots\" content=\"nofollow\">\n");
      out.write("\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n");
      out.write("        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n");
      out.write("        <meta name=\"description\" content=\"Aromatherapy - Massage - http://www.simplesite.com/us-123health/\">\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"./Welcome_files/8472132.design.v26182.css\">\n");
      out.write("        <link rel=\"canonical\" href=\"http://www.simplesite.com/us-123health/\">\n");
      out.write("        <link rel=\"shortcut icon\" href=\"data:image/x-icon;,\">\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"./Welcome_files/css\">\n");
      out.write("        <script type=\"text/javascript\" async=\"\" src=\"./Welcome_files/analytics.js.download\"></script><script async=\"\" src=\"./Welcome_files/gtm.js.download\"></script><script type=\"text/javascript\" src=\"./Welcome_files/FrontendAppLocalePage.aspx\"></script>\n");
      out.write("        <script type=\"text/javascript\" src=\"./Welcome_files/frontendApp.min.js.download\"></script>\n");
      out.write("        <script type=\"text/javascript\">if (typeof window.jQuery == \"undefined\") {\n");
      out.write("                (function () {\n");
      out.write("                    var a = document.createElement(\"script\");\n");
      out.write("                    a.type = \"text/javascript\";\n");
      out.write("                    a.src = \"/c/js/version3/frontendApp/init/frontendApp.min.js?_v=c3745b49b3583731aea941058c4b63a8\";\n");
      out.write("                    document.getElementsByTagName('head')[0].appendChild(a);\n");
      out.write("                })();\n");
      out.write("            }</script>\n");
      out.write("\n");
      out.write("        <script type=\"text/javascript\">var css_simplesite_com_fallback_element = document.getElementById('css_simplesite_com_fallback');\n");
      out.write("            if (typeof css_simplesite_com_fallback_element !== 'undefined' && css_simplesite_com_fallback_element !== null) {\n");
      out.write("                var isVisible = css_simplesite_com_fallback_element.offsetParent !== null\n");
      out.write("                if (isVisible) {\n");
      out.write("                    var head = document.head, link = document.createElement('link');\n");
      out.write("\n");
      out.write("                    link.type = 'text/css';\n");
      out.write("                    link.rel = 'stylesheet';\n");
      out.write("                    link.href = '{/d/designs/8472132.design.v26182.css}';\n");
      out.write("\n");
      out.write("                    head.appendChild(link);\n");
      out.write("                }\n");
      out.write("            }</script>\n");
      out.write("        <style type=\"text/css\">.fancybox-margin{margin-right:17px;}</style></head>\n");
      out.write("    <body data-pid=\"118896506\" data-iid=\"\">\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("        <div class=\"container-fluid site-wrapper\"> <!-- this is the Sheet -->\n");
      out.write("            <div class=\"container-fluid header-wrapper \" id=\"header\"> <!-- this is the Header Wrapper -->\n");
      out.write("                <div class=\"container\">\n");
      out.write("                    <div class=\"title-wrapper\">\n");
      out.write("                        <div class=\"title-wrapper-inner\">\n");
      out.write("                            <a rel=\"nofollow\" class=\"logo \" href=\"http://www.simplesite.com/us-123health/\">\n");
      out.write("                            </a>\n");
      out.write("                            <div class=\"title \">\n");
      out.write("                                Aromatherapy - Massage\n");
      out.write("                            </div>\n");
      out.write("                            <div class=\"subtitle\">\n");
      out.write("                                Welcome to this website\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>  <!-- these are the titles -->\n");
      out.write("                    <div class=\"navbar navbar-compact\">\n");
      out.write("                        <div class=\"navbar-inner\">\n");
      out.write("                            <div class=\"container\">\n");
      out.write("                                <!-- .btn-navbar is used as the toggle for collapsed navbar content -->\n");
      out.write("                                <a rel=\"nofollow\" class=\"btn btn-navbar\" data-toggle=\"collapse\" data-target=\".nav-collapse\" title=\"Toggle menu\">\n");
      out.write("                                    <span class=\"menu-name\">Menu</span>\n");
      out.write("                                    <span class=\"menu-bars\">\n");
      out.write("                                        <span class=\"icon-bar\"></span>\n");
      out.write("                                        <span class=\"icon-bar\"></span>\n");
      out.write("                                        <span class=\"icon-bar\"></span>\n");
      out.write("                                    </span>\n");
      out.write("                                </a>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("                                <!-- Everything you want hidden at 940px or less, place within here -->\n");
      out.write("                                <div class=\"nav-collapse collapse\">\n");
      out.write("                                    <ul class=\"nav\" id=\"topMenu\" data-submenu=\"horizontal\">\n");
      out.write("                                        <li class=\" active \" style=\"\">\n");
      out.write("                                          <a rel=\"nofollow\" href=\"Welcome.jsp\">Welcome</a>\n");
      out.write("</li><li class=\" active \" style=\"\">\n");
      out.write("    <a rel=\"nofollow\" href=\"FullServices.jsp\">Therapy and Massage</a>\n");
      out.write("</li><li class=\"  \" style=\"\">\n");
      out.write("    <a rel=\"nofollow\" href=\"PriceList.jsp\">Price List</a>\n");
      out.write("</li><li class=\"  \" style=\"\">\n");
      out.write("    <a rel=\"nofollow\" href=\"Employees.jsp\">Employees</a>\n");
      out.write("</li><li class=\"  \" style=\"\">\n");
      out.write("    <a rel=\"nofollow\" href=\"Contact.jsp\">Contact us</a>\n");
      out.write("                                        </li>                </ul>\n");
      out.write("                                </div>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                    <!-- this is the Menu content -->\n");
      out.write("                </div>\n");
      out.write("            </div>  <!-- this is the Header content -->\n");
      out.write("\n");
      out.write("            <div class=\"container-fluid content-wrapper\" id=\"content\"> <!-- this is the Content Wrapper -->\n");
      out.write("                <div class=\"container\">\n");
      out.write("                    <div class=\"row-fluid content-inner\">\n");
      out.write("                        <div id=\"left\" class=\"span9\"> <!-- ADD \"span12\" if no sidebar, or \"span9\" with sidebar -->\n");
      out.write("                            <div class=\"wrapper \">\n");
      out.write("                                <div class=\"content\">\n");
      out.write("                                    <div class=\"section article\">\n");
      out.write("                                        <div class=\"content\">\n");
      out.write("                                            <div class=\"img-simple span12 \">\n");
      out.write("                                                <div class=\"image\">\n");
      out.write("                                                    <a rel=\"nofollow\" data-ss=\"imagemodal\" data-href=\"http://cdn.simplesite.com/i/ef/3d/283445307041725935/i283445314524568867._szw1280h1280_.jpg\"><img src=\"./Welcome_files/i283445314524568867._szw1280h1280_.jpg\"></a>\n");
      out.write("                                                </div>\n");
      out.write("                                            </div>\n");
      out.write("\n");
      out.write("                                        </div>\n");
      out.write("                                    </div>\n");
      out.write("                                    <div class=\"section\">\n");
      out.write("                                        <div class=\"content\">\n");
      out.write("                                            <ul class=\"thumbnails column-article-section\">\n");
      out.write("\n");
      out.write("                                                ");
      if (_jspx_meth_c_forEach_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("                                            </ul>\n");
      out.write("                                        </div>\n");
      out.write("                                    </div>\n");
      out.write("                                    <div class=\"section news\">\n");
      out.write("                                        <style>    .wordwrapfix {\n");
      out.write("                                                word-wrap:break-word;\n");
      out.write("                                            }\n");
      out.write("                                        </style>\n");
      out.write("                                        <div class=\"heading wordwrapfix\">\n");
      out.write("                                            <h3>Courses</h3>\n");
      out.write("                                        </div>\n");
      out.write("                                        ");
      if (_jspx_meth_c_forEach_1(_jspx_page_context))
        return;
      out.write("\n");
      out.write("                                    </div>\n");
      out.write("                                    <div class=\"section article\">\n");
      out.write("                                        <style>    .wordwrapfix {\n");
      out.write("                                                word-wrap:break-word;\n");
      out.write("                                            }\n");
      out.write("                                        </style>\n");
      out.write("                                        <div class=\"heading wordwrapfix\">\n");
      out.write("                                            <h3>Make an appointment</h3>\n");
      out.write("                                        </div>\n");
      out.write("                                        ");
      if (_jspx_meth_c_forEach_2(_jspx_page_context))
        return;
      out.write("\n");
      out.write("                                        </div>\n");
      out.write("                                    </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("                                </div>\n");
      out.write("                            </div>\n");
      out.write("                            <div id=\"right\" class=\"span3\">\n");
      out.write("                                <div class=\"sidebar\">\n");
      out.write("                                    <div class=\"wrapper share-box\">\n");
      out.write("                                        <style>    .wordwrapfix {\n");
      out.write("                                                word-wrap:break-word;\n");
      out.write("                                            }\n");
      out.write("                                        </style>\n");
      out.write("                                        <div class=\"heading wordwrapfix\">\n");
      out.write("                                            <h4>Share this page</h4>\n");
      out.write("                                        </div>\n");
      out.write("\n");
      out.write("                                        <div class=\"content\"><span><ul>\n");
      out.write("                                                    <li><a id=\"share-facebook\" href=\"http://www.simplesite.com/us-123health#\"><i class=\"icon-facebook-sign\"></i><span>Share on Facebook</span></a></li>\n");
      out.write("                                                    <li><a id=\"share-twitter\" href=\"http://www.simplesite.com/us-123health#\"><i class=\"icon-twitter-sign\"></i><span>Share on Twitter</span></a></li>\n");
      out.write("                                                    <li><a id=\"share-google-plus\" href=\"http://www.simplesite.com/us-123health#\"><i class=\"icon-google-plus-sign\"></i><span>Share on Google+</span></a></li>    \n");
      out.write("                                                </ul></span></div>\n");
      out.write("                                    </div>\n");
      out.write("                                </div>\n");
      out.write("                            </div>\n");
      out.write("                        </div>        \n");
      out.write("                    </div>\n");
      out.write("                </div>  <!-- the controller has determined which view to be shown in the content -->\n");
      out.write("\n");
      out.write("                <div class=\"container-fluid footer-wrapper\" id=\"footer\"> <!-- this is the Footer Wrapper -->\n");
      out.write("                    <div class=\"container\">\n");
      out.write("                        <div class=\"footer-info\">\n");
      out.write("                            <div class=\"footer-powered-by\">\n");
      out.write("                                <a rel=\"nofollow\" href=\"http://www.simplesite.com/pages/receive.aspx?partnerkey=123i%3afooterbanner&amp;referercustomerid=3016847&amp;refererpageid=118896506\">Created with SimpleSite</a>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                        <div class=\"footer-page-counter\" style=\"display: block;\">\n");
      out.write("                            <span class=\"footer-page-counter-item\">0</span>\n");
      out.write("\n");
      out.write("                            <span class=\"footer-page-counter-item\">4</span>\n");
      out.write("\n");
      out.write("                            <span class=\"footer-page-counter-item\">1</span>\n");
      out.write("\n");
      out.write("                            <span class=\"footer-page-counter-item\">1</span>\n");
      out.write("\n");
      out.write("                            <span class=\"footer-page-counter-item\">9</span>\n");
      out.write("\n");
      out.write("                            <span class=\"footer-page-counter-item\">9</span>\n");
      out.write("                        </div>\n");
      out.write("                        <div id=\"css_simplesite_com_fallback\" class=\"hide\"></div>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("\n");
      out.write("                <!-- this is the Footer content -->\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("            <input type=\"hidden\" id=\"anti-forgery-token\" value=\"vFZIp11QDd7w1cZjYdzwVVw3rTHgQi1lpMMqomvLq/qGfx85dADIn6rd+jb5+yvdIjnIKNejlFu6vxL7J8Np5e7g5yrI36losWXJkOuv169s22+Z/mKVySX+8iBmNTzgEHnWy/7MRLoBekjH3EpgI99d772eCmdJN7eG7tAqc/m4iWFE2sfSH8oY0Q3pJZcuhjM+blA+FzGTkoYvuddklk1q/4A0667VqYWgFZ7hNc8NO4Kw1Txa422/7pYh/UW67W3GdGTPhdbiuy5yZG8PFfcV3pshO8MZJZI4QqpmoSmbtrQpm9WTX4MbCbnQyM7h5NaUS9oN6X4rtQXeQ4JVwDzgCfuolg6kJOxKEjNy3wz2WXZRbIsB2tTp4Wbn2z/Ln9JWaw1KOYEERYwJ6vPWVaRgC601/8mXvOFAA/mWkb74c7/7dNZnzEC3wG+uUltU5QUKyYPoSFM3dqAQ14muXQ==\">\n");
      out.write("\n");
      out.write("\n");
      out.write("            <script>\n");
      out.write("                dataLayer = [{\"SiteVer\": \"US\", \"MainOrUser\": \"UserPage\", \"PreOrFre\": \"Free\", \"Language\": \"en\", \"Culture\": \"en-US\", \"Instrumentation\": \"ShowPage\", \"Market\": \"DK\"}];\n");
      out.write("            </script>\n");
      out.write("            <!-- Google Tag Manager -->\n");
      out.write("            <noscript>&lt;iframe src=\"//www.googletagmanager.com/ns.html?id=GTM-2MMH\"\n");
      out.write("            height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"&gt;&lt;/iframe&gt;</noscript>\n");
      out.write("            <script>(function (w, d, s, l, i) {\n");
      out.write("                    {\n");
      out.write("                        w[l] = w[l] || [];\n");
      out.write("                        w[l].push({'gtm.start':\n");
      out.write("                                    new Date().getTime(), event: 'gtm.js'});\n");
      out.write("                        var f = d.getElementsByTagName(s)[0],\n");
      out.write("                                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';\n");
      out.write("                        j.async = true;\n");
      out.write("                        j.src =\n");
      out.write("                                '//www.googletagmanager.com/gtm.js?id=' + i + dl;f.parentNode.insertBefore(j, f);\n");
      out.write("                    }\n");
      out.write("    })(window, document, 'script', 'dataLayer', 'GTM-2MMH');</script>\n");
      out.write("            <!-- End Google Tag Manager -->\n");
      out.write("\n");
      out.write("\n");
      out.write("            <!-- Remove after blog exp concludes -->\n");
      out.write("\n");
      out.write("            <div id=\"sm2-container\" class=\"movieContainer high_performance  high_performance\" style=\"z-index: 10000; position: fixed; width: 8px; height: 8px; bottom: 0px; left: 0px; overflow: hidden; visibility: hidden;\"></div><div style=\"display: none; visibility: hidden;\"><script type=\"text/javascript\">var google_conversion_id = 1066686464, google_custom_params = window.google_tag_params, google_remarketing_only = !0;</script>\n");
      out.write("                <script type=\"text/javascript\" src=\"./Welcome_files/conversion.js.download\"></script><iframe name=\"google_conversion_frame\" title=\"Google conversion frame\" width=\"300\" height=\"13\" src=\"./Welcome_files/saved_resource.html\" frameborder=\"0\" marginwidth=\"0\" marginheight=\"0\" vspace=\"0\" hspace=\"0\" allowtransparency=\"true\" scrolling=\"no\"></iframe>\n");
      out.write("                <noscript></noscript>\n");
      out.write("            </div></body></html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_c_forEach_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_0 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_c_forEach_0.setPageContext(_jspx_page_context);
    _jspx_th_c_forEach_0.setParent(null);
    _jspx_th_c_forEach_0.setVar("p");
    _jspx_th_c_forEach_0.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${service.top3Service}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    int[] _jspx_push_body_count_c_forEach_0 = new int[] { 0 };
    try {
      int _jspx_eval_c_forEach_0 = _jspx_th_c_forEach_0.doStartTag();
      if (_jspx_eval_c_forEach_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\n");
          out.write("                                                    <li class=\"span4\">\n");
          out.write("                                                        <div class=\"img-simple span12 \">\n");
          out.write("                                                            <div class=\"image\">\n");
          out.write("                                                                <a rel=\"nofollow\" data-ss=\"imagemodal\" data-href=\"http://cdn.simplesite.com/i/ef/3d/283445307041725935/i283445314489630203._szw1280h1280_.jpg\"><img src=\"");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${p.picture}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("\"></a>\n");
          out.write("                                                            </div>\n");
          out.write("                                                        </div>\n");
          out.write("\n");
          out.write("                                                        <h4>\n");
          out.write("                                                            <a rel=\"nofollow\" href=\"http://www.simplesite.com/us-123health/118896508\">");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${p.serviceName}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</a>\n");
          out.write("                                                        </h4>\n");
          out.write("\n");
          out.write("                                                        <p>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${p.content}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</p>\n");
          out.write("                                                    </li>\n");
          out.write("                                                ");
          int evalDoAfterBody = _jspx_th_c_forEach_0.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_forEach_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_forEach_0[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_forEach_0.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_forEach_0.doFinally();
      _jspx_tagPool_c_forEach_var_items.reuse(_jspx_th_c_forEach_0);
    }
    return false;
  }

  private boolean _jspx_meth_c_forEach_1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_1 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_c_forEach_1.setPageContext(_jspx_page_context);
    _jspx_th_c_forEach_1.setParent(null);
    _jspx_th_c_forEach_1.setVar("pp");
    _jspx_th_c_forEach_1.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${course.allCourse}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    int[] _jspx_push_body_count_c_forEach_1 = new int[] { 0 };
    try {
      int _jspx_eval_c_forEach_1 = _jspx_th_c_forEach_1.doStartTag();
      if (_jspx_eval_c_forEach_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\n");
          out.write("                                            <div class=\"content\">\n");
          out.write("                                                <dl class=\"dl-horizontal\">\n");
          out.write("                                                    <dt><span class=\"date-text\">");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pp.date}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</span></dt>\n");
          out.write("                                                    <dd>\n");
          out.write("                                                        <h4>\n");
          out.write("                                                            ");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pp.courseName}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</h4>\n");
          out.write("\n");
          out.write("                                                        <p>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pp.content}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</p>\n");
          out.write("                                                    </dd>\n");
          out.write("\n");
          out.write("                                                </dl>\n");
          out.write("                                            </div>\n");
          out.write("                                        ");
          int evalDoAfterBody = _jspx_th_c_forEach_1.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_forEach_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_forEach_1[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_forEach_1.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_forEach_1.doFinally();
      _jspx_tagPool_c_forEach_var_items.reuse(_jspx_th_c_forEach_1);
    }
    return false;
  }

  private boolean _jspx_meth_c_forEach_2(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_2 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_c_forEach_2.setPageContext(_jspx_page_context);
    _jspx_th_c_forEach_2.setParent(null);
    _jspx_th_c_forEach_2.setVar("ppp");
    _jspx_th_c_forEach_2.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${contact.contact}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    int[] _jspx_push_body_count_c_forEach_2 = new int[] { 0 };
    try {
      int _jspx_eval_c_forEach_2 = _jspx_th_c_forEach_2.doStartTag();
      if (_jspx_eval_c_forEach_2 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\n");
          out.write("                                            <div class=\"content\">\n");
          out.write("                                                <p>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${ppp.tel}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</p> <p><span data-mce-mark=\"1\">Email: ");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${ppp.email}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("&nbsp;</span></p> <p>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${ppp.content}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</p>    </div>\n");
          out.write("                                        </div>\n");
          out.write("                                        <div class=\"section signature\">\n");
          out.write("                                            \n");
          out.write("                                            <div class=\"content\">\n");
          out.write("                                                <div class=\"img-simple span4 signature-image\">\n");
          out.write("                                                    <div class=\"image\">\n");
          out.write("                                                        <a rel=\"nofollow\" data-ss=\"imagemodal\" data-href=\"http://cdn.simplesite.com/i/ef/3d/283445307041725935/i283445314527777596._szw1280h1280_.jpg\"><img src=\"./Welcome_files/i283445314527777596._szw480h1280_.jpg\"></a>\n");
          out.write("                                                    </div>\n");
          out.write("                                                </div>\n");
          out.write("\n");
          out.write("                                                <div class=\"span8 signature-text\">\n");
          out.write("                                                    <p>Kind regards</p>\n");
          out.write("\n");
          out.write("                                                </div>\n");
          out.write("                                            </div>\n");
          out.write("                                            ");
          int evalDoAfterBody = _jspx_th_c_forEach_2.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_forEach_2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_forEach_2[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_forEach_2.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_forEach_2.doFinally();
      _jspx_tagPool_c_forEach_var_items.reuse(_jspx_th_c_forEach_2);
    }
    return false;
  }
}
