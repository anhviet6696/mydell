/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import static connection.Databaseinfo.dbURL;
import static connection.Databaseinfo.driverName;
import static connection.Databaseinfo.passDB;
import static connection.Databaseinfo.userDB;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.tomcat.util.digester.ArrayStack;

/**
 *
 * @author tranq
 */
public class Contact {
    private String tel,email,content,address,city,country,oHours;

    public Contact() {
    }

    public Contact(String tel, String email, String content, String address, String city, String country, String oHours) {
        this.tel = tel;
        this.email = email;
        this.content = content;
        this.address = address;
        this.city = city;
        this.country = country;
        this.oHours = oHours;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getoHours() {
        return oHours;
    }

    public void setoHours(String oHours) {
        this.oHours = oHours;
    }

    @Override
    public String toString() {
        return "Contact{" + "tel=" + tel + ", email=" + email + ", content=" + content + ", address=" + address + ", city=" + city + ", country=" + country + ", oHours=" + oHours + '}';
    }
    public ArrayList<Contact> getContact(){
        ArrayList<Contact> list=new ArrayList<>();
        try {
            Class.forName(driverName);
            Connection con=DriverManager.getConnection(dbURL, userDB, passDB);
            Statement stm=con.createStatement();
            ResultSet rs=stm.executeQuery("Select * from Contact");
            while(rs.next()){
                list.add(new Contact(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7)));
            }
            con.close();
            return list;
        } catch (Exception ex) {
            Logger.getLogger(Contact.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
}
