/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import connection.Databaseinfo;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.tomcat.util.digester.ArrayStack;

/**
 *
 * @author tranq
 */
public class Course implements Databaseinfo,Serializable{
    private Date date;
    private String courseName;
    private String content;

    public Course() {
    }

    public Course(Date date, String courseName, String content) {
        this.date = date;
        this.courseName = courseName;
        this.content = content;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Course{" + "date=" + date + ", courseName=" + courseName + ", content=" + content + '}';
    }
    public ArrayList<Course> getAllCourse(){
        ArrayList<Course> list=new ArrayList<>();
        try {
            Class.forName(driverName);
            Connection con=DriverManager.getConnection(dbURL, userDB, passDB);
            Statement stm=con.createStatement();
            ResultSet rs=stm.executeQuery("Select date,courseName,content from Course");
            while(rs.next()){
                list.add(new Course(rs.getDate(1), rs.getString(2), rs.getString(3)));
            }
            con.close();
            return list;
        } catch (Exception ex) {
            Logger.getLogger(Course.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
}
