﻿create database MyFashion
use MyFashion

create table Post(title nvarchar(max),picture nvarchar(max), content nvarchar(max), date DateTime default(getdate()),
NumOfLike int ,comment nvarchar(max))

create table Me(name nvarchar(max), age int,pictureProfile nvarchar(max), history nvarchar(max))

create table Contact (topFanName nvarchar(100), email nvarchar(100), message nvarchar(max))

insert into Post (title,picture,content,NumOfLike,comment) values 
('Covid-19: Moi nguy hiem doi voi toan nhan loai','https://dhs.saccounty.net/PUB/SiteAssets/Pages/Communicable-Disease-Control/2019-Novel-Coronavirus-%282019-nCoV%29/COVID-19.JPG',
'Trung Quốc chỉ có thêm 8 ca nhiễm virus corona mới (SARS-CoV-2) trong ngày 12-3, giảm so với 15 ca trong ngày 11-3, và thêm 7 ca tử vong. Như vậy, tổng cố ca nhiễm tại Trung Quốc đại lục tính đến nay là 80.813 ca và 3.176 ca tử vong. ',
1,'That la kinh khung qua!')
insert into Post (title,picture,content,NumOfLike,comment) values 
('Dịch COVID-19 ngày 13-3: Hơn 1.000 người chết tại Ý, Argentina ban bố tình trạng khẩn cấp y tế','https://cdn.tuoitre.vn/thumb_w/586/2020/3/13/8941852126443429424629071957532444387180544n-15840595575031288681020.jpg',
'Thủ hiến bang Victoria của Úc Daniel Andrews sáng 13-3 thông báo cuộc đua F1 phải hủy bỏ do COVID-19. Giải đua này dự kiến diễn ra tại thành phố Melbourne từ ngày 13 đến 15-3. ',
1,'That la kinh khung qua!')
insert into Post (title,picture,content,NumOfLike,comment) values 
('Nhiều nước châu Âu đóng cửa trường học','https://cdn.tuoitre.vn/thumb_w/586/2020/3/13/phap-corona-macron-15840585364421726811739.jpg',
'Đến nay Bồ Đào Nha đã ghi nhận 78 ca nhiễm COVID-19. Nhà chức trách nước này đã ra lệnh hoãn các sự kiện tập trung hơn 1.000 người trong không gian kín và hơn 5.000 người tại các không gian mở trong nỗ lực ngăn chặn sự lây lan của dịch bệnh. ',
1,'That la kinh khung qua!')
insert into Post (title,picture,content,NumOfLike,comment) values 
('New York cấm tụ tập trên 500 người','https://cdn.tuoitre.vn/thumb_w/586/2020/3/13/881348785583979781028367739876752129785856n-15840593868832103915390.jpg',
'Thống đốc bang New York Andrew Cuomo ngày 12-3 (giờ Mỹ) thông báo bang này sẽ cấm các sự kiện, địa điểm có từ 500 người trở lên trong nỗ lực làm chậm sự lây lan của SARS-CoV-2.',
1,'That la kinh khung qua!')

insert into Me (name , age ,pictureProfile , history) values ('Tran Quoc Viet',21,
'https://scontent-hkg3-1.xx.fbcdn.net/v/t1.0-9/39807945_1097479877072528_5046038371764273152_n.jpg?_nc_cat=107&_nc_sid=13bebb&_nc_ohc=DWNIcakv6gMAX-HpWHY&_nc_ht=scontent-hkg3-1.xx&oh=65e09c0ff026c537142eb9552446a1ac&oe=5E8F7FC2','Bruno Fernandes. ... Bruno Miguel Borges Fernandes (sinh ngày 8 tháng 9 năm 1994) là một cầu thủ bóng đá người Bồ Đào Nha thi đấu ở vị trí tiền vệ cho Manchester United ở Ngoại hạng Anh và đội tuyển quốc gia Bồ Đào Nha.')


select * from Contact