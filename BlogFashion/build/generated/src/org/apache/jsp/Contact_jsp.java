package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class Contact_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<!-- saved from url=(0045)http://us-123fashion.simplesite.com/410906953 -->\n");
      out.write("<html lang=\"en-US\" class=\"\"><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Contact - us-123fashion.simplesite.com</title>\n");
      out.write("        <meta property=\"og:site_name\" content=\"My Fashion Blog\">\n");
      out.write("        <meta property=\"article:publisher\" content=\"https://www.facebook.com/simplesite\">\n");
      out.write("        <meta property=\"og:locale\" content=\"en-US\">\n");
      out.write("        <meta property=\"og:url\" content=\"http://us-123fashion.simplesite.com/410906953\">\n");
      out.write("        <meta property=\"og:title\" content=\"Contact me\">\n");
      out.write("        <meta property=\"og:description\" content=\"Send your message\">\n");
      out.write("        <meta property=\"og:updated_time\" content=\"2017-01-04T04:29:48.8638008+00:00\">\n");
      out.write("        <meta property=\"og:type\" content=\"website\">\n");
      out.write("        <meta name=\"robots\" content=\"nofollow\">\n");
      out.write("\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n");
      out.write("        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n");
      out.write("        <meta name=\"description\" content=\"My Fashion Blog - http://us-123fashion.simplesite.com/\">\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"./Contact_files/3623582.design.v25490.css\">\n");
      out.write("        <link rel=\"canonical\" href=\"http://us-123fashion.simplesite.com/410906953\">\n");
      out.write("        <link rel=\"shortcut icon\" href=\"data:image/x-icon;,\">\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("        <style type=\"text/css\">.fancybox-margin{margin-right:0px;}</style></head>\n");
      out.write("    <body data-pid=\"410906953\" data-iid=\"\">\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("        <div class=\"container-fluid site-wrapper\"> <!-- this is the Sheet -->\n");
      out.write("            <div class=\"container-fluid header-wrapper \" id=\"header\"> <!-- this is the Header Wrapper -->\n");
      out.write("                <div class=\"container\">\n");
      out.write("                    <div class=\"title-wrapper\">\n");
      out.write("                        <div class=\"title-wrapper-inner\">\n");
      out.write("                            <a rel=\"nofollow\" class=\"logo \" href=\"http://us-123fashion.simplesite.com/\">\n");
      out.write("                            </a>\n");
      out.write("                            <div class=\"title \">\n");
      out.write("                                My Fashion Blog\n");
      out.write("                            </div>\n");
      out.write("                            <div class=\"subtitle\">\n");
      out.write("                                Welcome to this website\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>  <!-- these are the titles -->\n");
      out.write("                    <div class=\"navbar navbar-compact\">\n");
      out.write("                        <div class=\"navbar-inner\">\n");
      out.write("                            <div class=\"container\">\n");
      out.write("                                <!-- .btn-navbar is used as the toggle for collapsed navbar content -->\n");
      out.write("                                <a rel=\"nofollow\" class=\"btn btn-navbar\" data-toggle=\"collapse\" data-target=\".nav-collapse\" title=\"Toggle menu\">\n");
      out.write("                                    <span class=\"menu-name\">Menu</span>\n");
      out.write("                                    <span class=\"menu-bars\">\n");
      out.write("                                        <span class=\"icon-bar\"></span>\n");
      out.write("                                        <span class=\"icon-bar\"></span>\n");
      out.write("                                        <span class=\"icon-bar\"></span>\n");
      out.write("                                    </span>\n");
      out.write("                                </a>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("                                <!-- Everything you want hidden at 940px or less, place within here -->\n");
      out.write("                                <div class=\"nav-collapse collapse\">\n");
      out.write("                                    <ul class=\"nav\" id=\"topMenu\" data-submenu=\"horizontal\">\n");
      out.write("                                        <li class=\"  \" style=\"\">\n");
      out.write("                                            <a rel=\"nofollow\" href=\"http://us-123fashion.simplesite.com/410906719\">My Fashion Blog</a>\n");
      out.write("                                        </li><li class=\"  \" style=\"\">\n");
      out.write("                                            <a rel=\"nofollow\" href=\"http://us-123fashion.simplesite.com/410906237\">About me</a>\n");
      out.write("                                        </li><li class=\" active \" style=\"\">\n");
      out.write("                                            <a rel=\"nofollow\" href=\"http://us-123fashion.simplesite.com/410906953\">Contact</a>\n");
      out.write("                                        </li>                </ul>\n");
      out.write("                                </div>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                    <!-- this is the Menu content -->\n");
      out.write("                </div>\n");
      out.write("            </div>  <!-- this is the Header content -->\n");
      out.write("\n");
      out.write("            <div class=\"container-fluid content-wrapper\" id=\"content\"> <!-- this is the Content Wrapper -->\n");
      out.write("                <div class=\"container\">\n");
      out.write("                    <div class=\"row-fluid content-inner\">\n");
      out.write("                        <div id=\"left\" class=\"span9\"> <!-- ADD \"span12\" if no sidebar, or \"span9\" with sidebar -->\n");
      out.write("                            <div class=\"wrapper contact\">\n");
      out.write("                                <div class=\"heading\">\n");
      out.write("                                    <h1 class=\"page-title\">Contact me</h1>\n");
      out.write("                                </div>\n");
      out.write("\n");
      out.write("                                <div class=\"content\">\n");
      out.write("                                    <div class=\"section\">\n");
      out.write("                                        <div class=\"content\">\n");
      out.write("                                            <div class=\"span12 contact-text\">\n");
      out.write("                                                <h4>Send your message</h4>\n");
      out.write("\n");
      out.write("                                            </div>\n");
      out.write("                                        </div>\n");
      out.write("                                    </div>\n");
      out.write("\n");
      out.write("                                    <div class=\"section contact-form\"><div class=\"content\"><div class=\"alert alert-success hide\">\n");
      out.write("                                                Thank you for your message.\n");
      out.write("                                            </div>\n");
      out.write("                                            <div class=\"alert alert-error hide\">\n");
      out.write("                                                <button type=\"button\" class=\"close\">×</button>\n");
      out.write("                                            </div>\n");
      out.write("                                            <p>Write your message here. Fill out the form:</p>\n");
      out.write("                                            <form action=\"contact\" method=\"POST\">\n");
      out.write("\n");
      out.write("                                                <input type=\"text\" name=\"topFanName\" placeholder=\"Your Name\" required/>\n");
      out.write("                                                <input type=\"text\" name=\"email\" placeholder=\"Your Email\" required/>\n");
      out.write("                                                <input type=\"text\" name =\"message\" >\n");
      out.write("                                                <input type=\"submit\" name=\"\" value=\"Send\">\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("                                            </form>\n");
      out.write("                                            <script type=\"text/javascript\">\n");
      out.write("\n");
      out.write("                                                $('#contactFormMessageText').blur(function () {\n");
      out.write("                                                    var text = $('#contactFormMessageText').val().trim();\n");
      out.write("                                                    $('#contactFormMessageText').val(text);\n");
      out.write("                                                });\n");
      out.write("\n");
      out.write("                                            </script></div></div>\n");
      out.write("                                </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                        <div id=\"right\" class=\"span3\">\n");
      out.write("                            <div class=\"sidebar\">\n");
      out.write("                                <div class=\"wrapper share-box\">\n");
      out.write("                                    <style>    .wordwrapfix {\n");
      out.write("                                            word-wrap:break-word;\n");
      out.write("                                        }\n");
      out.write("                                    </style>\n");
      out.write("                                    <div class=\"heading wordwrapfix\">\n");
      out.write("                                        <h4>Share this page</h4>\n");
      out.write("                                    </div>\n");
      out.write("\n");
      out.write("                                    <div class=\"content\"><span><ul>\n");
      out.write("                                                <li><a id=\"share-facebook\" href=\"http://us-123fashion.simplesite.com/410906953#\"><i class=\"icon-facebook-sign\"></i><span>Share on Facebook</span></a></li>\n");
      out.write("                                                <li><a id=\"share-twitter\" href=\"http://us-123fashion.simplesite.com/410906953#\"><i class=\"icon-twitter-sign\"></i><span>Share on Twitter</span></a></li>\n");
      out.write("                                                <li><a id=\"share-google-plus\" href=\"http://us-123fashion.simplesite.com/410906953#\"><i class=\"icon-google-plus-sign\"></i><span>Share on Google+</span></a></li>    \n");
      out.write("                                            </ul></span></div>\n");
      out.write("                                </div>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>        \n");
      out.write("                </div>\n");
      out.write("            </div>  <!-- the controller has determined which view to be shown in the content -->\n");
      out.write("\n");
      out.write("\n");
      out.write("            <!-- this is the Footer content -->\n");
      out.write("        </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("    </body></html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
