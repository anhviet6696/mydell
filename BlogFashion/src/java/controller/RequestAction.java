package controller;


import static com.opensymphony.xwork2.Action.SUCCESS;
import model.Contact;

/**
 *
 * @author tranq
 */
public class RequestAction{

    private String topFanName, email, message;
    Contact c=new Contact();

    public String getTopFanName() {
        return topFanName;
    }

    public void setTopFanName(String topFanName) {
        this.topFanName = topFanName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    public RequestAction() {
    }

    public Contact getC() {
        return c;
    }

    public void setC(Contact c) {
        this.c = c;
    }

    public String execute() throws Exception {
        c.insert(topFanName, email, message);
        return SUCCESS;
    }

    

}
