/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import connection.Databaseinfo;
import static connection.Databaseinfo.dbURL;
import static connection.Databaseinfo.driverName;
import static connection.Databaseinfo.passDB;
import static connection.Databaseinfo.userDB;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tranq
 */
public class Contact implements Databaseinfo {

    private String topFanName, email, message;

    public Contact() {
    }

    public Contact(String topFanName, String email, String message) {
        this.topFanName = topFanName;
        this.email = email;
        this.message = message;
    }

    public String getTopFanName() {
        return topFanName;
    }

    public void setTopFanName(String topFanName) {
        this.topFanName = topFanName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Contact{" + "topFanName=" + topFanName + ", email=" + email + ", message=" + message + '}';
    }

    public boolean insert(String name, String email, String message) {
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            PreparedStatement stm = con.prepareStatement("insert into Contact (topFanName,email,message) values (?,?,?)");
            stm.setString(1, name);
            stm.setString(2, email);
            stm.setString(3, message);
            stm.executeUpdate();
            con.close();;
            return true;
        } catch (Exception ex) {
            Logger.getLogger(Contact.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

}
