/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import connection.Databaseinfo;
import static connection.Databaseinfo.dbURL;
import static connection.Databaseinfo.driverName;
import static connection.Databaseinfo.passDB;
import static connection.Databaseinfo.userDB;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tranq
 */
public class Me implements Databaseinfo,Serializable{
    private String name,pictureProfile,history;
    private int Age;

    public Me() {
    }

    public Me(String name, int Age,String pictureProfile, String history) {
        this.name = name;
        this.pictureProfile = pictureProfile;
        this.history = history;
        this.Age = Age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPictureProfile() {
        return pictureProfile;
    }

    public void setPictureProfile(String pictureProfile) {
        this.pictureProfile = pictureProfile;
    }

    public String getHistory() {
        return history;
    }

    public void setHistory(String history) {
        this.history = history;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int Age) {
        this.Age = Age;
    }

    @Override
    public String toString() {
        return "Me{" + "name=" + name + ", pictureProfile=" + pictureProfile + ", history=" + history + ", Age=" + Age + '}';
    }
    public ArrayList<Me> getAboutMe (){
        ArrayList<Me> list=new ArrayList<>();
        try {
            Class.forName(driverName);
            Connection con=DriverManager.getConnection(dbURL, userDB, passDB);
            Statement stm=con.createStatement();
            ResultSet rs=stm.executeQuery("select * from Me");
            while(rs.next()){
                list.add(new Me(rs.getString(1), rs.getInt(2), rs.getString(3), rs.getString(4)));
            }
            con.close();
            return list;
        } catch (Exception ex) {
            Logger.getLogger(Me.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
}
