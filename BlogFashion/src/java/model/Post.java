
package model;

import connection.Databaseinfo;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tranquocviet
 */
public class Post  implements Databaseinfo,Serializable{
    private String title,picture,content,comment;
    private Date date;
    private int like;

    public Post() {
    }

    public Post(String title, String picture, String content , Date date, int like,String comment) {
        this.title = title;
        this.picture = picture;
        this.content = content;
        this.comment = comment;
        this.date = date;
        this.like = like;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }

    @Override
    public String toString() {
        return "Post{" + "title=" + title + ", picture=" + picture + ", content=" + content + ", comment=" + comment + ", date=" + date + ", like=" + like + '}';
    }
    public ArrayList<Post> getAllPost(){
        ArrayList<Post> list=new ArrayList<>();
        try {
            Class.forName(driverName);
            Connection con=DriverManager.getConnection(dbURL, userDB, passDB);
            Statement stm=con.createStatement();
            ResultSet rs=stm.executeQuery("select top 4 title, picture,content, date,NumOfLike ,comment from Post order by date DESC");
            while(rs.next()){
                list.add(new Post(rs.getString(1), rs.getString(2), rs.getString(3), rs.getDate(4), rs.getInt(5), rs.getString(6)));
            }
            con.close();
            return list;
        } catch (Exception ex) {
            Logger.getLogger(Post.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public ArrayList<Post> getTop4Post(){
        ArrayList<Post> list=new ArrayList<>();
        try {
            Class.forName(driverName);
            Connection con=DriverManager.getConnection(dbURL, userDB, passDB);
            Statement stm=con.createStatement();
            ResultSet rs=stm.executeQuery("select title, picture,content, date,NumOfLike ,comment from Post ");
            while(rs.next()){
                list.add(new Post(rs.getString(1), rs.getString(2), rs.getString(3), rs.getDate(4), rs.getInt(5), rs.getString(6)));
            }
            con.close();
            return list;
            } catch (Exception ex) {
            Logger.getLogger(Post.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
   
}
