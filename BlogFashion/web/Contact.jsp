<%-- 
    Document   : Contact
    Created on : Mar 13, 2020, 2:49:14 PM
    Author     : tranq
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<!-- saved from url=(0045)http://us-123fashion.simplesite.com/410906953 -->
<html lang="en-US" class=""><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Contact - us-123fashion.simplesite.com</title>
        <meta property="og:site_name" content="My Fashion Blog">
        <meta property="article:publisher" content="https://www.facebook.com/simplesite">
        <meta property="og:locale" content="en-US">
        <meta property="og:url" content="http://us-123fashion.simplesite.com/410906953">
        <meta property="og:title" content="Contact me">
        <meta property="og:description" content="Send your message">
        <meta property="og:updated_time" content="2017-01-04T04:29:48.8638008+00:00">
        <meta property="og:type" content="website">
        <meta name="robots" content="nofollow">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="My Fashion Blog - http://us-123fashion.simplesite.com/">
        <link rel="stylesheet" type="text/css" href="./Contact_files/3623582.design.v25490.css">
        <link rel="canonical" href="http://us-123fashion.simplesite.com/410906953">
        <link rel="shortcut icon" href="data:image/x-icon;,">




        <style type="text/css">.fancybox-margin{margin-right:0px;}</style></head>
    <body data-pid="410906953" data-iid="">




        <div class="container-fluid site-wrapper"> <!-- this is the Sheet -->
            <div class="container-fluid header-wrapper " id="header"> <!-- this is the Header Wrapper -->
                <div class="container">
                    <div class="title-wrapper">
                        <div class="title-wrapper-inner">
                            <a rel="nofollow" class="logo " href="http://us-123fashion.simplesite.com/">
                            </a>
                            <div class="title ">
                                My Fashion Blog
                            </div>
                            <div class="subtitle">
                                Welcome to this website
                            </div>
                        </div>
                    </div>  <!-- these are the titles -->
                    <div class="navbar navbar-compact">
                        <div class="navbar-inner">
                            <div class="container">
                                <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
                                <a rel="nofollow" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse" title="Toggle menu">
                                    <span class="menu-name">Menu</span>
                                    <span class="menu-bars">
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </span>
                                </a>



                                <!-- Everything you want hidden at 940px or less, place within here -->
                                <div class="nav-collapse collapse">
                                    <ul class="nav" id="topMenu" data-submenu="horizontal">
                                        <li class="  " style="">
                                            <a rel="nofollow" href="http://us-123fashion.simplesite.com/410906719">My Fashion Blog</a>
                                        </li><li class="  " style="">
                                            <a rel="nofollow" href="http://us-123fashion.simplesite.com/410906237">About me</a>
                                        </li><li class=" active " style="">
                                            <a rel="nofollow" href="http://us-123fashion.simplesite.com/410906953">Contact</a>
                                        </li>                </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- this is the Menu content -->
                </div>
            </div>  <!-- this is the Header content -->

            <div class="container-fluid content-wrapper" id="content"> <!-- this is the Content Wrapper -->
                <div class="container">
                    <div class="row-fluid content-inner">
                        <div id="left" class="span9"> <!-- ADD "span12" if no sidebar, or "span9" with sidebar -->
                            <div class="wrapper contact">
                                <div class="heading">
                                    <h1 class="page-title">Contact me</h1>
                                </div>

                                <div class="content">
                                    <div class="section">
                                        <div class="content">
                                            <div class="span12 contact-text">
                                                <h4>Send your message</h4>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="section contact-form"><div class="content"><div class="alert alert-success hide">
                                                Thank you for your message.
                                            </div>
                                            <div class="alert alert-error hide">
                                                <button type="button" class="close">×</button>
                                            </div>
                                            <p>Write your message here. Fill out the form:</p>
                                            <form action="contact" method="POST">

                                                <input type="text" name="topFanName" placeholder="Your Name" required/>
                                                <input type="text" name="email" placeholder="Your Email" required/>
                                                <input type="text" name ="message" >
                                                <input type="submit" name="" value="Send">



                                            </form>
                                            <script type="text/javascript">

                                                $('#contactFormMessageText').blur(function () {
                                                    var text = $('#contactFormMessageText').val().trim();
                                                    $('#contactFormMessageText').val(text);
                                                });

                                            </script></div></div>
                                </div>


                            </div>
                        </div>
                        <div id="right" class="span3">
                            <div class="sidebar">
                                <div class="wrapper share-box">
                                    <style>    .wordwrapfix {
                                            word-wrap:break-word;
                                        }
                                    </style>
                                    <div class="heading wordwrapfix">
                                        <h4>Share this page</h4>
                                    </div>

                                    <div class="content"><span><ul>
                                                <li><a id="share-facebook" href="http://us-123fashion.simplesite.com/410906953#"><i class="icon-facebook-sign"></i><span>Share on Facebook</span></a></li>
                                                <li><a id="share-twitter" href="http://us-123fashion.simplesite.com/410906953#"><i class="icon-twitter-sign"></i><span>Share on Twitter</span></a></li>
                                                <li><a id="share-google-plus" href="http://us-123fashion.simplesite.com/410906953#"><i class="icon-google-plus-sign"></i><span>Share on Google+</span></a></li>    
                                            </ul></span></div>
                                </div>
                            </div>
                        </div>
                    </div>        
                </div>
            </div>  <!-- the controller has determined which view to be shown in the content -->


            <!-- this is the Footer content -->
        </div>








    </body></html>