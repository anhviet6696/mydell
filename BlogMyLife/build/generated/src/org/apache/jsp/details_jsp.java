package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class details_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_forEach_var_items;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_c_forEach_var_items = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_c_forEach_var_items.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      model.Status post = null;
      synchronized (session) {
        post = (model.Status) _jspx_page_context.getAttribute("post", PageContext.SESSION_SCOPE);
        if (post == null){
          post = new model.Status();
          _jspx_page_context.setAttribute("post", post, PageContext.SESSION_SCOPE);
        }
      }
      out.write('\r');
      out.write('\n');
      org.apache.jasper.runtime.JspRuntimeLibrary.introspect(_jspx_page_context.findAttribute("post"), request);
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<!-- saved from url=(0045)http://us-123my-life.simplesite.com/422328564 -->\r\n");
      out.write("<html lang=\"en-US\" class=\"\"><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("        <title>My Blog - us-123my-life.simplesite.com</title>\r\n");
      out.write("        <meta property=\"og:site_name\" content=\"Blogging About My Life\">\r\n");
      out.write("        <meta property=\"article:publisher\" content=\"https://www.facebook.com/simplesite\">\r\n");
      out.write("        <meta property=\"og:locale\" content=\"en-US\">\r\n");
      out.write("        <meta property=\"og:url\" content=\"http://us-123my-life.simplesite.com/422328564\">\r\n");
      out.write("        <meta property=\"og:title\" content=\"My Blog\">\r\n");
      out.write("        <meta property=\"og:description\" content=\"Essential skills for a happy life! - \r\n");
      out.write("              Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex\r\n");
      out.write("              ea commod...\">\r\n");
      out.write("        <meta property=\"og:image\" content=\"http://cdn.simplesite.com/i/2b/ac/283445309157387307/i283445314544979646._szw1280h1280_.jpg\">\r\n");
      out.write("        <meta property=\"og:updated_time\" content=\"2017-01-04T04:44:57.0966159+00:00\">\r\n");
      out.write("        <meta property=\"og:type\" content=\"blog\">\r\n");
      out.write("        <meta name=\"robots\" content=\"nofollow\">\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n");
      out.write("        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n");
      out.write("        <meta name=\"description\" content=\"Blogging About My Life - http://us-123my-life.simplesite.com/\">\r\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"./home_files/9767673.design.v27169.css\">\r\n");
      out.write("        <link rel=\"canonical\" href=\"http://us-123my-life.simplesite.com/422328564\">\r\n");
      out.write("        <link rel=\"alternate\" type=\"application/rss+xml\" href=\"http://us-123my-life.simplesite.com/422328564/feed.rss\" title=\"My Blog RSS feed\">\r\n");
      out.write("        <link rel=\"alternate\" type=\"application/atom+xml\" href=\"http://us-123my-life.simplesite.com/422328564/atomFeed.rss\" title=\"My Blog RSS feed\">\r\n");
      out.write("        <link rel=\"apple-touch-icon\" sizes=\"57x57\" href=\"http://us-123my-life.simplesite.com/apple-touch-icon-57x57.png\">\r\n");
      out.write("        <link rel=\"apple-touch-icon\" sizes=\"60x60\" href=\"http://us-123my-life.simplesite.com/apple-touch-icon-60x60.png\">\r\n");
      out.write("        <link rel=\"apple-touch-icon\" sizes=\"72x72\" href=\"http://us-123my-life.simplesite.com/apple-touch-icon-72x72.png\">\r\n");
      out.write("        <link rel=\"apple-touch-icon\" sizes=\"76x76\" href=\"http://us-123my-life.simplesite.com/apple-touch-icon-76x76.png\">\r\n");
      out.write("        <link rel=\"apple-touch-icon\" sizes=\"114x114\" href=\"http://us-123my-life.simplesite.com/apple-touch-icon-114x114.png\">\r\n");
      out.write("        <link rel=\"apple-touch-icon\" sizes=\"120x120\" href=\"http://us-123my-life.simplesite.com/apple-touch-icon-120x120.png\">\r\n");
      out.write("        <link rel=\"apple-touch-icon\" sizes=\"144x144\" href=\"http://us-123my-life.simplesite.com/apple-touch-icon-144x144.png\">\r\n");
      out.write("        <link rel=\"apple-touch-icon\" sizes=\"152x152\" href=\"http://us-123my-life.simplesite.com/apple-touch-icon-152x152.png\">\r\n");
      out.write("        <link rel=\"apple-touch-icon\" sizes=\"180x180\" href=\"http://us-123my-life.simplesite.com/apple-touch-icon-180x180.png\">\r\n");
      out.write("        <link rel=\"icon\" sizes=\"194x194\" href=\"http://us-123my-life.simplesite.com/favicon-194x194.png\">\r\n");
      out.write("        <link rel=\"icon\" sizes=\"192x192\" href=\"http://us-123my-life.simplesite.com/android-chrome-192x192.png\">\r\n");
      out.write("        <link rel=\"manifest\" href=\"http://us-123my-life.simplesite.com/manifest.json\">\r\n");
      out.write("        <link rel=\"manifest\" href=\"http://us-123my-life.simplesite.com/yandex-browser-manifest.json\">\r\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"./home_files/jplayer.pink.flag.min.css\">\r\n");
      out.write("        <script type=\"text/javascript\" async=\"\" src=\"./home_files/analytics.js.download\"></script><script async=\"\" src=\"./home_files/gtm.js.download\"></script><script type=\"text/javascript\" src=\"./home_files/FrontendAppLocalePage.aspx\"></script>\r\n");
      out.write("        <script type=\"text/javascript\" src=\"./home_files/frontendApp.min.js.download\"></script>\r\n");
      out.write("        <script type=\"text/javascript\">if (typeof window.jQuery == \"undefined\") {\r\n");
      out.write("                (function () {\r\n");
      out.write("                    var a = document.createElement(\"script\");\r\n");
      out.write("                    a.type = \"text/javascript\";\r\n");
      out.write("                    a.src = \"/c/js/version3/frontendApp/init/frontendApp.min.js?_v=c3745b49b3583731aea941058c4b63a8\";\r\n");
      out.write("                    document.getElementsByTagName('head')[0].appendChild(a);\r\n");
      out.write("                })();\r\n");
      out.write("            }</script>\r\n");
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">var css_simplesite_com_fallback_element = document.getElementById('css_simplesite_com_fallback');\r\n");
      out.write("            if (typeof css_simplesite_com_fallback_element !== 'undefined' && css_simplesite_com_fallback_element !== null) {\r\n");
      out.write("                var isVisible = css_simplesite_com_fallback_element.offsetParent !== null\r\n");
      out.write("                if (isVisible) {\r\n");
      out.write("                    var head = document.head, link = document.createElement('link');\r\n");
      out.write("\r\n");
      out.write("                    link.type = 'text/css';\r\n");
      out.write("                    link.rel = 'stylesheet';\r\n");
      out.write("                    link.href = '{/d/designs/9767673.design.v27169.css}';\r\n");
      out.write("\r\n");
      out.write("                    head.appendChild(link);\r\n");
      out.write("                }\r\n");
      out.write("            }</script>\r\n");
      out.write("        <style type=\"text/css\">.fancybox-margin{margin-right:17px;}</style></head>\r\n");
      out.write("    <body data-pid=\"422328564\" data-iid=\"\">\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("        <div class=\"container-fluid site-wrapper\"> <!-- this is the Sheet -->\r\n");
      out.write("            <div class=\"container-fluid header-wrapper \" id=\"header\"> <!-- this is the Header Wrapper -->\r\n");
      out.write("                <div class=\"container\">\r\n");
      out.write("                    <div class=\"title-wrapper\">\r\n");
      out.write("                        <div class=\"title-wrapper-inner\">\r\n");
      out.write("                            <a rel=\"nofollow\" class=\"logo \" href=\"http://us-123my-life.simplesite.com/\">\r\n");
      out.write("                            </a>\r\n");
      out.write("                            <div class=\"title \">\r\n");
      out.write("                                Blogging About My Life\r\n");
      out.write("                            </div>\r\n");
      out.write("                            <div class=\"subtitle\">\r\n");
      out.write("                                Welcome to my blog\r\n");
      out.write("                            </div>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div>  <!-- these are the titles -->\r\n");
      out.write("                    <div class=\"navbar navbar-compact\">\r\n");
      out.write("                        <div class=\"navbar-inner\">\r\n");
      out.write("                            <div class=\"container\">\r\n");
      out.write("                                <!-- .btn-navbar is used as the toggle for collapsed navbar content -->\r\n");
      out.write("                                <a rel=\"nofollow\" class=\"btn btn-navbar\" data-toggle=\"collapse\" data-target=\".nav-collapse\" title=\"Toggle menu\">\r\n");
      out.write("                                    <span class=\"menu-name\">Menu</span>\r\n");
      out.write("                                    <span class=\"menu-bars\">\r\n");
      out.write("                                        <span class=\"icon-bar\"></span>\r\n");
      out.write("                                        <span class=\"icon-bar\"></span>\r\n");
      out.write("                                        <span class=\"icon-bar\"></span>\r\n");
      out.write("                                    </span>\r\n");
      out.write("                                </a>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("                                <!-- Everything you want hidden at 940px or less, place within here -->\r\n");
      out.write("                                <div class=\"nav-collapse collapse\">\r\n");
      out.write("                                    <ul class=\"nav\" id=\"topMenu\" data-submenu=\"horizontal\">\r\n");
      out.write("                                        <li class=\" active \" style=\"\">\r\n");
      out.write("                                            <a rel=\"nofollow\" href=\"http://us-123my-life.simplesite.com/422328564\">My Blog</a>\r\n");
      out.write("                                        </li><li class=\"  \" style=\"\">\r\n");
      out.write("                                            <a rel=\"nofollow\" href=\"http://us-123my-life.simplesite.com/422328565\">About Me</a>\r\n");
      out.write("                                        </li>                </ul>\r\n");
      out.write("                                </div>\r\n");
      out.write("                            </div>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div>\r\n");
      out.write("                    <!-- this is the Menu content -->\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>  <!-- this is the Header content -->\r\n");
      out.write("\r\n");
      out.write("            <div class=\"container-fluid content-wrapper\" id=\"content\"> <!-- this is the Content Wrapper -->\r\n");
      out.write("                <div class=\"container\">\r\n");
      out.write("                    <div class=\"row-fluid content-inner\">\r\n");
      out.write("                        <div id=\"left\" class=\"span9\"> <!-- ADD \"span12\" if no sidebar, or \"span9\" with sidebar -->\r\n");
      out.write("                            <div class=\"wrapper blog\">\r\n");
      out.write("                                <div class=\"heading\">\r\n");
      out.write("                                    <h1 class=\"page-title\">My Blog</h1>\r\n");
      out.write("                                </div>\r\n");
      out.write("\r\n");
      out.write("                                <div class=\"content\">\r\n");
      out.write("                                    ");
      if (_jspx_meth_c_forEach_0(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("                                    <ul class=\"pager\">\r\n");
      out.write("                                        <li><a rel=\"nofollow\" href=\"overview.jsp\">Overview</a></li>\r\n");
      out.write("                                    </ul>\r\n");
      out.write("                                </div>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("                            </div>\r\n");
      out.write("                        </div>\r\n");
      out.write("                        <div id=\"right\" class=\"span3\">\r\n");
      out.write("                            <div class=\"sidebar\">\r\n");
      out.write("                                <div class=\"wrapper share-box\">\r\n");
      out.write("                                    <style>    .wordwrapfix {\r\n");
      out.write("                                            word-wrap:break-word;\r\n");
      out.write("                                        }\r\n");
      out.write("                                    </style>\r\n");
      out.write("                                    <div class=\"heading wordwrapfix\">\r\n");
      out.write("                                        <h4>Share this page</h4>\r\n");
      out.write("                                    </div>\r\n");
      out.write("\r\n");
      out.write("                                    <div class=\"content\"><span><ul>\r\n");
      out.write("                                                <li><a id=\"share-facebook\" href=\"http://us-123my-life.simplesite.com/422328564#\"><i class=\"icon-facebook-sign\"></i><span>Share on Facebook</span></a></li>\r\n");
      out.write("                                                <li><a id=\"share-twitter\" href=\"http://us-123my-life.simplesite.com/422328564#\"><i class=\"icon-twitter-sign\"></i><span>Share on Twitter</span></a></li>\r\n");
      out.write("                                                <li><a id=\"share-google-plus\" href=\"http://us-123my-life.simplesite.com/422328564#\"><i class=\"icon-google-plus-sign\"></i><span>Share on Google+</span></a></li>    \r\n");
      out.write("                                            </ul></span></div>\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div class=\"wrapper viral-box\">\r\n");
      out.write("                                    <style>    .wordwrapfix {\r\n");
      out.write("                                            word-wrap:break-word;\r\n");
      out.write("                                        }\r\n");
      out.write("                                    </style>\r\n");
      out.write("                                    <div class=\"heading wordwrapfix\">\r\n");
      out.write("                                        <h4>Create a website</h4>\r\n");
      out.write("                                    </div>\r\n");
      out.write("\r\n");
      out.write("                                    <div class=\"content\">\r\n");
      out.write("                                        <p>Everybody can create a website, it's easy.</p>\r\n");
      out.write("                                        <div class=\"bottom\">\r\n");
      out.write("                                            <a rel=\"nofollow\" href=\"http://www.simplesite.com/pages/receive.aspx?partnerkey=123i%3arightbanner&amp;referercustomerid=15831371&amp;refererpageid=422328564\" class=\"btn btn-block\">Try it for FREE now</a>\r\n");
      out.write("                                        </div>\r\n");
      out.write("\r\n");
      out.write("                                    </div>\r\n");
      out.write("                                </div>\r\n");
      out.write("                            </div>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div>        \r\n");
      out.write("                </div>\r\n");
      out.write("            </div>  <!-- the controller has determined which view to be shown in the content -->\r\n");
      out.write("\r\n");
      out.write("            <div class=\"container-fluid footer-wrapper\" id=\"footer\"> <!-- this is the Footer Wrapper -->\r\n");
      out.write("                <div class=\"container\">\r\n");
      out.write("                    <div class=\"footer-info\">\r\n");
      out.write("                        <div class=\"footer-info-text\">\r\n");
      out.write("                            This website was created with SimpleSite\r\n");
      out.write("                        </div>\r\n");
      out.write("                        <div class=\"footer-powered-by\">\r\n");
      out.write("                            <a rel=\"nofollow\" href=\"http://www.simplesite.com/pages/receive.aspx?partnerkey=123i%3afooterbanner&amp;referercustomerid=15831371&amp;refererpageid=422328564\">Get Your own FREE website. Click here!</a>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div>\r\n");
      out.write("                    <div class=\"footer-page-counter\" style=\"display: block;\">\r\n");
      out.write("                        <span class=\"footer-page-counter-item\">0</span>\r\n");
      out.write("\r\n");
      out.write("                        <span class=\"footer-page-counter-item\">0</span>\r\n");
      out.write("\r\n");
      out.write("                        <span class=\"footer-page-counter-item\">6</span>\r\n");
      out.write("\r\n");
      out.write("                        <span class=\"footer-page-counter-item\">7</span>\r\n");
      out.write("\r\n");
      out.write("                        <span class=\"footer-page-counter-item\">6</span>\r\n");
      out.write("\r\n");
      out.write("                        <span class=\"footer-page-counter-item\">0</span>\r\n");
      out.write("                    </div>\r\n");
      out.write("                    <div id=\"css_simplesite_com_fallback\" class=\"hide\"></div>\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("\r\n");
      out.write("            <!-- this is the Footer content -->\r\n");
      out.write("        </div>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("        <input type=\"hidden\" id=\"anti-forgery-token\" value=\"vFZIp11QDd7w1cZjYdzwVVw3rTHgQi1lpMMqomvLq/qGfx85dADIn6rd+jb5+yvdIjnIKNejlFu6vxL7J8Np5e7g5yrI36losWXJkOuv169s22+Z/mKVySX+8iBmNTzgEHnWy/7MRLoBekjH3EpgI99d772eCmdJN7eG7tAqc/m4iWFE2sfSH8oY0Q3pJZcuhjM+blA+FzGTkoYvuddklk1q/4A0667VqYWgFZ7hNc8NO4Kw1Txa422/7pYh/UW67W3GdGTPhdbiuy5yZG8PFfcV3pshO8MZJZI4QqpmoSmbtrQpm9WTX4MbCbnQyM7h5NaUS9oN6X4rtQXeQ4JVwDzgCfuolg6kJOxKEjNy3wz2WXZRbIsB2tTp4Wbn2z/L6tNJvSXC+VnaCrogm/kevHP457ePv10UYx5zjlDDSlerrki2zdJMdCOpSqmI/HGSK1p9byOnEfq2Bz8VxoafOg==\">\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("        <script>\r\n");
      out.write("            dataLayer = [{\"SiteVer\": \"US\", \"MainOrUser\": \"UserPage\", \"PreOrFre\": \"Free\", \"Language\": \"en\", \"Culture\": \"en-US\", \"Instrumentation\": \"ShowPage\", \"Market\": \"DK\"}];\r\n");
      out.write("        </script>\r\n");
      out.write("        <!-- Google Tag Manager -->\r\n");
      out.write("        <noscript>&lt;iframe src=\"//www.googletagmanager.com/ns.html?id=GTM-2MMH\"\r\n");
      out.write("        height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"&gt;&lt;/iframe&gt;</noscript>\r\n");
      out.write("        <script>(function (w, d, s, l, i) {\r\n");
      out.write("                {\r\n");
      out.write("                    w[l] = w[l] || [];\r\n");
      out.write("                    w[l].push({'gtm.start':\r\n");
      out.write("                                new Date().getTime(), event: 'gtm.js'});\r\n");
      out.write("                    var f = d.getElementsByTagName(s)[0],\r\n");
      out.write("                            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';\r\n");
      out.write("                    j.async = true;\r\n");
      out.write("                    j.src =\r\n");
      out.write("                            '//www.googletagmanager.com/gtm.js?id=' + i + dl;\r\n");
      out.write("                    f.parentNode.insertBefore(j, f);\r\n");
      out.write("                }\r\n");
      out.write("            })(window, document, 'script', 'dataLayer', 'GTM-2MMH');</script>\r\n");
      out.write("        <!-- End Google Tag Manager -->\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("        <!-- Remove after blog exp concludes -->\r\n");
      out.write("\r\n");
      out.write("        <div id=\"sm2-container\" class=\"movieContainer high_performance\" style=\"z-index: 10000; position: fixed; width: 8px; height: 8px; bottom: 0px; left: 0px; overflow: hidden; visibility: hidden;\"><embed name=\"sm2movie\" id=\"sm2movie\" src=\"/Images/sm297/soundmanager2_flash9.swf\" quality=\"high\" allowscriptaccess=\"always\" bgcolor=\"#ffffff\" pluginspage=\"www.macromedia.com/go/getflashplayer\" title=\"JS/Flash audio component (SoundManager 2)\" type=\"application/x-shockwave-flash\" wmode=\"transparent\" haspriority=\"true\"></div><div style=\"display: none; visibility: hidden;\"><script type=\"text/javascript\">var google_conversion_id = 1066686464, google_custom_params = window.google_tag_params, google_remarketing_only = !0;</script>\r\n");
      out.write("            <script type=\"text/javascript\" src=\"./home_files/conversion.js.download\"></script><iframe name=\"google_conversion_frame\" title=\"Google conversion frame\" width=\"300\" height=\"13\" src=\"./home_files/saved_resource.html\" frameborder=\"0\" marginwidth=\"0\" marginheight=\"0\" vspace=\"0\" hspace=\"0\" allowtransparency=\"true\" scrolling=\"no\"></iframe>\r\n");
      out.write("            <noscript></noscript>\r\n");
      out.write("        </div></body></html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_c_forEach_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_0 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_c_forEach_0.setPageContext(_jspx_page_context);
    _jspx_th_c_forEach_0.setParent(null);
    _jspx_th_c_forEach_0.setVar("p");
    _jspx_th_c_forEach_0.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${post.getPostByID(2)}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    int[] _jspx_push_body_count_c_forEach_0 = new int[] { 0 };
    try {
      int _jspx_eval_c_forEach_0 = _jspx_th_c_forEach_0.doStartTag();
      if (_jspx_eval_c_forEach_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\r\n");
          out.write("                                        <div class=\"section\">\r\n");
          out.write("                                            <div class=\"content\">\r\n");
          out.write("                                                <div class=\"avatar\">\r\n");
          out.write("                                                    <i class=\"icon-pencil icon-4x\" title=");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${p.title}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("></i>\r\n");
          out.write("                                                </div>\r\n");
          out.write("                                                <div class=\"item\">\r\n");
          out.write("                                                    <div class=\"controls\">\r\n");
          out.write("                                                        <span class=\"date-text\">");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${p.date}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</span>\r\n");
          out.write("                                                    </div>\r\n");
          out.write("                                                    <div class=\"heading\">\r\n");
          out.write("                                                        <h4><a rel=\"nofollow\" href=\"details.jsp\">");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${p.title}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</a></h4>\r\n");
          out.write("                                                    </div>\r\n");
          out.write("\r\n");
          out.write("                                                    <div class=\"content\">\r\n");
          out.write("                                                        <div class=\"img-simple span6 pull-left\">\r\n");
          out.write("                                                            <div class=\"image\">\r\n");
          out.write("                                                                <a rel=\"nofollow\" data-ss=\"imagemodal\" data-href=\"http://cdn.simplesite.com/i/2b/ac/283445309157387307/i283445314544979646._szw1280h1280_.jpg\"><img src=\"");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${p.picture}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("\"></a>\r\n");
          out.write("                                                            </div>\r\n");
          out.write("                                                        </div>\r\n");
          out.write("\r\n");
          out.write("                                                        <p>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${p.content}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</p>\r\n");
          out.write("                                                    </div>\r\n");
          out.write("\r\n");
          out.write("\r\n");
          out.write("                                                </div>\r\n");
          out.write("                                            </div>\r\n");
          out.write("                                        </div>\r\n");
          out.write("\r\n");
          out.write("\r\n");
          out.write("\r\n");
          out.write("                                    ");
          int evalDoAfterBody = _jspx_th_c_forEach_0.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_forEach_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_forEach_0[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_forEach_0.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_forEach_0.doFinally();
      _jspx_tagPool_c_forEach_var_items.reuse(_jspx_th_c_forEach_0);
    }
    return false;
  }
}
