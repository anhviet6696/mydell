package model;

import connection.Databaseinfo;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Status implements Databaseinfo, Serializable {

    private int id, like;
    private String title, picture, content;
    private Date date;

    public Status() {
    }

    public Status(int id, int like, String title, String picture, String content, Date date) {
        this.id = id;
        this.like = like;
        this.title = title;
        this.picture = picture;
        this.content = content;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Status{" + "id=" + id + ", like=" + like + ", title=" + title + ", picture=" + picture + ", content=" + content + ", date=" + date + '}';
    }

    public ArrayList<Status> getPost() {
        ArrayList<Status> list = new ArrayList<>();
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("Select id,likes,EntryName,picture, EntryContent,Date from Blog");
            while (rs.next()) {
                list.add(new Status(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4),
                        rs.getString(5), rs.getDate(6)));
            }
            con.close();
        } catch (Exception ex) {
            Logger.getLogger(Status.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;

    }

    public ArrayList<Status> getTop3() {
        ArrayList<Status> list = new ArrayList<>();
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select top (3) id,likes,EntryName,picture, EntryContent,Date from Blog \n"
                    + "order by Date DESC");
            while (rs.next()) {
                list.add(new Status(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4),
                        rs.getString(5), rs.getDate(6)));
            }
            con.close();
        } catch (Exception ex) {
            Logger.getLogger(Status.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public ArrayList<Status> getPostByID(int id) {
        ArrayList<Status> list = new ArrayList<>();
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select top (3) id,likes,EntryName,picture, EntryContent,Date from Blog where id= '" + id + "'\n"
                    + "order by Date DESC");
            while (rs.next()) {
                list.add(new Status(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4),
                        rs.getString(5), rs.getDate(6)));
            }
            con.close();
        } catch (Exception ex) {
            Logger.getLogger(Status.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public static void main(String[] args) {
        Status stt = new Status();
        for (Status st : stt.getPostByID(4)) {
            System.out.println(st);
        }

    }
}
