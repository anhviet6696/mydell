create database MariaCafe
use MariaCafe

create table Cake (title nvarchar(max), picture nvarchar(max),content nvarchar(max),price decimal(6,2),
date datetime default(getdate()))
create table Author( title nvarchar(max),content nvarchar(max),picture nvarchar(max))
create table Contact (address nvarchar(max),tele nvarchar(max),email nvarchar(max),oHours nvarchar(max))

drop table Cake

insert into Cake (title,picture,content,price )
values
('Traditional Cake', 'https://www.landolakes.com/RecipeManagementSystem/media/Recipe-Media-Files/Recipes/Retail/DesktopImages/Rainbow-Cake600x600_2.jpg?ext=.jpg',
'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut 
laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit 
lobortis nisl ut aliquip ex ea commodo consequat.',12.5)

insert into Cake (title,picture,content,price )
values
('In the afternoon','https://preppykitchen.com/wp-content/uploads/2017/07/chocolate-cake-recipe-2-500x500.jpg',
'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore
 magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut
  aliquip ex ea commodo consequat.',14.5)

insert into Author (title,content,picture) values ('Maria Cosy Cafe',
'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore
 magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut
  aliquip ex ea commodo consequat.', 'https://cdn.tuoitre.vn/thumb_w/640/2018/6/3/chan-dung-phuong-anh-dao-guong-mat-dien-anh-moi-dang-rat-duoc-yeu-thich-anh-nvcc-2-2read-only-1528003202864708724936.jpg')

  insert into Contact (address ,tele ,email ,oHours) values (
  ')2 Tien Son 9','0349880019','tranquocviet@gmail.com','All  times'
  )
  select address,email,tele,oHours from Contact
  select top 3 title, picture,content,price , date from Cake
  select * from Author