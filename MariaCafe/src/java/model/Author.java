/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import connection.Databaseinfo;
import static connection.Databaseinfo.dbURL;
import static connection.Databaseinfo.driverName;
import static connection.Databaseinfo.passDB;
import static connection.Databaseinfo.userDB;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author tranq
 */
public class Author implements Databaseinfo, Serializable {

    private String title, content, picture;

    public Author() {

    }

    public Author(String title, String content, String picture) {
        this.title = title;
        this.content = content;
        this.picture = picture;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    @Override
    public String toString() {
        return "Author{" + "title=" + title + ", content=" + content + ", picture=" + picture + '}';
    }

    public ArrayList<Author> getAuthor() {
        ArrayList<Author> list = new ArrayList<>();
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(" select title,content,picture from Author");
            while (rs.next()) {
                list.add(new Author(rs.getString(1), rs.getString(2), rs.getString(3)));
            }
            con.close();
            return list;
        } catch (Exception ex) {
        }
        return null;
    }
    
}
