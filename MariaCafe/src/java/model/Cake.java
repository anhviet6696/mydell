/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import connection.Databaseinfo;
import static connection.Databaseinfo.dbURL;
import static connection.Databaseinfo.driverName;
import static connection.Databaseinfo.passDB;
import static connection.Databaseinfo.userDB;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author tranq
 */
public class Cake implements Databaseinfo, Serializable {

    private String title, picture, content;
    private Date date;
    private double price;

    public Cake(String title, String picture, String content, Date date, double price) {
        this.title = title;
        this.picture = picture;
        this.content = content;
        this.date = date;
        this.price = price;
    }

    public Cake() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Cake{" + "title=" + title + ", picture=" + picture + ", content=" + content + ", date=" + date + ", price=" + price + '}';
    }

    public ArrayList<Cake> getCake() {
        ArrayList<Cake> list = new ArrayList<>();
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(" select title, picture,content,date,price from Cake");
            while (rs.next()) {
                list.add(new Cake(rs.getString(1), rs.getString(2), rs.getString(3), rs.getDate(4), rs.getDouble(5)));
            }
            con.close();
        } catch (Exception ex) {
        }
        return list;
    }

    public ArrayList<Cake> getTop3Cake() {
        ArrayList<Cake> list = new ArrayList<>();
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(" select top 2 title, picture,content,date,price from Cake order by Date DESC");
            while (rs.next()) {
                list.add(new Cake(rs.getString(1), rs.getString(2), rs.getString(3), rs.getDate(4), rs.getDouble(5)));
            }
            con.close();
        } catch (Exception ex) {
        }
        return list;
    }

}
