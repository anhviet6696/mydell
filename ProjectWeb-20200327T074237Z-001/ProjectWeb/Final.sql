﻿create database Final
use Final
--
create table UserTBL
		(uid int identity(1,1) primary key,sotaikhoan varchar(20) ,
		tien int default 0, phonenb varchar(12) not null, pass varchar(12) not null,
		fname nvarchar(100) not null, email varchar(100), 
		address nvarchar(100),lock varchar(50) default 'Unlock',UserRight int default 0)
--
create table CarTBL
		(bienso varchar(20) primary key not null,uid int not null,
		socho int not null,hangxe varchar(20) not null,
		loainguyenlieu nvarchar(100),hopso nvarchar(100), 
		imgxe varchar(255),imgCV varchar(255),
        noiohientai nvarchar(255) not null,
		status varchar(100) default('Waiting') not null,
		tinhnangxe nvarchar(255) not null,
		mota nvarchar(max),
		tienchothue int not null,tiencoc int,upStt varchar(max)default('Unposted'),
		statusAccepted varchar(20) default('Unaccepted'))
--

create table HirecarTBL
		(Hirecarid int identity(1,1) not null primary key,
		bienso varchar(20) not null,startrent date not null,
		endrent date not null, comment varchar(100),
		location nvarchar(100) not null,
		giaxechothue int not null,hirecarStt varchar(max) default('Waiting'))
--
create table OrderTBL
		(oid int identity(1,1) not null primary key,
		uid int not null,Hirecarid int not null,
		ngaynhan datetime,ngaytra datetime,
		ghichu nvarchar(max),tongtien decimal(20,2) not null,
		tienchuxe decimal(20,2) not null,fee decimal(20,2) not null,
		status varchar(50) default 'Waiting')
--
create table LognoptienTBL
		(accno int identity(1,1) primary key, uid int not null, 
		ngaynop varchar(100) not null, sotien decimal(20,2) not null, 
		mapin varchar(100) not null,sotaikhoan varchar(100) not null,
		phuongthuc varchar(50))

 --------------------
ALTER TABLE CarTBL	
ADD CONSTRAINT FK_Dangkixe FOREIGN KEY(uid)
References UserTBL(uid)
--------------------
ALTER TABLE LognoptienTBL	
ADD CONSTRAINT FK_log FOREIGN KEY(uid)
References UserTBL(uid)
--------------------
ALTER TABLE HirecarTBL	
ADD CONSTRAINT FK_dangbai FOREIGN KEY(bienso)
References CarTBL(bienso)
----------------------
ALTER TABLE OrderTBL	
ADD CONSTRAINT FK_order FOREIGN KEY(Hirecarid)
References HirecarTBL(Hirecarid)
--------------------
ALTER TABLE OrderTBL	
ADD CONSTRAINT FK_rent FOREIGN KEY(uid)
References UserTBL(uid)

--------------
ALTER TABLE CarTBL
DROP CONSTRAINT FK_Dangkixe
ALTER TABLE LognoptienTBL
DROP CONSTRAINT FK_log
ALTER TABLE HirecarTBL
DROP CONSTRAINT FK_dangbai
ALTER TABLE OrderTBL
DROP CONSTRAINT FK_order,FK_rent

------------------
Drop table UserTBL,CarTBL,HirecarTBL,OrderTBL,LognoptienTBL
select * from UserTBL
select * from CarTBL
select * from HirecarTBL
Insert into UserTBL(phonenb,pass,fname,email,address,UserRight) values ('admin','admin',N'Admin','Letanvy020799er@gmail.com',N'Đà Nẵng',1)
Select uid,tien,sotaikhoan,phonenb,pass,fname,email,address,lock from UserTBL
Select oid,UserTBL.uid,UserTBL.fname,HirecarTBL.HirecarID,HirecarTBL.startrent,HirecarTBL.endrent,ngaynhan,ngaytra,HirecarTBL.comment,tongtien,bienso, 
from OrderTBL inner join HirecarTBL on OrderTBL.HirecarID=HirecarTBL.HirecarID inner join UserTBL on OrderTBL.uid=UserTBL.uid

--
insert into UserTBL(phonenb,fname,pass) values ('01202266696','viet','12313')
insert into UserTBL(phonenb,fname,pass) values ('0349880019','viet','123123')
insert into CarTBL(bienso,uid,socho,hangxe,loainguyenlieu,hopso,noiohientai,tinhnangxe,tienchothue)
values ('43C-09996',1,4,'Kia','Xang','So tu dong','Da nang','Blutooth',900000)
insert into CarTBL(bienso,uid,socho,hangxe,loainguyenlieu,hopso,noiohientai,tinhnangxe,tienchothue)
values ('43C-12345',1,7,'Lambogini','Xang','So san','Da nang','Blutooth',1500000)
insert into CarTBL(bienso,uid,socho,hangxe,loainguyenlieu,hopso,noiohientai,tinhnangxe,tienchothue)
values ('43C-75411',2,5,'Huyndai','Dau','So san','Ha Noi','Blutooth',1500000)
---
select  UserTBL.uid,UserTBL.sotaikhoan,UserTBL.phonenb,UserTBL.pass,UserTBL.fname, CarTBL.bienso,CarTBL.socho,
CarTBL.hangxe,CarTBL.loainguyenlieu,CarTBL.hopso,CarTBL.tienchothue,CarTBL.noiohientai
from UserTBL inner join CarTBL on UserTBL.uid=CarTBL.uid where UserTBL.uid=1 and CarTBL.socho=4
--
select  UserTBL.uid,UserTBL.sotaikhoan,UserTBL.phonenb,UserTBL.pass,UserTBL.fname, CarTBL.bienso,CarTBL.socho,
CarTBL.hangxe,CarTBL.loainguyenlieu,CarTBL.hopso,CarTBL.tienchothue,CarTBL.noiohientai
from UserTBL inner join CarTBL on UserTBL.uid=CarTBL.uid where CarTBL.bienso='43C-09996'

select HirecarTBL.location,CarTBL.hangxe,CarTBL.socho,HirecarTBL.giaxechothue,CarTBL.tiencoc,CarTBL.imgxe,CarTBL.bienso
from CarTBL inner join HirecarTBL on CarTBL.bienso=HirecarTBL.bienso where startrent <= '2019-12-25' and  endrent>= '2019-12-30' and CarTBL.socho=4 and Location Like '%?à N?ng%'
Update CarTBL set tiencoc=100000 where socho=5

insert into CarTBL(bienso,uid,socho,hangxe,loainguyenlieu,hopso,noiohientai,tinhnangxe,tienchothue)
values ('43C-099789',1,4,'Kia','Xang','So tu dong',N'Liên Chi?u, ?à N?ng','Bluetooth',900000)
insert into CarTBL(bienso,uid,socho,hangxe,loainguyenlieu,hopso,noiohientai,tinhnangxe,tienchothue)
values ('43C-12545',1,7,'Lambogini','Xang','So san','?à N?ng','Bluetooth',1500000)
insert into CarTBL(bienso,uid,socho,hangxe,loainguyenlieu,hopso,noiohientai,tinhnangxe,tienchothue)
values ('43C-75451',2,5,'Huyndai','Dau','So san','?à N?ng','Blutooth',1500000)
insert into HirecarTBL(bienso,startrent,endrent,location,giaxechothue) values('92A1-19927','2019-12-11 7:00','2019-12-30 7:00',N'Liên Chi?u,?à N?ng',1000000)
insert into HirecarTBL(bienso,startrent,endrent,location,giaxechothue) values('92G1-abc','2019-12-11 7:00','2019-12-30 7:00',N'Liên Chi?u,?à N?ng',1000000)
insert into HirecarTBL(bienso,startrent,endrent,location,giaxechothue) values('43C-09996','2019-12-11 7:00','2019-12-30 7:00',N'Liên Chi?u,?à N?ng',1000000)

Update CarTBL set status='Waiting' where bienso='92G1-abc'
insert into HirecarTBL(startrent,bienso,endrent,location,giaxechothue) values('2019-12-23','43C-099789','2019-12-30',N'?à N?ng',100000)

select * from HirecarTBL where location LIKE N'%?%'

select CarTBL.socho,carTBL.hopso,carTBL.loainguyenlieu,carTBL.mota,carTBL.tinhnangxe,carTBL.tiencoc,carTBL.imgxe,HirecarTBL.startrent,HirecarTBL.endrent,HirecarTBL.giaxechothue,HirecarTBL.comment,HirecarTBL.location from HirecarTBL 
inner join CarTBL on CarTBL.bienso=HirecarTBL.bienso where HirecarTBL.bienso='92G1-abc'
select UserTBL.uid,UserTBL.phonenb,UserTBL.fname,UserTBL.fname,UserTBL.email from UserTBL inner join CarTBL on UserTBL.uid=CarTBL.uid inner join
 HireCarTBL on  carTBL.uid=HirecarTBL.uid where UserTBL.uid=1
 select uid from HirecarTBL where bienso=''
 SELECT DATEDIFF(hour, startrent, endrent) AS DateDiff from HirecarTBL
 create table HirecarTBL
		(Hirecarid int identity(1,1) not null primary key,uid int not null,
		bienso varchar(20) not null, startrent datetime not null, 
		endrent datetime not null,startrenthour time not null, comment varchar(100),
		location nvarchar(100) not null,
		giaxechothue int not null)
--
select HirecarTBL.location,HirecarTBL.giaxechothue ,CarTBL.socho,CarTBL.hangxe,CarTBL.tiencoc,CarTBL.imgxe,CarTBL.bienso,HirecarTBL.uid from CarTBL inner join 
                    HirecarTBL on CarTBL.bienso=HirecarTBL.bienso
					Update HirecarTBL set giaxechothue=1000000 where bienso='92G1-abc'

					select UserTBL.phonenb,UserTBL.fname,UserTBL.email from UserTBL inner join CarTBL on UserTBL.uid=CarTBL.uid inner join
             HireCarTBL on  carTBL.uid=HirecarTBL.uid where UserTBL.uid in (select CarTBL.uid where CarTBL.bienso='92G1-abc') 
			 select UserTBL.uid from Usee

--lich su thue xe
--ten,sdt
select UserTBL.fname,UserTBL.phonenb from UserTBL where uid in
(select CarTBL.uid from HirecarTBL inner join CarTBL on HirecarTBL.bienso=CarTBL.bienso  where HirecarTBL.bienso 
in (select HirecarTBL.bienso from HirecarTBL inner join OrderTBL on HirecarTBL.Hirecarid = OrderTBL.Hirecarid where HirecarTBL.Hirecarid=1))
--bienso,hangxe,location,startrent,endrent
select HirecarTBL.bienso,carTBL.hangxe,HirecarTBL.location,HirecarTBL.startrent,HirecarTBL.endrent from HirecarTBL inner join CarTBL on HirecarTBL.bienso=CarTBL.bienso  where HirecarTBL.bienso 
in (select HirecarTBL.bienso from HirecarTBL inner join OrderTBL on HirecarTBL.Hirecarid = OrderTBL.Hirecarid where HirecarTBL.Hirecarid=1)
--tongtien chu xe nhan duoc tu gd
select OrderTBL.tongtien,OrderTBL.status,HirecarTBL.location from HirecarTBL inner join OrderTBL on HirecarTBL.Hirecarid = OrderTBL.Hirecarid where HirecarTBL.Hirecarid=1
--ls cho thue xe
--user di thue xe
select UserTBL.fname,UserTBL.phonnb from UserTBL inner join OrderTBL on USerTBL.uid=OrderTBL.uid where UserTBL.uid=2
--bienso,hangxe,location,startrent,endrent
select HirecarTBL.bienso,carTBL.hangxe,HirecarTBL.location,HirecarTBL.startrent,HirecarTBL.endrent from HirecarTBL inner join CarTBL on HirecarTBL.bienso=CarTBL.bienso  where HirecarTBL.bienso 
in (select HirecarTBL.bienso from HirecarTBL inner join OrderTBL on HirecarTBL.Hirecarid = OrderTBL.Hirecarid where HirecarTBL.Hirecarid=1)
--tongtien chu xe nhan duoc tu gd
select OrderTBL.tienchuxe,OrderTBL.status from HirecarTBL inner join OrderTBL on HirecarTBL.Hirecarid = OrderTBL.Hirecarid where HirecarTBL.Hirecarid=1
--thong tin xe
select UserTBL.phonenb,UserTBL.fname,UserTBL.email from UserTBL where UserTBL.uid in
(select CarTBL.uid from CarTBL inner join HirecarTBL on HirecarTBL.bienso=CarTBL.bienso where CarTBL.bienso='43C-09996')
update HirecarTBL set HirecarTBL.giaxechothue=CarTBL.tienchothue
                    from HirecarTBL inner join CarTBL on CarTBL.bienso=HirecarTBL.bienso
                    where HirecarTBL.bienso='92G1-abc'

select carTBL.hangxe,HirecarTBL.bienso ,CarTBL.socho,HirecarTBL.giaxechothue from UserTBL inner join CarTBL on UserTBL.uid=CarTBL.uid 
                inner join HirecarTBL on CarTBL.bienso=HirecarTBL.bienso
                   where CarTBL.upStt='Posted' and UserTBL.uid=1
select UserTBL.tien from UserTBL where uid=1
Update UserTBL set tien+=480000 where uid=1
select*from LognoptienTBL

Insert into LognoptienTBL(uid,sotaikhoan,sotien,mapin,phuongthuc,ngaynop) values(1,'123', 50000,'1234', 'theATM','2019')
select Hirecarid from HirecarTBL where bienso=''
select * from OrderTBL
Insert into OrderTBL(uid,Hirecarid,tongtien) values()
Update HirecarTBL set hirecarStt='Waiting' where bienso='92G1-abc'
Update CarTBL set upStt='Posted' where bienso='92G1-abc'
Update  HirecarTBL set hirecarStt='Hidden' where Hirecarid in (select HirecarTBL.Hirecarid from HirecarTBL inner join CarTBL on HirecarTBL.bienso=CarTBL.bienso where carTBL.upStt='Posted' and HirecarTBL.hirecarStt='Waiting' and HirecarTBL.bienso='92G1-abcqq')
Update CarTBL set upStt='Posted' where bienso='92G1-abcdd'
select * from CarTBL
select * from HirecarTBL
select * from LognoptienTBL
Update OrderTBL set status= 'Order' where Hirecarid=2




select * from UserTBL
select * from CarTBL
select * from HirecarTBL
select * from OrderTBL
select * from LognoptienTBL

--lich su thue xe
--ten,sdt
select UserTBL.fname,UserTBL.phonenb from UserTBL where uid in(select CarTBL.uid from HirecarTBL inner join CarTBL on HirecarTBL.bienso=CarTBL.bienso  where HirecarTBL.bienso 
in (select HirecarTBL.bienso from HirecarTBL inner join OrderTBL on HirecarTBL.Hirecarid = OrderTBL.Hirecarid where HirecarTBL.Hirecarid=1))
--bienso,hangxe,location,startrent,endrent
select HirecarTBL.bienso,carTBL.hangxe,HirecarTBL.location,HirecarTBL.startrent,HirecarTBL.endrent from HirecarTBL inner join CarTBL on HirecarTBL.bienso=CarTBL.bienso  where HirecarTBL.bienso 
in (select HirecarTBL.bienso from HirecarTBL inner join OrderTBL on HirecarTBL.Hirecarid = OrderTBL.Hirecarid where HirecarTBL.Hirecarid=1)
--tongtien chu xe nhan duoc tu gd
select OrderTBL.tongtien,OrderTBL.status,HirecarTBL.location from HirecarTBL inner join OrderTBL on HirecarTBL.Hirecarid = OrderTBL.Hirecarid where HirecarTBL.Hirecarid=1
--ls cho thue xe
--user di thue xe
select UserTBL.fname,UserTBL.phonnb from UserTBL inner join OrderTBL on USerTBL.uid=OrderTBL.uid where UserTBL.uid=2
--bienso,hangxe,location,startrent,endrent
select HirecarTBL.bienso,carTBL.hangxe,HirecarTBL.location,HirecarTBL.startrent,HirecarTBL.endrent from HirecarTBL inner join CarTBL on HirecarTBL.bienso=CarTBL.bienso  where HirecarTBL.bienso 
in (select HirecarTBL.bienso from HirecarTBL inner join OrderTBL on HirecarTBL.Hirecarid = OrderTBL.Hirecarid where HirecarTBL.Hirecarid=1)
--tongtien chu xe nhan duoc tu gd
select OrderTBL.tienchuxe,OrderTBL.status from HirecarTBL inner join OrderTBL on HirecarTBL.Hirecarid = OrderTBL.Hirecarid where HirecarTBL.Hirecarid=1