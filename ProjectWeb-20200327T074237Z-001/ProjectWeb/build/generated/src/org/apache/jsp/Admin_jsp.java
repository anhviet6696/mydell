package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.ArrayList;
import model.UserTBL;
import model.UserTBLDB;

public final class Admin_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_s_forEach_var_items;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_s_forEach_var_items = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_s_forEach_var_items.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=utf-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      model.UserTBLDB cus = null;
      synchronized (session) {
        cus = (model.UserTBLDB) _jspx_page_context.getAttribute("cus", PageContext.SESSION_SCOPE);
        if (cus == null){
          cus = new model.UserTBLDB();
          _jspx_page_context.setAttribute("cus", cus, PageContext.SESSION_SCOPE);
        }
      }
      out.write('\n');
      model.CarTBLDB cartbl = null;
      synchronized (session) {
        cartbl = (model.CarTBLDB) _jspx_page_context.getAttribute("cartbl", PageContext.SESSION_SCOPE);
        if (cartbl == null){
          cartbl = new model.CarTBLDB();
          _jspx_page_context.setAttribute("cartbl", cartbl, PageContext.SESSION_SCOPE);
        }
      }
      out.write('\n');
      model.UserTBL uid = null;
      synchronized (session) {
        uid = (model.UserTBL) _jspx_page_context.getAttribute("uid", PageContext.SESSION_SCOPE);
        if (uid == null){
          uid = new model.UserTBL();
          _jspx_page_context.setAttribute("uid", uid, PageContext.SESSION_SCOPE);
        }
      }
      out.write('\n');
      org.apache.jasper.runtime.JspRuntimeLibrary.introspect(_jspx_page_context.findAttribute("uid"), request);
      out.write('\n');
      org.apache.jasper.runtime.JspRuntimeLibrary.introspect(_jspx_page_context.findAttribute("cartbl"), request);
      out.write('\n');
      model.CarTBL car = null;
      synchronized (session) {
        car = (model.CarTBL) _jspx_page_context.getAttribute("car", PageContext.SESSION_SCOPE);
        if (car == null){
          car = new model.CarTBL();
          _jspx_page_context.setAttribute("car", car, PageContext.SESSION_SCOPE);
        }
      }
      out.write('\n');
      org.apache.jasper.runtime.JspRuntimeLibrary.introspect(_jspx_page_context.findAttribute("car"), request);
      out.write('\n');
      model.HirecarTBL hirecar = null;
      synchronized (session) {
        hirecar = (model.HirecarTBL) _jspx_page_context.getAttribute("hirecar", PageContext.SESSION_SCOPE);
        if (hirecar == null){
          hirecar = new model.HirecarTBL();
          _jspx_page_context.setAttribute("hirecar", hirecar, PageContext.SESSION_SCOPE);
        }
      }
      out.write('\n');
      org.apache.jasper.runtime.JspRuntimeLibrary.introspect(_jspx_page_context.findAttribute("hirecar"), request);
      out.write('\n');
      model.OrderTBL order = null;
      synchronized (session) {
        order = (model.OrderTBL) _jspx_page_context.getAttribute("order", PageContext.SESSION_SCOPE);
        if (order == null){
          order = new model.OrderTBL();
          _jspx_page_context.setAttribute("order", order, PageContext.SESSION_SCOPE);
        }
      }
      out.write('\n');
      org.apache.jasper.runtime.JspRuntimeLibrary.introspect(_jspx_page_context.findAttribute("cus"), request);
      out.write("\n");
      out.write("<!doctype html>\n");
      out.write("<html>\n");
      out.write("\n");
      out.write("    <head>\n");
      out.write("\n");
      out.write("        <link href=\"styles/admin.css\" rel='stylesheet' type='text/css' media=\"all\" />\n");
      out.write("\n");
      out.write("        <title>Vinoto</title>\n");
      out.write("\n");
      out.write("    <div class=\"Head\">\n");
      out.write("        <div class=logo>\n");
      out.write("            <img src=\"images/logo.png\" width=\"120px\" height=45px>\n");
      out.write("        </div>\n");
      out.write("        <div class=\"text\">\n");
      out.write("            <p>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${CUSTOMER.fname}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</p>\n");
      out.write("            <a href=\"Login.jsp\" style=\"color:yellow\"><pre>   Logout</pre> </a>\n");
      out.write("        </div>\n");
      out.write("\n");
      out.write("    </div>\n");
      out.write("</head>\n");
      out.write("\n");
      out.write("<body>\n");
      out.write("    <div class=\"content-left\">\n");
      out.write("        <a href=\"#\" onclick=\"showtabel1();\">Quản Lý User</a>\n");
      out.write("        <hr>\n");
      out.write("        <a href=\"#\" onclick=\"showtabel2();\">Quản Lý Giao Dịch</a>\n");
      out.write("        <hr>\n");
      out.write("        <a href=\"#\" onclick=\"showtabel3();\">Quản Lý Xe Đăng Ký</a>\n");
      out.write("       \n");
      out.write("    </div>\n");
      out.write("    <div class=\"content-right\">\n");
      out.write("        <section>\n");
      out.write("            <div id=\"tabel1\">\n");
      out.write("                <h1>User Trong Hệ Thống</h1>\n");
      out.write("                <div class=\"tbl-header\">\n");
      out.write("                    <table cellpadding=\"0\" cellspacing=\"0\">\n");
      out.write("                        <thead>\n");
      out.write("                            <tr>\n");
      out.write("                                <th>PhoneNumber</th>\n");
      out.write("                                <th>Name</th>\n");
      out.write("                                <th>Email</th>\n");
      out.write("                                <th>UserRight</th>\n");
      out.write("                                <th>Address</th>\n");
      out.write("                                <th>Tiền</th> \n");
      out.write("                                <th>Status</th>  \n");
      out.write("                                <th>      </th>  \n");
      out.write("                                <th>      </th>  \n");
      out.write("                            </tr>\n");
      out.write("                        </thead>\n");
      out.write("                    </table>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"tbl-content\">\n");
      out.write("                    <table cellpadding=\"0\" cellspacing=\"0\">\n");
      out.write("                        <tbody>\n");
      out.write("                            ");
      if (_jspx_meth_s_forEach_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("                        </tbody>\n");
      out.write("                    </table>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </section>\n");
      out.write("     <section>\n");
      out.write("            <div id=\"tabel2\">\n");
      out.write("                <h1>Lịch sử thuê xe</h1>\n");
      out.write("                <div class=\"tbl-header\">\n");
      out.write("                    <table cellpadding=\"0\" cellspacing=\"0\">\n");
      out.write("                        <thead>\n");
      out.write("                            <tr>\n");
      out.write("                                <th>Chủ xe</th>\n");
      out.write("                                <th>Người thuê</th>\n");
      out.write("                                <th>SĐT người thuê</th>\n");
      out.write("                                <th>Biển số</th>\n");
      out.write("                                <th>Ngày thuê</th>\n");
      out.write("                                <th>Ngày trả</th>\n");
      out.write("                                <th>Tổng tiền</th>\n");
      out.write("                            </tr>\n");
      out.write("                        </thead>\n");
      out.write("                    </table>\n");
      out.write("                </div>\n");
      out.write("                \n");
      out.write("                    <div class=\"tbl-content\">\n");
      out.write("                        <table cellpadding=\"0\" cellspacing=\"0\">\n");
      out.write("                            <tbody>\n");
      out.write("                                <c:forEach items=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${order.orderList}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\" var=\"p\">\n");
      out.write("                                <tr>\n");
      out.write("                                    <c:forEach items=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${cus.getAllChuXe(p.bienso)}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\" var=\"pp\">\n");
      out.write("                                        <td>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pp.fname}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</td>\n");
      out.write("                                    </c:forEach>\n");
      out.write("\n");
      out.write("                                    <td>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${p.fname}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</td>\n");
      out.write("                                    <td>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${p.phonenb}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</td>\n");
      out.write("                                    <td>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${p.bienso}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</td>\n");
      out.write("                                    \n");
      out.write("                                    <td>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${p.ngaynhan}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</td>\n");
      out.write("                                    <td>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${p.ngaytra}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</td>\n");
      out.write("                                    <td>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${p.tongtien}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</td>\n");
      out.write("                                </tr>\n");
      out.write("                                </c:forEach>\n");
      out.write("                            </tbody>\n");
      out.write("                        </table>\n");
      out.write("                    </div>\n");
      out.write("                \n");
      out.write("            </div>\n");
      out.write("        </section>\n");
      out.write("        \n");
      out.write("        <section>\n");
      out.write("            <div id=\"tabel3\">\n");
      out.write("                <h1>Quan li xe trong he thong</h1>\n");
      out.write("                <div class=\"tbl-header\">\n");
      out.write("                    <table cellpadding=\"0\" cellspacing=\"0\">\n");
      out.write("                        <thead>\n");
      out.write("                            <tr>\n");
      out.write("                                <th>Biển Số</th>\n");
      out.write("                                    <th>Số chỗ</th>\n");
      out.write("                                    <th>Hãng Xe</th>\n");
      out.write("                                    <th>Loại Nguyên Liệu</th>\n");
      out.write("                                    <th>Hộp Số</th>\n");
      out.write("                                    <th>Nơi ở hiện tại</th>\n");
      out.write("                                    <th>Tính Năng Xe</th>\n");
      out.write("                                    <th>Tiền Cho Thuê</th>\n");
      out.write("                                    <th>Trang thai</th>\n");
      out.write("                                    <th>Chức Năng</th>\n");
      out.write("                                    <th>Chức Năng</th> \n");
      out.write("                            </tr>\n");
      out.write("                        </thead>\n");
      out.write("                    </table>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"tbl-content\">\n");
      out.write("                    <table cellpadding=\"0\" cellspacing=\"0\">\n");
      out.write("                        <tbody>\n");
      out.write("                            ");
      if (_jspx_meth_s_forEach_1(_jspx_page_context))
        return;
      out.write("\n");
      out.write("                        </tbody>\n");
      out.write("                    </table>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </section>\n");
      out.write("    </div>\n");
      out.write("    <script>\n");
      out.write("        function showtabel1() {\n");
      out.write("            document.getElementById(\"tabel1\").style.visibility = \"visible\";\n");
      out.write("            document.getElementById(\"tabel2\").style.visibility = \"hidden\";\n");
      out.write("            document.getElementById(\"tabel3\").style.visibility = \"hidden\";\n");
      out.write("        }\n");
      out.write("\n");
      out.write("        function showtabel2() {\n");
      out.write("            document.getElementById(\"tabel1\").style.visibility = \"hidden\";\n");
      out.write("            document.getElementById(\"tabel2\").style.visibility = \"visible\";\n");
      out.write("            document.getElementById(\"tabel3\").style.visibility = \"hidden\";\n");
      out.write("        }\n");
      out.write("\n");
      out.write("        function showtabel3() {\n");
      out.write("            document.getElementById(\"tabel1\").style.visibility = \"hidden\";\n");
      out.write("            document.getElementById(\"tabel2\").style.visibility = \"hidden\";\n");
      out.write("            document.getElementById(\"tabel3\").style.visibility = \"visible\";\n");
      out.write("        }\n");
      out.write("    </script>\n");
      out.write("</body>\n");
      out.write("\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_s_forEach_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_s_forEach_0 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_s_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_s_forEach_0.setPageContext(_jspx_page_context);
    _jspx_th_s_forEach_0.setParent(null);
    _jspx_th_s_forEach_0.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${cus.listAll()}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    _jspx_th_s_forEach_0.setVar("UserTBLDB");
    int[] _jspx_push_body_count_s_forEach_0 = new int[] { 0 };
    try {
      int _jspx_eval_s_forEach_0 = _jspx_th_s_forEach_0.doStartTag();
      if (_jspx_eval_s_forEach_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\n");
          out.write("                                <tr>\n");
          out.write("                                    <td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${UserTBLDB.phonenb}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n");
          out.write("                                    <td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${UserTBLDB.fname}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n");
          out.write("                                    <td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${UserTBLDB.email}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n");
          out.write("                                    <td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${UserTBLDB.userRight}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>      \n");
          out.write("                                    <td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${UserTBLDB.address}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n");
          out.write("                                    <td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${UserTBLDB.tien}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td> \n");
          out.write("                                    <td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${UserTBLDB.lock}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n");
          out.write("                                    <td><button type=\"submit\" ><a href=\"changeUnLock?uid=");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${UserTBLDB.uid}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("\">unLock</button></a></td>\n");
          out.write("                                    <td><button type=\"submit\"><a href=\"changeLock?uid=");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${UserTBLDB.uid}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("\">Lock</button></a></td>\n");
          out.write("                                </tr>\n");
          out.write("                            ");
          int evalDoAfterBody = _jspx_th_s_forEach_0.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_s_forEach_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_s_forEach_0[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_s_forEach_0.doCatch(_jspx_exception);
    } finally {
      _jspx_th_s_forEach_0.doFinally();
      _jspx_tagPool_s_forEach_var_items.reuse(_jspx_th_s_forEach_0);
    }
    return false;
  }

  private boolean _jspx_meth_s_forEach_1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_s_forEach_1 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_s_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_s_forEach_1.setPageContext(_jspx_page_context);
    _jspx_th_s_forEach_1.setParent(null);
    _jspx_th_s_forEach_1.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${cartbl.allbystatusAcceptCar}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    _jspx_th_s_forEach_1.setVar("p");
    int[] _jspx_push_body_count_s_forEach_1 = new int[] { 0 };
    try {
      int _jspx_eval_s_forEach_1 = _jspx_th_s_forEach_1.doStartTag();
      if (_jspx_eval_s_forEach_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\n");
          out.write("                               <tr>\n");
          out.write("                                        <td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${p.bienso}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n");
          out.write("                                        <td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${p.socho}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n");
          out.write("                                        <td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${p.hangxe}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n");
          out.write("                                        <td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${p.loainguyenlieu}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n");
          out.write("                                        <td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${p.hopso}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n");
          out.write("                                        <td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${p.noiohientai}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n");
          out.write("                                        <td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${p.tinhnangxe}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n");
          out.write("                                        <td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${p.tienchothue}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n");
          out.write("                                        <td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${p.statusAccepted}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n");
          out.write("                                        <td><button><a href=\"acceptcar?bienso=");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${p.bienso}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("\" >Accepted</a></button></td>\n");
          out.write("                                        <td><button><a href=\"unacceptcar?bienso=");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${p.bienso}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("\">Unaccepted</a></button></td>\n");
          out.write("                                    </tr>\n");
          out.write("                            ");
          int evalDoAfterBody = _jspx_th_s_forEach_1.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_s_forEach_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_s_forEach_1[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_s_forEach_1.doCatch(_jspx_exception);
    } finally {
      _jspx_th_s_forEach_1.doFinally();
      _jspx_tagPool_s_forEach_var_items.reuse(_jspx_th_s_forEach_1);
    }
    return false;
  }
}
