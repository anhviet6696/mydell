package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class Login_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList<String>(2);
    _jspx_dependants.add("/includes/header.jsp");
    _jspx_dependants.add("/includes/footer.jsp");
  }

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=utf-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write('\n');
      out.write("\n");
      out.write("\n");
      out.write("<!doctype html>\n");
      out.write("\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta charset=\"utf-8\">\n");
      out.write("        <title>Vin Oto - Niềm tự hào của chúng tôi ! </title>\n");
      out.write("        <link rel=\"shortcut icon\" href=\"images/favicon.ico\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"styles/Css.css\">\n");
      out.write("\n");
      out.write("    </head>\n");
      out.write("    ");
 request.setCharacterEncoding("UTF-8");
      out.write("\n");
      out.write("    <body class=\"body_header\">\n");
      out.write("        <header>\n");
      out.write("            <div class=\"Head\">\n");
      out.write("                <div class=logo>\n");
      out.write("                    <a href=\"Index.jsp\">\n");
      out.write("                        <img src=\"images/logo.png\" width=\"120px\" height=45px></a>\n");
      out.write("                </div>\n");
      out.write("\n");
      out.write("                <div class=phoneIcon>\n");
      out.write("                    <img src=\"images/phoneLogo.png\" width=\"25px\" height=25px>\n");
      out.write("                </div>\n");
      out.write("\n");
      out.write("                <div class=\"textNumber\">\n");
      out.write("                    <p>0988888888</p>\n");
      out.write("                </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("                <div class=email>\n");
      out.write("                    <img src=\"images/mail.png\" width=\"25px\" height=25px>\n");
      out.write("                </div>\n");
      out.write("\n");
      out.write("                <div class=\"textEmail\">\n");
      out.write("                    <p>vinOtosupport@gmail.com</p>\n");
      out.write("                </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("                <div class=facebook>\n");
      out.write("                    <a href=\"https://www.facebook.com/Vin-OTO-107100654062033/\" target=\"_blank\">\n");
      out.write("                        <img src=\"images/facebook.png\" width=\"25px\" height=25px>\n");
      out.write("                    </a>\n");
      out.write("                </div>\n");
      out.write("\n");
      out.write("                <div class=\"textFacebook\">\n");
      out.write("                    <p>Vin Oto</p>\n");
      out.write("                </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("                <div class=\"dangkydangnhap\">\n");
      out.write("                    <button  class=dangky><a id=\"a_head\" href=\"Register.jsp\">Đăng Ký</a></button>\n");
      out.write("                    <button class=dangnhap><a id=\"a_head\"  href=\"Login.jsp\">Đăng Nhập</a></button>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("        </header>\n");
      out.write("    </body>\n");
      out.write("</html>");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <link rel=\"stylesheet\" href=\"styles/Css.css\">\n");
      out.write("    <head>\n");
      out.write("        <meta charset=\"utf-8\">\n");
      out.write("    </head>\n");
      out.write("    <body class=\"body_login\">\n");
      out.write("        <div class=\"login\">\n");
      out.write("            <h1 id=\"login_h1\">Login</h1>\n");
      out.write("            <span></span>\n");
      out.write("            <span></span>\n");
      out.write("            <span></span>\n");
      out.write("            <span></span>\n");
      out.write("            <form action=\"login\" method=\"POST\">\n");
      out.write("                <input type=\"text\" name=\"phonenb\" placeholder=\"PhoneNumber\" required/>\n");
      out.write("                <input type=\"password\" name=\"pass\" placeholder=\"Password\" required/>\n");
      out.write("                <input type=\"checkbox\" name =\"checkRemember\" ><span id=\"span_login\">Remember me</span>\n");
      out.write("                <input type=\"submit\" name=\"\" value=\"Login\">\n");
      out.write("            </form>\n");
      out.write("        </div>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
      out.write("\n");
      out.write("<footer>\n");
      out.write("    <div class=\"wrapper\">\n");
      out.write("        <div class=\"column\">\n");
      out.write("\n");
      out.write("            <div class=\"text_footer\">\n");
      out.write("                <h4>Contact with us</h4>\n");
      out.write("                <p>Thanks For all thing , We will never forget your support</p>\n");
      out.write("                <div id=\"social-links\">\n");
      out.write("                    <a href=\"#\"><img src=\"https://images.vexels.com/media/users/3/137253/isolated/preview/90dd9f12fdd1eefb8c8976903944c026-facebook-icon-logo-by-vexels.png\" width=\"40px\" height=\"40px\"></a>\n");
      out.write("                    <a href=\"#\"><img src=\"https://images.vexels.com/media/users/3/137419/isolated/preview/b1a3fab214230557053ed1c4bf17b46c-twitter-icon-logo-by-vexels.png\" width=\"40px\" height=\"40px\"></a>\n");
      out.write("                    <a href=\"#\"><img src=\"https://lh3.googleusercontent.com/L6jXqpFlxBsakV5StgFPQt4QkEF5G4_GvIbuNY9t0GL0uXIhBUZoDe4u_6cAsQaoOlID\" width=\"40px\" height=\"40px\"></a>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("\n");
      out.write("        <div class=\"column\">\n");
      out.write("\n");
      out.write("            <div class=\"text_footer\">\n");
      out.write("                <h4>Information</h4>\n");
      out.write("                <p>\n");
      out.write("                    About<br /> Service\n");
      out.write("                    <br /> Conditions\n");
      out.write("                    <br /> Become a partner<br /> Best Price Guarantee<br /> Privacy and Prolicy\n");
      out.write("                </p>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("\n");
      out.write("        <div class=\"column\">\n");
      out.write("\n");
      out.write("            <div class=\"text_footer\">\n");
      out.write("                <h4>Customer Support</h4>\n");
      out.write("                <p class=\"links\">\n");
      out.write("                    <a href=\"#\">FAQ</a><br />\n");
      out.write("                    <a href=\"#\">Payment Option</a><br />\n");
      out.write("                    <a href=\"#\">Booking Tips</a><br />\n");
      out.write("                    <a href=\"#\">How its work</a><br />\n");
      out.write("                    <a href=\"#\">Contact us</a><br />\n");
      out.write("                    <a href=\"#\">How to use it</a>\n");
      out.write("                </p>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("\n");
      out.write("        <div class=\"column\">\n");
      out.write("            <div class=\"text_footer\">\n");
      out.write("                <div class=\"haveQuestion\">\n");
      out.write("                    <h4>Have a Questions</h4>\n");
      out.write("                    <p class=\"links\">\n");
      out.write("                        <a href=\"https://www.facebook.com/thanh.simon.7\">Vinoto@gmail.com</a><br>\n");
      out.write("                        <br> Phone Number :+ 999999999\n");
      out.write("                        <br>\n");
      out.write("                        <br /> Location : Khu Đô Thị FPT, Thành Phố Đà Nẵng\n");
      out.write("                    </p>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("\n");
      out.write("        <div id=\"footer_l\"> This Web Side Make By 4 HANDSOME MAN : Quang ,Viet ,Huy ,Vy </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("    </div>\n");
      out.write("</footer>\n");
      out.write("</body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
