<%@page import="java.util.ArrayList"%>
<%@page import="model.UserTBL"%>
<%@page import="model.UserTBLDB"%>
<%@page contentType="text/html" pageEncoding="utf-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "s" %>
<jsp:useBean id="cus" class="model.UserTBLDB" scope="session"></jsp:useBean>
<jsp:useBean id="cartbl" class="model.CarTBLDB" scope="session"></jsp:useBean>
<jsp:useBean id="uid" class="model.UserTBL" scope="session"></jsp:useBean>
<jsp:setProperty name="uid" property="*"/>
<jsp:setProperty name="cartbl" property="*"/>
<jsp:useBean id="car" class="model.CarTBL" scope="session" />
<jsp:setProperty name ="car"  property="*"/>
<jsp:useBean id="hirecar" class="model.HirecarTBL" scope="session" />
<jsp:setProperty name ="hirecar"  property="*"/>
<jsp:useBean id="order" class="model.OrderTBL" scope="session" />
<jsp:setProperty name ="cus"  property="*"/>
<!doctype html>
<html>

    <head>

        <link href="styles/admin.css" rel='stylesheet' type='text/css' media="all" />

        <title>Vinoto</title>

    <div class="Head">
        <div class=logo>
            <img src="images/logo.png" width="120px" height=45px>
        </div>
        <div class="text">
            <p>${CUSTOMER.fname}</p>
            <a href="Login.jsp" style="color:yellow"><pre>   Logout</pre> </a>
        </div>

    </div>
</head>

<body>
    <div class="content-left">
        <a href="#" onclick="showtabel1();">Quản Lý User</a>
        <hr>
        <a href="#" onclick="showtabel2();">Quản Lý Giao Dịch</a>
        <hr>
        <a href="#" onclick="showtabel3();">Quản Lý Xe Đăng Ký</a>
       
    </div>
    <div class="content-right">
        <section>
            <div id="tabel1">
                <h1>User Trong Hệ Thống</h1>
                <div class="tbl-header">
                    <table cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th>PhoneNumber</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>UserRight</th>
                                <th>Address</th>
                                <th>Tiền</th> 
                                <th>Status</th>  
                                <th>      </th>  
                                <th>      </th>  
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="tbl-content">
                    <table cellpadding="0" cellspacing="0">
                        <tbody>
                            <s:forEach items="${cus.listAll()}" var="UserTBLDB">
                                <tr>
                                    <td>${UserTBLDB.phonenb}</td>
                                    <td>${UserTBLDB.fname}</td>
                                    <td>${UserTBLDB.email}</td>
                                    <td>${UserTBLDB.userRight}</td>      
                                    <td>${UserTBLDB.address}</td>
                                    <td>${UserTBLDB.tien}</td> 
                                    <td>${UserTBLDB.lock}</td>
                                    <td><button type="submit" ><a href="changeUnLock?uid=${UserTBLDB.uid}">unLock</button></a></td>
                                    <td><button type="submit"><a href="changeLock?uid=${UserTBLDB.uid}">Lock</button></a></td>
                                </tr>
                            </s:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
     <section>
            <div id="tabel2">
                <h1>Lịch sử thuê xe</h1>
                <div class="tbl-header">
                    <table cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Chủ xe</th>
                                <th>Người thuê</th>
                                <th>SĐT người thuê</th>
                                <th>Biển số</th>
                                <th>Ngày thuê</th>
                                <th>Ngày trả</th>
                                <th>Tổng tiền</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                
                    <div class="tbl-content">
                        <table cellpadding="0" cellspacing="0">
                            <tbody>
                                <c:forEach items="${order.orderList}" var="p">
                                <tr>
                                    <c:forEach items="${cus.getAllChuXe(p.bienso)}" var="pp">
                                        <td>${pp.fname}</td>
                                    </c:forEach>

                                    <td>${p.fname}</td>
                                    <td>${p.phonenb}</td>
                                    <td>${p.bienso}</td>
                                    
                                    <td>${p.ngaynhan}</td>
                                    <td>${p.ngaytra}</td>
                                    <td>${p.tongtien}</td>
                                </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                
            </div>
        </section>
        
        <section>
            <div id="tabel3">
                <h1>Quan li xe trong he thong</h1>
                <div class="tbl-header">
                    <table cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Biển Số</th>
                                    <th>Số chỗ</th>
                                    <th>Hãng Xe</th>
                                    <th>Loại Nguyên Liệu</th>
                                    <th>Hộp Số</th>
                                    <th>Nơi ở hiện tại</th>
                                    <th>Tính Năng Xe</th>
                                    <th>Tiền Cho Thuê</th>
                                    <th>Trang thai</th>
                                    <th>Chức Năng</th>
                                    <th>Chức Năng</th> 
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="tbl-content">
                    <table cellpadding="0" cellspacing="0">
                        <tbody>
                            <s:forEach items="${cartbl.allbystatusAcceptCar}" var="p">
                               <tr>
                                        <td>${p.bienso}</td>
                                        <td>${p.socho}</td>
                                        <td>${p.hangxe}</td>
                                        <td>${p.loainguyenlieu}</td>
                                        <td>${p.hopso}</td>
                                        <td>${p.noiohientai}</td>
                                        <td>${p.tinhnangxe}</td>
                                        <td>${p.tienchothue}</td>
                                        <td>${p.statusAccepted}</td>
                                        <td><button><a href="acceptcar?bienso=${p.bienso}" >Accepted</a></button></td>
                                        <td><button><a href="unacceptcar?bienso=${p.bienso}">Unaccepted</a></button></td>
                                    </tr>
                            </s:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>
    <script>
        function showtabel1() {
            document.getElementById("tabel1").style.visibility = "visible";
            document.getElementById("tabel2").style.visibility = "hidden";
            document.getElementById("tabel3").style.visibility = "hidden";
        }

        function showtabel2() {
            document.getElementById("tabel1").style.visibility = "hidden";
            document.getElementById("tabel2").style.visibility = "visible";
            document.getElementById("tabel3").style.visibility = "hidden";
        }

        function showtabel3() {
            document.getElementById("tabel1").style.visibility = "hidden";
            document.getElementById("tabel2").style.visibility = "hidden";
            document.getElementById("tabel3").style.visibility = "visible";
        }
    </script>
</body>

</html>