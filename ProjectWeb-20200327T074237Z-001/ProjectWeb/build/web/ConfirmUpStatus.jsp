<%@page import="model.CarTBL"%>
<%@page contentType="text/html" pageEncoding="utf-8" %>
<%@ include file="/includes/header_cus.jsp" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:useBean id="car" class="model.CarTBL" scope="session" />
<jsp:setProperty name ="car"  property="*"/>
<jsp:useBean id="hirecar" class="model.HirecarTBL" scope="session" />
<jsp:setProperty name ="hirecar"  property="*"/>
<!DOCTYPE html>
<html>
    <link rel="stylesheet" href="styles/UpStatus.css">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <form action="up" method="post">
            <div id="formxacnhandangbai">
                <%CarTBL cartbl = new CarTBL();
                String bienso = request.getParameter("bienso");
                String imgxe=cartbl.getimgxe(bienso);
                %>
                <div class="imagecar"><img src="images/<%=imgxe%>" width="1000px" height="495px"></div>
                <div class="thongtinxe">
                    <c:forEach items="${car.getAllByBienSo(car.bienso)}" var="p">
                        <p class="title">Biển số :&emsp;&emsp;&emsp;&emsp;<mark class="subtitle" name="bienso">${p.bienso}</mark></p>
                            </c:forEach>
                            <c:forEach items="${car.getAllByBienSo(car.bienso)}" var="p">
                        <p class="title">Số Chỗ :&emsp;&emsp;&emsp;&emsp;<mark class="subtitle" name="socho">${p.socho}</mark></p>
                            </c:forEach>
                            <c:forEach items="${car.getAllByBienSo(car.bienso)}" var="p">
                        <p class="title">Hãng Xe :&emsp;&emsp;&emsp;&emsp;<mark class="subtitle"name="hangxe">${p.hangxe}</mark></p>
                            </c:forEach>
                            <c:forEach items="${car.getAllByBienSo(car.bienso)}" var="p">
                        <p class="title">Nhiên Liệu:&emsp;&emsp;&emsp;&emsp;<mark class="subtitle"name="loainguyenlieu">${p.loainguyenlieu}</mark></p>
                            </c:forEach>
                            <c:forEach items="${car.getAllByBienSo(car.bienso)}" var="p">
                        <p class="title">Hộp Số :&emsp;&emsp;&emsp;&emsp;<mark class="subtitle" name="hopso">${p.hopso}</mark></p>
                            </c:forEach>
                            <c:forEach items="${car.getAllByBienSo(car.bienso)}" var="p">
                        <p class="title">Tính năng :&emsp;&emsp;&emsp;&emsp;<mark class="subtitle"name="tinhnangxe">${p.tinhnangxe}</mark></p>
                            </c:forEach>


                    <p class="title">Ghi Chú :&emsp;&emsp;&emsp;&emsp;
                    <fieldset> <textarea class="subtitle"></textarea></fieldset>
                    </p>

                </div>
            </div>

            <div class="giatien">
                <div id="contact">
                    <h5 class="title">Time For Rent</h5>
                    <fieldset name="startrent">
                        <h5>Start for rent:</h5><hr>
                        <mark class="subtitle" >${hirecar.startrent}</mark>
                    </fieldset>
                    <fieldset>
                        <h5>End for rent:</h5><hr>
                        <mark class="subtitle" name="endrent">${hirecar.endrent}</mark>
                    </fieldset>
                    <div class="information">
                        <h3 class="title">Thành Phố Hoạt Động :</h3><br>
                        <p name="location">${hirecar.location}</p>
                    </div>

                    <div class="information">
                        <c:forEach items="${car.getAllByBienSo(car.bienso)}" var="p">
                            <h3 class="title">Giá Thành:</h3><br>
                            <p class="money" name="tienchothue" value="${p.tienchothue}">${p.tienchothue}/Day</p>
                        </c:forEach>
                    </div>
                    <fieldset>
                        <button name="submit" type="submit" id="contact-submit">Up Car</button>
                    </fieldset>
                </div>
            </div>
        </form>
    </body>
</html>
<%@ include file="/includes/footer.jsp" %>