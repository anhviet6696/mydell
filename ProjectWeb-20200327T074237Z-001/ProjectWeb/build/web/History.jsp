<%@page contentType="text/html" pageEncoding="utf-8" %>
<%@ include file="/includes/header_cus.jsp" %>

<!DOCTYPE html>
<html>
    <link rel="stylesheet" href="styles/history.css">
    <head>
        <meta charset="utf-8">
    </head>
    <body  >
    <body>
        <section>
            <div id="tabel1">
                <h1>Lịch sử thuê xe</h1>
                <div class="tbl-header">
                    <table cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Biển Số</th>
                                <th>Hãng Xe</th>
                                <th>Số điện thoại</th>
                                <th>Loại Hình thuê</th>
                                <th>Status</th>
                                <th>Chủ xe</th>
                                <th>Location</th>
                                <th>Ngày thuê</th>
                                <th>Ngày trả</th>
                                <th>Giá</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="tbl-content">
                    <table cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <td>A00003</td>
                                <td>Toyota</td>
                                <td>0913438844</td>
                                <td>FullTIme</td>
                                <td>Đang Thuê</td>
                                <td>Quang</td>
                                <td>Đà Nẵng</td>
                                <td>20/10/2019</td>
                                <td>28/10/2019</td>
                                <td>8000k VND</td>
                            </tr>
                            <tr>
                                <td>A00003</td>
                                <td>Toyota</td>
                                <td>0913438844</td>
                                <td>FullTIme</td>
                                <td>Đang Thuê</td>
                                <td>Quang</td>
                                <td>Đà Nẵng</td>
                                <td>20/10/2019</td>
                                <td>28/10/2019</td>
                                <td>8000k VND</td>
                            </tr>
                            <tr>
                                <td>A00003</td>
                                <td>Toyota</td>
                                <td>0913438844</td>
                                <td>FullTIme</td>
                                <td>Đang Thuê</td>
                                <td>Quang</td>
                                <td>Đà Nẵng</td>
                                <td>20/10/2019</td>
                                <td>28/10/2019</td>
                                <td>8000k VND</td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <section>
            <div id="tabel2">

                <div class="tb2-header">
                    <table cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Bạn chưa đăng kí xe trên hệ thống của chúng tôi.Bạn có muốn đăng kí xe hay không ?</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="tb2-content">
                    <table cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <td><pre><a href="RegisterCar.jsp"><button type=button" id="bt_ls"><p class="p_ls">Có</p></button></a>    <a href="RegisterCar.jsp"><button id="bt_ls"><p class="p_ls">Không</p></button></a> </pre></td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <!--        <section>
                    <div class="thongbao">
                        <h2>Bạn Chưa Có Xe Đăng Ký</h2>
                        <h2>Bạn Có Muốn Đăng Ký Không</h2>
                        <button type="submit">Yes</button>
                        <button type="submit">No</button>
                    </div>
        
                </section>-->
        <script>
            function showtabel1() {
                document.getElementById("tabel1").style.visibility = "visible";
                document.getElementById("tabel2").style.visibility = "hidden";
            }

            function showtabel2() {
                document.getElementById("tabel1").style.visibility = "hidden";
                document.getElementById("tabel2").style.visibility = "visible";
            }
        </script>
        <div class="buttonchange">
            <button type="button" onclick="showtabel1();"><p class="p_ls">Lịch sử thuê xe</p></button>
            <button type="button" onclick="showtabel2();"><p class="p_ls">Lịch sử cho thuê xe</p></button>
        </div>
    </body>


</body>
</html>
<%@ include file="/includes/footer.jsp" %>