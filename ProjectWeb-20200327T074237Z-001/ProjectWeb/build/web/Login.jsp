<%@page contentType="text/html" pageEncoding="utf-8" %>
<%@ include file="/includes/header.jsp" %>

<!DOCTYPE html>
<html>
    <link rel="stylesheet" href="styles/Css.css">
    <head>
        <meta charset="utf-8">
    </head>
    <body class="body_login">
        <div class="login">
            <h1 id="login_h1">Login</h1>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <form action="login" method="POST">
                <input type="text" name="phonenb" placeholder="PhoneNumber" required/>
                <input type="password" name="pass" placeholder="Password" required/>
                <input type="checkbox" name ="checkRemember" ><span id="span_login">Remember me</span>
                <input type="submit" name="" value="Login">
            </form>
        </div>
    </body>
</html>
<%@ include file="/includes/footer.jsp" %>