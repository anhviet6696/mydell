<%@page contentType="text/html" pageEncoding="utf-8" %>
<%@ include file="/includes/header_cus.jsp" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:useBean id="car" class="model.CarTBL" scope="session" />
<jsp:useBean id="hirecar" class="model.HirecarTBL" scope="session" />
<jsp:useBean id="CUSTOMER" class="model.UserTBL" scope="session" />

<!DOCTYPE html>
<html>
        <link rel="stylesheet" href="styles/fix-up.css">
    <head>
       <meta charset="utf-8">
    </head>
<body>

    <div class="container">
        <form id="contact" action="ConfirmUpStatus.jsp" method="post">
            <h3>Form Đăng Bài Cho Thuê</h3>
            <fieldset>
                <select id="Chosebox" name="bienso">
                    <c:forEach var="p" items="${car.getAll(CUSTOMER.uid)}" >
                        <option class ="mauoption" value="${p.bienso}" name="bienso" >${p.bienso}</option>
                    </c:forEach>
                </select>
            </fieldset>
            <fieldset>
                <input placeholder="Thành Phố Hoạt Động" type="text"  name="location" required>
            </fieldset>
            <h5>Time For Rent</h5>
            <fieldset>
               <input placeholder="From : EX: 27/10/2019" type="text"  name="startrent" required>
            </fieldset>
            <fieldset>
                <input placeholder="To : EX: 7/11/2019" type="text" name="endrent" required>
            </fieldset>
               
            <fieldset>
                <textarea placeholder="Type your message here...." tabindex="5" ></textarea>
            </fieldset>
            <fieldset>
                <button name="submit" type="submit" id="contact-submit">Up Car</button>
            </fieldset>
        </form>
        <!-----//end-body---->
    </div>

</body>
</html>
<%@ include file="/includes/footer.jsp" %>

