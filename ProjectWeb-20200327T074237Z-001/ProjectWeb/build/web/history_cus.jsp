<%@page contentType="text/html" pageEncoding="utf-8" %>
<%@ include file="/includes/header_cus.jsp" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:useBean id="oderTBLDB" class="model.OrderTBLDB" scope="session" />
<jsp:setProperty name ="oderTBLDB"  property="*"/>
<jsp:useBean id="CUSTOMER" class="model.UserTBL" scope="session" />
<jsp:setProperty name ="CUSTOMER"  property="*"/>
<jsp:useBean id="hirecar" class="model.HirecarTBL" scope="session" />
<jsp:setProperty name ="hirecar"  property="*"/>
<!DOCTYPE html>
<html>
    <link rel="stylesheet" href="styles/history.css">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <div id="tabel2">
            <section>
                <div id="tabel2a">
                    <h1>a</h1>
                    <div class="tbl-header">
                        <table cellpadding="0" cellspacing="0">
                            <thead>

                                <tr>
                                    <th>Chủ xe</th>
                                    <th>Số điện thoại</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="tbl-content">
                        <table cellpadding="0" cellspacing="0">
                            <tbody>
                                <c:forEach  items="${oderTBLDB.getUserLichSuThue(CUSTOMER.uid)}" var="p">
                                    <tr>
                                        <td>${p.fname}</td>
                                        <td>${p.phonenb}</td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div id="tabel2b">
                    <h1>Lịch sử thuê xe</h1>
                    <div class="tbl-header">
                        <table cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Biển Số</th>
                                    <th>Hãng Xe</th>
                                    <th>Location</th>
                                    <th>Ngày thuê</th>
                                    <th>Ngày trả</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="tbl-content">
                        <table cellpadding="0" cellspacing="0">
                            <tbody>
                                <c:forEach items="${oderTBLDB.getCarLichSuThue(CUSTOMER.uid)}" var="pp">
                                    <tr>
                                        <td>${pp.bienso}</td>
                                        <td>${pp.hangxe}</td>
                                        <td>${pp.location}</td>
                                        <td>${pp.startrent}</td>
                                        <td>${pp.endrent}</td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div id="tabel2c">
                    <h1>a</h1>
                    <div class="tbl-header">
                        <table cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Status</th>
                                    <th>Tổng tiền</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="tbl-content">
                        <table cellpadding="0" cellspacing="0">
                            <tbody>
                                <c:forEach items="${oderTBLDB.getOrderLichSuThue(CUSTOMER.uid)}" var="ppp">
                                    <tr>
                                        <td>Đang Thuê</td>
                                        <td>${ppp.tongtien}</td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
        
            <div id="tabel3">
        <section>
            <div id="tabel3a">
                <h1>a</h1>
                <div class="tbl-header">
                    <table cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Biển Số</th>
                                <th>Hãng Xe</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="tbl-content">
                    <table cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>

                                <td>28/10/2019</td>
                                <td>8000k VND</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div id="tabel3b">
                <h1>Xe Thuê</h1>
                <div class="tbl-header">
                    <table cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Biển Số</th>
                                <th>Hãng Xe</th>
                                <th>Biển Số</th>
                                <th>Hãng Xe</th>
                                <th>Biển Số</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="tbl-content">
                    <table cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <td>28/10/2019</td>
                                <td>8000k VND</td>
                                <td>28/10/2019</td>
                                <td>8000k VND</td>
                                <td>28/10/2019</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div id="tabel3c">
                <h1>a</h1>
                <div class="tbl-header">
                    <table cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Biển Số</th>
                                <th>Hãng Xe</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="tbl-content">
                    <table cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>

                                <td>28/10/2019</td>
                                <td>8000k VND</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>


        


        <script>
            function showtabel1() {
                document.getElementById("tabel2").style.visibility = "visible";
                document.getElementById("tabel3").style.visibility = "hidden";
            }

            function showtabel2() {
                document.getElementById("tabel2").style.visibility = "hidden";
                document.getElementById("tabel3").style.visibility = "visible";
            }
        </script>
        <div class="buttonchange">
            <button type="button" onclick="showtabel1();"><p class="p_ls">Lịch sử thuê xe</p></button>
            
        </div>
    </body>
</html>
<%@ include file="/includes/footer.jsp" %>