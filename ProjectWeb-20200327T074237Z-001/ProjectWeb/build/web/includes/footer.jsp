<%@page contentType="text/html" pageEncoding="utf-8" %>
<footer>
    <div class="wrapper">
        <div class="column">

            <div class="text_footer">
                <h4>Contact with us</h4>
                <p>Thanks For all thing , We will never forget your support</p>
                <div id="social-links">
                    <a href="#"><img src="https://images.vexels.com/media/users/3/137253/isolated/preview/90dd9f12fdd1eefb8c8976903944c026-facebook-icon-logo-by-vexels.png" width="40px" height="40px"></a>
                    <a href="#"><img src="https://images.vexels.com/media/users/3/137419/isolated/preview/b1a3fab214230557053ed1c4bf17b46c-twitter-icon-logo-by-vexels.png" width="40px" height="40px"></a>
                    <a href="#"><img src="https://lh3.googleusercontent.com/L6jXqpFlxBsakV5StgFPQt4QkEF5G4_GvIbuNY9t0GL0uXIhBUZoDe4u_6cAsQaoOlID" width="40px" height="40px"></a>
                </div>
            </div>
        </div>

        <div class="column">

            <div class="text_footer">
                <h4>Information</h4>
                <p>
                    About<br /> Service
                    <br /> Conditions
                    <br /> Become a partner<br /> Best Price Guarantee<br /> Privacy and Prolicy
                </p>
            </div>
        </div>

        <div class="column">

            <div class="text_footer">
                <h4>Customer Support</h4>
                <p class="links">
                    <a href="#">FAQ</a><br />
                    <a href="#">Payment Option</a><br />
                    <a href="#">Booking Tips</a><br />
                    <a href="#">How its work</a><br />
                    <a href="#">Contact us</a><br />
                    <a href="#">How to use it</a>
                </p>
            </div>
        </div>

        <div class="column">
            <div class="text_footer">
                <div class="haveQuestion">
                    <h4>Have a Questions</h4>
                    <p class="links">
                        <a href="https://www.facebook.com/thanh.simon.7">Vinoto@gmail.com</a><br>
                        <br> Phone Number :+ 999999999
                        <br>
                        <br /> Location : Khu Đô Thị FPT, Thành Phố Đà Nẵng
                    </p>
                </div>
            </div>
        </div>

        <div id="footer_l"> This Web Side Make By 4 HANDSOME MAN : Quang ,Viet ,Huy ,Vy </div>


    </div>
</footer>
</body>
</html>