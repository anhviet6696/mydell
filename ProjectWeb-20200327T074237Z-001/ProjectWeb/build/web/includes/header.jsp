<%@page contentType="text/html" pageEncoding="utf-8" %>

<!doctype html>

<html>
    <head>
        <meta charset="utf-8">
        <title>Vin Oto - Niềm tự hào của chúng tôi ! </title>
        <link rel="shortcut icon" href="images/favicon.ico">
        <link rel="stylesheet" href="styles/Css.css">

    </head>
    <% request.setCharacterEncoding("UTF-8");%>
    <body class="body_header">
        <header>
            <div class="Head">
                <div class=logo>
                    <a href="Index.jsp">
                        <img src="images/logo.png" width="120px" height=45px></a>
                </div>

                <div class=phoneIcon>
                    <img src="images/phoneLogo.png" width="25px" height=25px>
                </div>

                <div class="textNumber">
                    <p>0988888888</p>
                </div>


                <div class=email>
                    <img src="images/mail.png" width="25px" height=25px>
                </div>

                <div class="textEmail">
                    <p>vinOtosupport@gmail.com</p>
                </div>


                <div class=facebook>
                    <a href="https://www.facebook.com/Vin-OTO-107100654062033/" target="_blank">
                        <img src="images/facebook.png" width="25px" height=25px>
                    </a>
                </div>

                <div class="textFacebook">
                    <p>Vin Oto</p>
                </div>


                <div class="dangkydangnhap">
                    <button  class=dangky><a id="a_head" href="Register.jsp">Đăng Ký</a></button>
                    <button class=dangnhap><a id="a_head"  href="Login.jsp">Đăng Nhập</a></button>
                </div>
            </div>

        </header>
    </body>
</html>