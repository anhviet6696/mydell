/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionSupport;
import model.CarTBLDB;

/**
 *
 * @author Quang
 */
public class ChapnhanxedkAction extends ActionSupport {
   
    public ChapnhanxedkAction() {
    }
    
   private String bienso;

    public String getBienso() {
        return bienso;
    }

    public void setBienso(String bienso) {
        this.bienso = bienso;
    }

    
    public String execute() throws Exception {
        CarTBLDB car = new CarTBLDB();
        car.acceptStatusCar(bienso);
        return SUCCESS;
        
    }
}
