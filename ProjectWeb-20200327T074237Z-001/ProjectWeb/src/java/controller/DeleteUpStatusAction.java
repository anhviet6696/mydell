package controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.Map;
import model.CarTBL;
import model.HirecarTBL;
import model.UserTBL;

public class DeleteUpStatusAction extends ActionSupport {

    private String bienso;

    public DeleteUpStatusAction() {
    }

    public String getBienso() {
        return bienso;
    }

    public void setBienso(String bienso) {
        this.bienso = bienso;
    }

public String execute() throws Exception {
        Map<String, Object> session = ActionContext.getContext().getSession();
        CarTBL c = (CarTBL) session.get("car");
        UserTBL us = (UserTBL) session.get("CUSTOMER");
        HirecarTBL hc = (HirecarTBL) session.get("hirecar");
        ArrayList<HirecarTBL> list = hc.getAllHirecar(us.getUid());
        for (HirecarTBL s : list) {
            if(bienso.equals(s.getBienso())){
            if (s.getHirecarStt().equals("Waiting") ) {
               hc.updateHidden(s.getBienso());
                System.out.println(s.getBienso());
               c.updateUnPosted(s.getBienso());
                    return SUCCESS;
                }
            if (s.getHirecarStt().equals("Order")) {
                session.put("mess","Cảm ơn bạn đã tin tưởng vào website của chúng tôi.\n" +
"Xe của bạn đã được order. Không thể Delete bài đăng này !");
                return ERROR;
                
            }
            }
        
       
    }
         return SUCCESS;
}
}
