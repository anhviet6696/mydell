
package controller;

import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionContext;
import java.util.Map;
import model.UserTBLDB;
import model.UserTBL;

public class LoginAction {
     private String phonenb;
    private String pass;
    private UserTBL customer=new UserTBL();

    public String getPhonenb() {
        return phonenb;
    }

    public void setPhonenb(String phonenb) {
        this.phonenb = phonenb;
    }
    

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
    public LoginAction() {
    }
    
    public String execute() throws Exception {
        Map<String, Object> session = ActionContext.getContext().getSession();
        UserTBL user = new UserTBL();
        UserTBLDB st = new UserTBLDB();
        if (st.getuid(phonenb) == 0) {
             session.put("mess", "Tài khoản của bạn có lẽ chưa có trên hệ thống ! !"+"\n Hoặc tài khoản và mật khậu bạn nhập đã sai !");
            return ERROR;
        }
        UserTBLDB userDB = new UserTBLDB();
        customer = userDB.getUserRight(userDB.getuid(phonenb));
        if ((customer = user.login(phonenb, pass)) != null) {
            if (customer.getUserRight() == 1) {
                session.put("CUSTOMER", customer);
                return "Admin";
            } else {
                if(customer.getLock().equals("Unlock")){
                     session.put("CUSTOMER", customer);
                     return "Customer";
                }
                else{
                    session.put("mess", "Tài khoản của bạn đã bị khóa !\n"+"\n Chúc bạn may mắn lần sau !");
                    return "lock";
                }
               
            }
        }else{
            session.put("mess", "Tài khoản của bạn có lẽ chưa có trên hệ thống ! !"+"\n Hoặc tài khoản và mật khậu bạn nhập đã sai !");
            return "mksai";
             
        }
      
    }
      public String logout() {
        Map<String, Object> session = ActionContext.getContext().getSession();
        if (session.containsKey("CUSTOMER")) {
            session.remove("CUSTOMER");
            ((org.apache.struts2.dispatcher.SessionMap) session).invalidate();
        }
        return SUCCESS;
    }
    
}
