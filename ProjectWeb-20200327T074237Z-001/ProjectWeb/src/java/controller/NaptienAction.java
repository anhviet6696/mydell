/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.util.Map;
import model.LognoptienTBL;
import model.UserTBL;
import model.UserTBLDB;

/**
 *
 * @author letan
 */
public class NaptienAction extends ActionSupport implements ModelDriven<LognoptienTBL>  {
    LognoptienTBL nt =  new LognoptienTBL();
    
    private String phonenb;
    private int tien;
    public NaptienAction() {
    }

    public String getPhonenb() {
        return phonenb;
    }

    public void setPhonenb(String phonenb) {
        this.phonenb = phonenb;
    }

    public int getTien() {
        return tien;
    }

    public void setTien(int tien) {
        this.tien = tien;
    }
    
    
    
    public String execute() throws Exception {
       Map<String,Object> session = ActionContext.getContext().getSession();
       LognoptienTBL np = new LognoptienTBL();
       UserTBL user = new UserTBL();
       UserTBLDB userDB =new UserTBLDB();
       session.put("mess","");
       UserTBL s = (UserTBL)session.get("CUSTOMER");
        if(np.addMoney(s.getUid(),nt.getSotaikhoan(), nt.getSotien(), nt.getMapin(), nt.getPhuongthuc()) == true){
           if(user.updateTien(nt.getSotien()) == true){
               session.put("mess","Cảm ơn bạn đã tin tưởng vào website của chúng tôi.\n" +
"Bạn đã nạp tiền thành công. Trân trọng cảm ơn !");
             return SUCCESS;
         }
        }
        return ERROR;
    }
    @Override
    public LognoptienTBL getModel() {
      return nt;
    }
    
}