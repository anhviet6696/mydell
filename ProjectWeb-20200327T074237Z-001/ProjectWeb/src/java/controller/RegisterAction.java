/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.util.Map;
import model.UserTBLDB;
import model.UserTBL;

/**
 *
 * @author letan
 */
public class RegisterAction extends ActionSupport implements ModelDriven<UserTBL>{
    UserTBL user = new UserTBL();

    public UserTBL getUser() {
        return user;
    }

    public void setUser(UserTBL user) {
        this.user = user;
    }
    
    public RegisterAction() {
    }
    
    public String execute() throws Exception {
        UserTBLDB us = new UserTBLDB();
        if(us.getuid(user.getPhonenb())==0){
           if(us.addNewUser(user.getPhonenb(), user.getPass(), user.getFname(), user.getEmail(), user.getAddress())==true){
            return SUCCESS;
        }}
       Map<String,Object> session = ActionContext.getContext().getSession();
        session.put("mess","Cảm ơn bạn đã tin tưởng vào website của chúng tôi.\n" +
"Số điện thoại của bạn đã được đăng kí trên hệ thống. Trân trọng cảm ơn !");
        return ERROR;
    }

    @Override
    public UserTBL getModel() {
      return user;
    }
    
}
