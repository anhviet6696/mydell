/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import static com.opensymphony.xwork2.Action.INPUT;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import javax.servlet.ServletContext;
import model.CarTBLDB;
import model.UserTBL;
import org.apache.struts2.util.ServletContextAware;

/**
 *
 * @author letan
 */
public class RegisterCarAction extends ActionSupport implements ServletContextAware {

    private String bienso;
    private int socho;
    private int tienchothue, tiencoc;
    private String hangxe;
    private String loainguyenlieu;
    private String hopso;
    private String noiohientai;
    private String mota;
    private File file;
    private String fileContentType;
    private String fileFileName;
    private File file1;
    private String file1ContentType;
    private String file1FileName;
    private String[] tinhnangxe;
    private ServletContext context;

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getFileContentType() {
        return fileContentType;
    }

    public void setFileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
    }

    public String getFileFileName() {
        return fileFileName;
    }

    public void setFileFileName(String fileFileName) {
        this.fileFileName = fileFileName;
    }

    public File getFile1() {
        return file1;
    }

    public void setFile1(File file1) {
        this.file1 = file1;
    }

    public String getFile1ContentType() {
        return file1ContentType;
    }

    public void setFile1ContentType(String file1ContentType) {
        this.file1ContentType = file1ContentType;
    }

    public String getFile1FileName() {
        return file1FileName;
    }

    public void setFile1FileName(String file1FileName) {
        this.file1FileName = file1FileName;
    }

    public ServletContext getContext() {
        return context;
    }

    public void setContext(ServletContext context) {
        this.context = context;
    }

    public RegisterCarAction() {
    }

    public String execute() throws Exception {
       UserTBL user = (UserTBL) ActionContext.getContext().getSession().get("CUSTOMER");
        String rs = "";
        Map<String,Object> session = ActionContext.getContext().getSession();
        session.put("mess","");
        for (int i = 0; i < tinhnangxe.length; i++) {
            rs += tinhnangxe[i] + ",";
        }
        rs = rs.substring(0, rs.length() - 1);
        CarTBLDB cardb = new CarTBLDB();
        if(cardb.getbienso(bienso)!=0){
            session.put("mess","Biển số xe của bạn đã được đăng kí trên hệ thống, vui lòng đăng kí xe khác. Trân Trọng !");
            return ERROR;
            
        }
        String pathXe = user.getUid()+"_anhxe_" + getFileFileName();
        String pathCV= user.getUid()+"_anhcavatxe_"+ getFile1FileName();
        System.out.println(pathXe);
        cardb.addNewCar(user.getUid(), bienso, socho, tienchothue, hangxe, loainguyenlieu, hopso, tiencoc, noiohientai, mota, rs,pathXe,pathCV);
        try {
            FilesUtil.saveFile(getFile(), user.getUid()+"_anhxe_" + getFileFileName(), "C:\\Users\\tranq\\OneDrive\\Desktop\\Chuyên ngành 5\\LAB231\\mydell\\ProjectWeb-20200327T074237Z-001\\ProjectWeb\\web\\images");
            FilesUtil.saveFile(getFile1(), user.getUid()+"_anhcavatxe_" + getFile1FileName(), "C:\\Users\\tranq\\OneDrive\\Desktop\\Chuyên ngành 5\\LAB231\\mydell\\ProjectWeb-20200327T074237Z-001\\ProjectWeb\\web\\images");
        } catch (IOException e) {
            e.printStackTrace();
            return INPUT;
        }
        session.put("mess","");
        session.put("mess","Cảm ơn bạn đã tin tưởng vào website của chúng tôi.\n" +
"Đơn đăng kí xe của bạn đã gửi thành công. Vui lòng đến : 14 Doãn Uẩn,Sơn Trà Đà Nẵng vào Thứ 2,4,6 mỗi tuần để kiểm tra xe. Trân trọng cảm ơn !");
        return SUCCESS;
    }

    @Override
    public void setServletContext(ServletContext sc) {
        this.context = sc;
    }

    public String getBienso() {
        return bienso;
    }

    public void setBienso(String bienso) {
        this.bienso = bienso;
    }

    public int getSocho() {
        return socho;
    }

    public void setSocho(int socho) {
        this.socho = socho;
    }

    public int getTienchothue() {
        return tienchothue;
    }

    public void setTienchothue(int tienchothue) {
        this.tienchothue = tienchothue;
    }

    public int getTiencoc() {
        return tiencoc;
    }

    public void setTiencoc(int tiencoc) {
        this.tiencoc = tiencoc;
    }

    public String[] getTinhnangxe() {
        return tinhnangxe;
    }

    public void setTinhnangxe(String[] tinhnangxe) {
        this.tinhnangxe = tinhnangxe;
    }

  

    public String getHangxe() {
        return hangxe;
    }

    public void setHangxe(String hangxe) {
        this.hangxe = hangxe;
    }

    public String getLoainguyenlieu() {
        return loainguyenlieu;
    }

    public void setLoainguyenlieu(String loainguyenlieu) {
        this.loainguyenlieu = loainguyenlieu;
    }

    public String getHopso() {
        return hopso;
    }

    public void setHopso(String hopso) {
        this.hopso = hopso;
    }

    public String getNoiohientai() {
        return noiohientai;
    }

    public void setNoiohientai(String noiohientai) {
        this.noiohientai = noiohientai;
    }

    public String getMota() {
        return mota;
    }

    public void setMota(String mota) {
        this.mota = mota;
    }

}
