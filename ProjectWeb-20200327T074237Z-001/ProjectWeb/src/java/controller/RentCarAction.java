/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.util.ArrayList;
import java.util.Map;
import model.LognoptienTBL;
import model.OrderTBL;
import model.OrderTBLDB;
import model.UserTBL;

/**
 *
 * @author letan
 */
public class RentCarAction extends ActionSupport {



    
    public String execute() throws Exception {
        Map<String,Object> session = ActionContext.getContext().getSession();
         UserTBL s = (UserTBL)session.get("CUSTOMER");
        OrderTBL od = (OrderTBL)session.get("order");
        LognoptienTBL log = new LognoptienTBL();
        if(s==null){
            return "login";
        }
        if(log.checktongtien(s.getUid(), od.getTongtien())== false){
            return "naptien";
        }
        log.subMoneyorder(s.getUid(), od.getTongtien());
        OrderTBLDB save = new OrderTBLDB();
        save.addOrder(s.getUid(),od.getHirecarid(),od.getTongtien(),od.getTienchuxe(),od.getFee());
        System.out.println(od.getFee());
        session.put("mess","Cảm ơn bạn đã tin tưởng vào website của chúng tôi.\n" +
"Bạn đã đặt xe thành công. Trân trọng cảm ơn !");
        return SUCCESS;
    }


//    

 
}
