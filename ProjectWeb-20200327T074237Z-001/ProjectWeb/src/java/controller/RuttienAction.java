/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;
import model.LognoptienTBL;
import model.UserTBL;

/**
 *
 * @author letan
 */

    
public class RuttienAction extends ActionSupport {
    private int tien;
    private String pass;
    UserTBL user = new UserTBL();
    public RuttienAction() {
    }

    public int getTien() {
        return tien;
    }

    public void setTien(int tien) {
        this.tien = tien;
    }

    public UserTBL getUser() {
        return user;
    }

    public void setUser(UserTBL user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
    
    @Override
    public String execute() throws Exception {
       Map<String,Object> session = ActionContext.getContext().getSession();
       LognoptienTBL log = new LognoptienTBL();
       UserTBL s = (UserTBL)session.get("CUSTOMER");
       session.put("mess","");
       if(s.getPass().equals(pass)){
           if(tien >= 50000 && log.checksotien(s.getUid(), tien)){
           log.subMoney(s.getUid(),tien);
            session.put("mess","Cảm ơn bạn đã tin tưởng vào website của chúng tôi.\n" +
"Bạn đã rút tiền thành công. Trân trọng cảm ơn !");
           return SUCCESS;
          }
       }
       session.put("mess","Số tiền trong tài khoản của bạn không để thực hiện giao dịch này !");
        return ERROR;
    }}
