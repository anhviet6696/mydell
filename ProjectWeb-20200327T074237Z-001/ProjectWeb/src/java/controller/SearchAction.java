/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.util.ArrayList;
import java.util.Map;
import model.HirecarTBL;
import model.LognoptienTBL;
import model.UserTBL;

/**
 *
 * @author letan
 */
public class SearchAction extends ActionSupport implements ModelDriven<HirecarTBL> {

    HirecarTBL hire = new HirecarTBL();
    private int socho;

    public HirecarTBL getHire() {
        return hire;
    }

    public void setHire(HirecarTBL hire) {
        this.hire = hire;
    }

    public int getSocho() {
        return socho;
    }

    public void setSocho(int socho) {
        this.socho = socho;
    }

    public SearchAction() {
    }

    @Override
    public String execute() throws Exception {
        Map<String, Object> session = ActionContext.getContext().getSession();
//        UserTBL s = (UserTBL) session.get("CUSTOMER");
//        if(s==null){
//            
//            return "login";
//        }
        return SUCCESS;
    }

    @Override
    public HirecarTBL getModel() {
        return hire;
    }

}
