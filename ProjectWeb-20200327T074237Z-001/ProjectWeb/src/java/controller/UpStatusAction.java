package controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.util.ArrayList;
import java.util.Map;
import model.CarTBL;
import model.HirecarTBL;

public class UpStatusAction extends ActionSupport {

    private String bienso;
    private int giaxechothue;
    private int tienchothue;
    private String startrent;
    private String endrent;
    private String location;
    private String comment;
    private CarTBL car = new CarTBL();
    private HirecarTBL hcar = new HirecarTBL();

    public UpStatusAction() {
    }

    public String getStartrent() {
        return startrent;
    }

    public void setStartrent(String startrent) {
        this.startrent = startrent;
    }

    public String getEndrent() {
        return endrent;
    }

    public void setEndrent(String endrent) {
        this.endrent = endrent;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getGiaxechothue() {
        return giaxechothue = tienchothue;
    }

    public void setGiaxechothue(int giaxechothue) {
        this.giaxechothue = giaxechothue;
    }

    public String getBienso() {
        return bienso;
    }

    public void setBienso(String bienso) {
        this.bienso = bienso;
    }

    public int getTienchothue() {
        return tienchothue;
    }

    public void setTienchothue(int tienchothue) {
        this.tienchothue = tienchothue;
    }

    public CarTBL getCar() {
        return car;
    }

    public void setCar(CarTBL car) {
        this.car = car;
    }

    public HirecarTBL getHcar() {
        return hcar;
    }

    public void setHcar(HirecarTBL hcar) {
        this.hcar = hcar;
    }



    @Override
    public String execute() throws Exception {
        Map<String, Object> session = ActionContext.getContext().getSession();
        CarTBL c = (CarTBL) session.get("car");
        HirecarTBL hc = (HirecarTBL) session.get("hirecar");
        hc.upStatus(c.getBienso(), hc.getStartrent(), hc.getEndrent(), hc.getLocation(), hc.getTienchothue());
        c.updatePosted(c.getBienso());
        hc.updateMoney(c.getBienso(), getTienchothue());
        System.out.println(getTienchothue());
        return SUCCESS;

    }

}
