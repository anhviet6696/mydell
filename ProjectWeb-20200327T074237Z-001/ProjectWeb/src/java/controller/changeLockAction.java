/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;
import model.UserTBL;
import model.UserTBLDB;
import static org.apache.tomcat.jni.User.uid;

/**
 *
 * @author Quang
 */
public class changeLockAction extends ActionSupport {
    
    public changeLockAction() {
    }
    private int uid;

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }
    
    public String execute() throws Exception {
       UserTBLDB adminUser = new UserTBLDB();
       if((adminUser.updateLock(uid)) == true){
           return SUCCESS;
       }else
           return ERROR;
    }
    
}
