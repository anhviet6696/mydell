/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.util.logging.Logger;
import java.util.Map;
import model.UserTBL;
import model.UserTBLDB;

/**
 *
 * @author Quang
 */
public class changeUnLockAction extends ActionSupport {

    public changeUnLockAction() {

    }
    private int uid;

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String execute() throws Exception {
        UserTBLDB adminUser = new UserTBLDB();
        if ((adminUser.updateUnLock(uid)) == true) {
            return SUCCESS;
        } else {
            return ERROR;
        }
    }

}
