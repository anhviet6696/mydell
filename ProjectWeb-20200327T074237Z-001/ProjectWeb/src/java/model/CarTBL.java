package model;

import java.io.File;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import static model.Databaseinfo.dbURL;
import static model.Databaseinfo.driverName;
import static model.Databaseinfo.passDB;
import static model.Databaseinfo.userDB;
import model.UserTBLDB;

public class CarTBL extends UserTBL implements Databaseinfo, Serializable {

    private String bienso;
    private int socho;
    private String hangxe;
    private String loainguyenlieu;
    private String hopso;
    private String thuoctinh;
    private String filenameXe;
    private String pathxe;
    private String filenameCV;
    private String pathCV;
    private String status;
    private String tinhnangxe;
    private String noiohientai;
    private String startrent;
    private String endrent;
    private int tienchothue;
    private int tiencoc;
    private Date ngaykiemtra;
    private String mota;
    private String upStt;
    private String startrenthour;
    private String endrenthour;
    private String statusAccepted;

    //--
    UserTBL user = new UserTBL();

    public CarTBL() {
    }
public CarTBL(String bienso, String hangxe) {
        this.bienso = bienso;
        this.hangxe = hangxe;
    }

    public CarTBL(String bienso) {
        this.bienso = bienso;
    }
    public CarTBL(int uid, String name, String phone, String bienso) {
        super(uid, name, phone);
        this.bienso = bienso;
    }
public CarTBL(String bienso, int socho, String hangxe, int tienchothue) {
        this.bienso = bienso;
        this.socho = socho;
        this.hangxe = hangxe;
        this.tienchothue=tienchothue;
    }
    public CarTBL(int socho, String loainguyenlieu, String hopso, String pathxe, String tinhnangxe, int tiencoc, String mota,String hangxe) {
        this.socho = socho;
        this.loainguyenlieu = loainguyenlieu;
        this.hopso = hopso;
        this.pathxe = pathxe;
        this.tinhnangxe = tinhnangxe;
        this.tiencoc = tiencoc;
        this.mota = mota;
        this.hangxe = hangxe;
    }
    public CarTBL(String bienso, int socho, String hangxe, String loainguyenlieu, String hopso, String noiohientai, String status, String tinhnangxe, int tienchothue, String statusAccepted) {
        this.bienso = bienso;
        this.socho = socho;
        this.hangxe = hangxe;
        this.loainguyenlieu = loainguyenlieu;
        this.hopso = hopso;
        this.noiohientai = noiohientai;
        this.status = status;
        this.tinhnangxe = tinhnangxe;
        this.tienchothue = tienchothue;
        this.statusAccepted = statusAccepted;
    }

    public CarTBL(String phonenb, String fname, String email) {
        super(phonenb, fname, email);
    }

    public String getStatusAccepted() {
        return statusAccepted;
    }

    public void setStatusAccepted(String statusAccepted) {
        this.statusAccepted = statusAccepted;
    }

 

    public CarTBL(int socho, String hangxe, int tiencoc) {
        this.socho = socho;
        this.hangxe = hangxe;
        this.tiencoc = tiencoc;
    }

    public CarTBL(String bienso, String startrent, String endrent, int tienchothue) {
        this.bienso = bienso;
        this.startrent = startrent;
        this.endrent = endrent;
        this.tienchothue = tienchothue;
    }

    public CarTBL(int socho, String hangxe, int tiencoc, String pathxe, String bienso) {
        this.socho = socho;
        this.hangxe = hangxe;
        this.tiencoc = tiencoc;
        this.pathxe = pathxe;
        this.bienso = bienso;
    }

    public CarTBL(int uid, String stk, String phone, String pass, String name, String bienso, int socho, String hangxe, String loainguyenlieu, String hopso, int tienchothue, String noiohientai, String tinhnangxe, String status) {
        user = UserTBLDB.getUser(uid);
        uid = user.getUid();
        stk = user.getSotaikhoan();
        phone = user.getPhonenb();
        pass = user.getPass();
        name = user.getFname();
        this.bienso = bienso;
        this.socho = socho;
        this.hangxe = hangxe;
        this.loainguyenlieu = loainguyenlieu;
        this.hopso = hopso;
        this.noiohientai = noiohientai;
        this.tienchothue = tienchothue;
        this.tinhnangxe = tinhnangxe;
        this.status = status;
    }

    public String getBienso() {
        return bienso;
    }

    public void setBienso(String bienso) {
        this.bienso = bienso;
    }

    public int getSocho() {
        return socho;
    }

    public void setSocho(int socho) {
        this.socho = socho;
    }

    public String getHangxe() {
        return hangxe;
    }

    public void setHangxe(String hangxe) {
        this.hangxe = hangxe;
    }

    public String getStartrent() {
        return startrent;
    }

    public void setStartrent(String startrent) {
        this.startrent = startrent;
    }

    public String getEndrent() {
        return endrent;
    }

    public void setEndrent(String endrent) {
        this.endrent = endrent;
    }

    public String getLoainguyenlieu() {
        return loainguyenlieu;
    }

    public void setLoainguyenlieu(String loainguyenlieu) {
        this.loainguyenlieu = loainguyenlieu;
    }

    public String getHopso() {
        return hopso;
    }

    public void setHopso(String hopso) {
        this.hopso = hopso;
    }

    public String getThuoctinh() {
        return thuoctinh;
    }

    public void setThuoctinh(String thuoctinh) {
        this.thuoctinh = thuoctinh;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTinhnangxe() {
        return tinhnangxe;
    }

    public void setTinhnangxe(String tinhnangxe) {
        this.tinhnangxe = tinhnangxe;
    }

    public String getNoiohientai() {
        return noiohientai;
    }

    public void setNoiohientai(String noiohientai) {
        this.noiohientai = noiohientai;
    }

    public int getTienchothue() {
        return tienchothue;
    }

    public void setTienchothue(int tienchothue) {
        this.tienchothue = tienchothue;
    }

    public int getTiencoc() {
        return tiencoc;
    }

    public void setTiencoc(int tiencoc) {
        this.tiencoc = tiencoc;
    }

    public Date getNgaykiemtra() {
        return ngaykiemtra;
    }

    public void setNgaykiemtra(Date ngaykiemtra) {
        this.ngaykiemtra = ngaykiemtra;
    }

    public String getMota() {
        return mota;
    }

    public void setMota(String mota) {
        this.mota = mota;
    }

    public String getUpStt() {
        return upStt;
    }

    public void setUpStt(String upStt) {
        this.upStt = upStt;
    }

    public String getFilenameXe() {
        return filenameXe;
    }

    public void setFilenameXe(String filenameXe) {
        this.filenameXe = filenameXe;
    }

    public String getPathxe() {
        return pathxe;
    }

    public void setPathxe(String pathxe) {
        this.pathxe = pathxe;
    }

    public String getFilenameCV() {
        return filenameCV;
    }

    public void setFilenameCV(String filenameCV) {
        this.filenameCV = filenameCV;
    }

    public String getPathCV() {
        return pathCV;
    }

    public void setPathCV(String pathCV) {
        this.pathCV = pathCV;
    }

    public String getStartrenthour() {
        return startrenthour;
    }

    public void setStartrenthour(String startrenthour) {
        this.startrenthour = startrenthour;
    }

    public String getEndrenthour() {
        return endrenthour;
    }

    public void setEndrenthour(String endrenthour) {
        this.endrenthour = endrenthour;
    }

    public UserTBL getUser() {
        return user;
    }

    public void setUser(UserTBL user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "CarTBL{" + "bienso=" + bienso + ", socho=" + socho + ", hangxe=" + hangxe + ", loainguyenlieu=" + loainguyenlieu + ", hopso=" + hopso + ", thuoctinh=" + thuoctinh + ", tinhnangxe=" + tinhnangxe + ", noiohientai=" + noiohientai + ", startrent=" + startrent + ", endrent=" + endrent + ", tienchothue=" + tienchothue + ", tiencoc=" + tiencoc + ", ngaykiemtra=" + ngaykiemtra + ", mota=" + mota + ", user=" + user + '}' + "status=" + status;
    }

    public ArrayList<CarTBL> getAll(int userID) {
        ArrayList<CarTBL> c = new ArrayList<>();
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select  UserTBL.uid,UserTBL.sotaikhoan,UserTBL.phonenb,UserTBL.pass,UserTBL.fname, CarTBL.bienso,CarTBL.socho,\n"
                    + "CarTBL.hangxe,CarTBL.loainguyenlieu,CarTBL.hopso,CarTBL.tienchothue,CarTBL.noiohientai,CarTBL.tinhnangxe,CarTBL.status\n"
                    + "from UserTBL inner join CarTBL on UserTBL.uid=CarTBL.uid where UserTBL.uid='" + userID + "' and CarTBL.upStt='Unposted' and CarTBL.statusAccepted='Accepted'");
            while (rs.next()) {
                c.add(new CarTBL(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7),
                        rs.getString(8), rs.getString(9), rs.getString(10), rs.getInt(11), rs.getString(12), rs.getString(13), rs.getString(14)));
            }
            con.close();
            return c;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<CarTBL> getAllByBienSo(String bienso) {
        ArrayList<CarTBL> c = new ArrayList<>();
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select  UserTBL.uid,UserTBL.sotaikhoan,UserTBL.phonenb,UserTBL.pass,UserTBL.fname, CarTBL.bienso,CarTBL.socho,\n"
                    + "CarTBL.hangxe,CarTBL.loainguyenlieu,CarTBL.hopso,CarTBL.tienchothue,CarTBL.noiohientai,CarTBL.tinhnangxe,CarTBL.status\n"
                    + "from UserTBL inner join CarTBL on UserTBL.uid=CarTBL.uid where CarTBL.bienso='" + bienso + "'");
            while (rs.next()) {
                c.add(new CarTBL(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7),
                        rs.getString(8), rs.getString(9), rs.getString(10), rs.getInt(11), rs.getString(12), rs.getString(13), rs.getString(14)));
            }
            con.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return c;
    }
    public String getimgxe(String bienso){
          String imgxe="";
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select CarTBL.imgxe from CarTBL where CarTBL.bienso='"+bienso+"'");
            while (rs.next()) {
               imgxe=rs.getString(1);
            }
            con.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return imgxe;
    }
     public String getimgxebyuid(int uid){
          String imgxe="";
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select CarTBL.imgxe from CarTBL where CarTBL.uid='"+uid+"'");
            while (rs.next()) {
               imgxe=rs.getString(1);
            }
            con.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return imgxe;
    }

    public CarTBL updatePosted(String bienso) {
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            PreparedStatement stmt = con.prepareStatement("Update CarTBL set upStt='Posted' where bienso='" + bienso + "'");
            int rs = stmt.executeUpdate();
            con.close();
        } catch (Exception ex) {
            Logger.getLogger(CarTBL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public CarTBL updateUnPosted(String bienso) {
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            PreparedStatement stmt = con.prepareStatement("Update CarTBL set upStt='UnPosted' where bienso='" + bienso + "'");
            int rs = stmt.executeUpdate();
            con.close();
        } catch (Exception ex) {
            Logger.getLogger(CarTBL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static void main(String[] args) {
        CarTBL car = new CarTBL();
        System.out.println(car.getimgxe("92G1-abcd"));
    }
}
