/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import static model.Databaseinfo.dbURL;
import static model.Databaseinfo.driverName;
import static model.Databaseinfo.passDB;
import static model.Databaseinfo.userDB;

/**
 *
 * @author letan
 */
public class CarTBLDB implements Databaseinfo{

 public  boolean addNewCar(int uid,String bienso, int socho,int tienchothue, String hangxe,String nguyenlieu,String hopso,int tiendatcoc,String noiohientai,String mota,String tinhnangxe,String imgxe,String imgCV){
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL,userDB,passDB);
            PreparedStatement stmt= con.prepareStatement("insert into CarTBL(uid,bienso,socho,tienchothue,hangxe,loainguyenlieu,hopso,tiencoc,noiohientai,mota,tinhnangxe,imgxe,imgCV) values"
                    + "("+uid+",'"+bienso+"',"+socho+","+tienchothue+",'"+hangxe+"',N'"+nguyenlieu+"',N'"+hopso+"',"+tiendatcoc+",N'"+noiohientai+"',N'"+mota+"',N'"+tinhnangxe+"','"+imgxe+"','"+imgCV+"')");
            stmt.executeUpdate();
            con.close();
        } catch (Exception ex) {
            Logger.getLogger(UserTBLDB.class.getName()).log(Level.SEVERE, null, ex);
          return false;
        }
           return true;
    }
         public  int getbienso(String bienso){
        int uid=0;
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL,userDB,passDB);
            PreparedStatement stmt= con.prepareStatement("select uid from CarTBL where bienso='"+bienso+"'");
            ResultSet rs = stmt.executeQuery();
             while(rs.next()){
                 uid = rs.getInt(1);
             }
             con.close();
        } catch (Exception ex) {
            Logger.getLogger(UserTBLDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return uid;
    }
         public ArrayList<CarTBL> getAllbystatusAcceptCar() {
        ArrayList<CarTBL> list = new ArrayList<>();
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            PreparedStatement stmt = con.prepareStatement("Select bienso,socho,hangxe,loainguyenlieu,hopso,noiohientai,status,tinhnangxe,tienchothue,statusAccepted from CarTBL where statusAccepted ='Unaccepted'");
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                String bienso = rs.getString(1);
                int socho = rs.getInt(2);
                String hangxe = rs.getString(3);
                String loainguyenlieu = rs.getString(4);
                String hopso = rs.getString(5);
                String noiohientai = rs.getString(6);
                String status = rs.getString(7);
                String tinhnangxe = rs.getString(8);
                int tienchothue = rs.getInt(9);
                String statusAccepted = rs.getString(10);
                list.add(new CarTBL(bienso, socho, hangxe, loainguyenlieu, hopso, noiohientai, status, tinhnangxe, tienchothue, statusAccepted));
            }
            con.close();
        } catch (Exception ex) {
            Logger.getLogger(CarTBL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    public CarTBL acceptStatusCar(String bienso) {
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            PreparedStatement stmt = con.prepareStatement("Update CarTBL set statusAccepted = 'Accepted' where bienso = '" + bienso + "'");
            int rs = stmt.executeUpdate();
            con.close();
        } catch (Exception ex) {
            Logger.getLogger(CarTBL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
     public CarTBL unAcceptStatusCar(String bienso) {
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            PreparedStatement stmt = con.prepareStatement("Update CarTBL set statusAccepted = 'Cancel' where bienso = '" + bienso + "'");
            int rs = stmt.executeUpdate();
            con.close();
        } catch (Exception ex) {
            Logger.getLogger(CarTBL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public static void main(String[] args) {
       CarTBLDB st = new CarTBLDB();
//       st.getbienso("92G1-19927");
           st.addNewCar(1,"92G1-19928",4, 1000000,"toyota 2019","Xăng", "tự động", 15000000, "Đà Nẵng", "Xe vip nhất Sài Gòn","Cửa Trần","test","abc");
}}
