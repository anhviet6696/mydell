/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import static model.Databaseinfo.dbURL;
import static model.Databaseinfo.driverName;
import static model.Databaseinfo.passDB;
import static model.Databaseinfo.userDB;

/**
 *
 * @author letan
 */
public class HirecarTBL extends CarTBL implements Databaseinfo {

    private int Hirecarid;
    private String startrent;
    private String endrent;
    private String comment;
    private String location;
    private int giaxechothue;
    private String hirecarStt;

    public HirecarTBL(String location, int giaxechothue, int socho, String hangxe, int tiencoc, String pathxe, String bienso) {
        super(socho, hangxe, tiencoc, pathxe, bienso);
        this.location = location;
        this.giaxechothue = giaxechothue;
    }

    public HirecarTBL(String hangxe, String bienso, int socho, int tienchothue, String stt) {
        super(bienso, socho, hangxe, tienchothue);
        this.hirecarStt = stt;
    }

    public HirecarTBL(String phonenb, String fname, String email) {
        super(phonenb, fname, email);
    }

    public HirecarTBL(String hangxe, String bienso, int socho, int tienchothue) {
        super(bienso, socho, hangxe, tienchothue);
    }

    public HirecarTBL(String location, int giaxechothue, int socho, String hangxe, int tiencoc) {
        super(socho, hangxe, tiencoc);
        this.location = location;
        this.giaxechothue = giaxechothue;
    }

    public HirecarTBL(String comment, String location, int giaxechothue, int socho, String loainguyenlieu, String hopso, String pathxe, String tinhnangxe, int tiencoc, String mota, String hangxe) {
        super(socho, loainguyenlieu, hopso, pathxe, tinhnangxe, tiencoc, mota, hangxe);
        this.comment = comment;
        this.location = location;
        this.giaxechothue = giaxechothue;
    }

    public HirecarTBL() {
    }

    public String getHirecarStt() {
        return hirecarStt;
    }

    public void setHirecarStt(String hirecarStt) {
        this.hirecarStt = hirecarStt;
    }

    public int getHirecarid() {
        return Hirecarid;
    }

    public void setHirecarid(int Hirecarid) {
        this.Hirecarid = Hirecarid;
    }

    public String getStartrent() {
        return startrent;
    }

    public void setStartrent(String startrent) {
        this.startrent = startrent;
    }

    public String getEndrent() {
        return endrent;
    }

    public void setEndrent(String endrent) {
        this.endrent = endrent;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getGiaxechothue() {
        return giaxechothue;
    }

    public void setGiaxechothue(int giaxechothue) {
        this.giaxechothue = giaxechothue;
    }
    public HirecarTBL(String bienso, String hangxe, String location, String startrent, String endrent) {
        super(bienso, hangxe);
        this.startrent = startrent;
        this.endrent = endrent;
        this.location = location;
    }

    @Override
    public String toString() {
        return "HirecarTBL{" + "Hirecarid=" + Hirecarid + ", startrent=" + startrent + ", endrent=" + endrent + ", comment=" + comment + ", location=" + location + ", giaxechothue=" + giaxechothue + ", hirecarStt=" + hirecarStt + '}';
    }

    public boolean upStatus(String bienso, String starent, String endrent, String location, int giachothue) {
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            PreparedStatement stmt = con.prepareStatement("insert into HirecarTBL (bienso,startrent,endrent,location,giaxechothue) values('" + bienso + "','" + starent + "','" + endrent + "',N'" + location + "'," + giachothue + ")");
            stmt.executeUpdate();
            con.close();

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public ArrayList<HirecarTBL> getAllHirecar(int uid) {
        ArrayList<HirecarTBL> c = new ArrayList<>();
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select carTBL.hangxe,HirecarTBL.bienso ,CarTBL.socho,HirecarTBL.giaxechothue ,HirecarTBL.HirecarStt from UserTBL inner join CarTBL on UserTBL.uid=CarTBL.uid inner join HirecarTBL on CarTBL.bienso=HirecarTBL.bienso where CarTBL.upStt='Posted' and HirecarTBL.HirecarStt not in ( 'Hidden' ) and UserTBL.uid='" + uid + "'");
            while (rs.next()) {
                c.add(new HirecarTBL(rs.getString(1), rs.getString(2), rs.getInt(3), rs.getInt(4), rs.getString(5)));
            }
            con.close();
            return c;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public int updateHidden(String bienso) {// xoa bai dang\
        int rs=0;
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            PreparedStatement stmt = con.prepareStatement("Update  HirecarTBL set hirecarStt='Hidden' where Hirecarid in (select HirecarTBL.Hirecarid from HirecarTBL inner join CarTBL on HirecarTBL.bienso=CarTBL.bienso "
                    + "where carTBL.upStt='Posted' and HirecarTBL.hirecarStt='Waiting' and HirecarTBL.bienso='"+bienso + "')");
            stmt.executeUpdate();
            rs=1;
            con.close();
        } catch (Exception ex) {
            Logger.getLogger(HirecarTBL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }

    public int updateMoney(String bienso, int tienchothue) {
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            PreparedStatement stmt = con.prepareStatement("update HirecarTBL set HirecarTBL.giaxechothue=CarTBL.tienchothue\n"
                    + "from HirecarTBL inner join CarTBL on CarTBL.bienso=HirecarTBL.bienso\n"
                    + "where HirecarTBL.bienso='" + bienso + "'");
            int rs = stmt.executeUpdate();
            con.close();
        } catch (Exception ex) {
            Logger.getLogger(CarTBL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

//    public ArrayList<HirecarTBL> getAllHirecar(String bienso) {
//        ArrayList<HirecarTBL> c = new ArrayList<>();
//        try {
//            Class.forName(driverName);
//            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
//            Statement stmt = con.createStatement();
//            ResultSet rs = stmt.executeQuery("select  UserTBL.uid,UserTBL.sotaikhoan,UserTBL.phonenb,UserTBL.pass,UserTBL.fname, CarTBL.bienso,CarTBL.socho,\n"
//                    + "CarTBL.hangxe,CarTBL.loainguyenlieu,CarTBL.hopso,CarTBL.tienchothue,CarTBL.noiohientai,CarTBL.tinhnangxe,CarTBL.status\n"
//                    + "from UserTBL inner join CarTBL on UserTBL.uid=CarTBL.uid where UserTBL.uid='" + userID + "' and CarTBL.status='Waiting'");
//            while (rs.next()) {
//                c.add(new CarTBL(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7),
//                        rs.getString(8), rs.getString(9), rs.getString(10), rs.getInt(11), rs.getString(12), rs.getString(13), rs.getString(14)));
//            }
//            con.close();
//            return c;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
    public ArrayList<HirecarTBL> searchCar(String startrent, String endrent, int socho, String location) {
        ArrayList<HirecarTBL> c = new ArrayList<>();
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select HirecarTBL.location,HirecarTBL.giaxechothue ,CarTBL.socho,CarTBL.hangxe,CarTBL.tiencoc,CarTBL.imgxe,CarTBL.bienso from CarTBL inner join "
                    + "HirecarTBL on CarTBL.bienso=HirecarTBL.bienso where startrent <='" + startrent
                    + "'and endrent>='" + endrent + "'and hirecarStt='Waiting' and socho=" + socho + " and Location like N'%" + location + "%'");
            while (rs.next()) {
                c.add(new HirecarTBL(rs.getString(1), rs.getInt(2), rs.getInt(3), rs.getString(4), rs.getInt(5), rs.getString(6), rs.getString(7)));
            }
            con.close();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return c;
    }

    public ArrayList<HirecarTBL> getresulthirecarsearch(String bienso) {
        ArrayList<HirecarTBL> c = new ArrayList<>();
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select HirecarTBL.comment,HirecarTBL.location,HirecarTBL.giaxechothue,carTBL.socho,carTBL.loainguyenlieu, carTBL.hopso,"
                    + "carTBL.imgxe,carTBL.tinhnangxe,carTBL.tiencoc,carTBL.mota,carTBL.hangxe from HirecarTBL \n"
                    + "inner join CarTBL on CarTBL.bienso=HirecarTBL.bienso where HirecarTBL.bienso='" + bienso + "'");
            while (rs.next()) {
                c.add(new HirecarTBL(rs.getString(1), rs.getString(2), rs.getInt(3), rs.getInt(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getInt(9), rs.getString(10), rs.getString(11)));
            }
            con.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return c;
    }

    public ArrayList<HirecarTBL> getresultchuxesearch(String bienso) {
        ArrayList<HirecarTBL> c = new ArrayList<>();
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select UserTBL.phonenb,UserTBL.fname,UserTBL.email from UserTBL where UserTBL.uid in\n"
                    + "(select CarTBL.uid from CarTBL inner join HirecarTBL on HirecarTBL.bienso=CarTBL.bienso where CarTBL.bienso='" + bienso + "')");
            while (rs.next()) {
                c.add(new HirecarTBL(rs.getString(1), rs.getString(2), rs.getString(3)));
            }

            con.close();
            return c;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public int getuidbybienso(String bienso) {
        int uid = 0;
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(" select uid from HirecarTBL where bienso='" + bienso + "')");
            while (rs.next()) {
                uid = rs.getInt(1);
            }
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return uid;
    }

    public long caculateday(String startrent, String endrent) {
        long day = 0;
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-mm-dd");
        try {
            Date udob = sdf1.parse(startrent);
            Date udob2 = sdf1.parse(endrent);
            long gettime = udob2.getTime() - udob.getTime();
            day = (long) ((long) gettime / (1000.0 * 60 * 60 * 24));
            System.out.println(day);
        } catch (Exception ex) {
            Logger.getLogger(HirecarTBL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return day;
    }

    public double caculatemoney(int tienchothue, long day) {
        double money = 0;
        money = (tienchothue * day) + (tienchothue * day) * 0.2;
        return money;
    }

    public double caculatefee(int tienchothue, long day) {
        double money = 0;
        money = (tienchothue * day) * 0.2;
        return money;
    }

    public int gethcid(String bienso) {
        int hcid = 0;
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(" select Hirecarid from HirecarTBL where bienso='" + bienso + "'");
            while (rs.next()) {
                hcid = rs.getInt(1);
            }
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hcid;
    }

    public static void main(String[] args) {
        HirecarTBL hc = new HirecarTBL();
//         ArrayList<HirecarTBL> c = new ArrayList<>();
//        System.out.println(hc.searchCar("2019-12-24", "2019-12-25", 4, "Đà Nẵng"));
//        System.out.println(hc.upStatus("92G1-abc", "2019-12-23 7:00", "2019-12-28 9:05", "Đà Nẵng", 700000));
//        System.out.println(hc.checksearchCar("2019-12-24", "2019-12-24",4,"Đà Nẵng"));
//        ArrayList<HirecarTBL> list = new ArrayList<>();
//        list = hc.getAllHirecar(1);
//        for (HirecarTBL s : list) {
//            System.out.println(s.getTienchothue());
//        }
//        System.out.println(hc.getresultchuxesearch("92G1-abc"));
//        
//    
//long day = hc.caculateday("2019-11-2","2019-11-3");
//        System.out.println(hc.caculatemoney(100000,day));
//        String sr=String.valueOf(hc.caculatemoney(100000,day));
//        System.out.println(sr = sr.substring(0, sr.length() - 5));
//        System.out.println(hc.gethcid("92G1-abcd"));
//        System.out.println(hc.updateHidden("92G1-abc"));

    }
}
