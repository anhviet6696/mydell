
package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LognoptienTBL implements Databaseinfo{
    private int accno;
    private int uid;
    private Date ngaynop;
    private int sotien;
    private String phuongthuc;
    private String mapin;
    private String sotaikhoan;

    public LognoptienTBL() {
    }

    public LognoptienTBL(int accno, int uid, Date ngaynop, int sotien, String phuongthuc) {
        this.accno = accno;
        this.uid = uid;
        this.ngaynop = ngaynop;
        this.sotien = sotien;
        this.phuongthuc = phuongthuc;
    }

    public int getAccno() {
        return accno;
    }

    public void setAccno(int accno) {
        this.accno = accno;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public Date getNgaynop() {
        return ngaynop;
    }

    public void setNgaynop(Date ngaynop) {
        this.ngaynop = ngaynop;
    }

    public int getSotien() {
        return sotien;
    }

    public void setSotien(int sotien) {
        this.sotien = sotien;
    }

    public String getPhuongthuc() {
        return phuongthuc;
    }

    public void setPhuongthuc(String phuongthuc) {
        this.phuongthuc = phuongthuc;
    }

    public String getMapin() {
        return mapin;
    }

    public void setMapin(String mapin) {
        this.mapin = mapin;
    }

    public String getSotaikhoan() {
        return sotaikhoan;
    }

    public void setSotaikhoan(String sotaikhoan) {
        this.sotaikhoan = sotaikhoan;
    }
    
    
    public boolean checksotien(int uid,int tienthue){
        int tien=0;
         try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL,userDB,passDB);
            PreparedStatement stmt= con.prepareStatement("select UserTBL.tien from UserTBL where uid='"+uid+"'");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                tien= rs.getInt(1);
            }
            if(tien>tienthue){
                return true;
            }
            con.close();
        } catch (Exception ex) {
            Logger.getLogger(UserTBLDB.class.getName()).log(Level.SEVERE, null, ex);
        }
           return false;
    }
      public boolean checktongtien(int uid,double tienthue){
        int tien=0;
         try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL,userDB,passDB);
            PreparedStatement stmt= con.prepareStatement("select UserTBL.tien from UserTBL where uid='"+uid+"'");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                tien= rs.getInt(1);
            }
            if(tien>tienthue){
                return true;
            }
            con.close();
        } catch (Exception ex) {
            Logger.getLogger(UserTBLDB.class.getName()).log(Level.SEVERE, null, ex);
        }
           return false;
    }
    public boolean subMoney(int uid, int tientru) {
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            PreparedStatement stmt = con.prepareStatement("Update UserTBL SET tien -= " + tientru + " FROM UserTBL where uid = ?");
            stmt.setInt(1,uid);
            int rc = stmt.executeUpdate();
            con.close();
            return rc == 1;
        } catch (Exception e) {
            Logger.getLogger(UserTBLDB.class.getName()).log(Level.SEVERE, null, e);
        }
        return true;
    }

    public boolean addMoney(int uid, String sotaikhoan, int sotien, String mapin, String phuongthuc) {
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            PreparedStatement stmt = con.prepareStatement("Insert into LognoptienTBL(uid,sotaikhoan,sotien,mapin,phuongthuc,ngaynop) values(?,?,?,?,?,?)");
            stmt.setInt(1, uid);
            stmt.setString(2, sotaikhoan);
            stmt.setInt(3, sotien);
            stmt.setString(4, mapin);
            stmt.setString(5, phuongthuc);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date = simpleDateFormat.format(new java.util.Date());
            System.out.println(date);
            stmt.setString(6, date);
            stmt.execute();
            PreparedStatement stm = con.prepareStatement("Update UserTBL SET tien += " + sotien + " FROM UserTBL where uid = ?");
            stm.setInt(1, uid);
            stm.executeUpdate();
            con.close();
        } catch (Exception ex) {
            Logger.getLogger(LognoptienTBL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }
       public boolean subMoneyorder(int uid, double tientru) {
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            PreparedStatement stmt = con.prepareStatement("Update UserTBL SET tien -= " + tientru + " FROM UserTBL where uid = ?");
            stmt.setInt(1,uid);
            int rc = stmt.executeUpdate();
            con.close();
            return rc == 1;
        } catch (Exception e) {
            Logger.getLogger(UserTBLDB.class.getName()).log(Level.SEVERE, null, e);
        }
        return true;
    }

    public static void main(String[] args) {
        LognoptienTBL log = new LognoptienTBL();
//        System.out.println(log.checksotien(1, 400000));
//        log.addMoney(1,"123", 50000,"1234", "theATM");
    }
    }
    
