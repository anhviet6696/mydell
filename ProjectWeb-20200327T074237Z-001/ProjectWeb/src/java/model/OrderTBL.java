/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import static model.Databaseinfo.dbURL;
import static model.Databaseinfo.driverName;
import static model.Databaseinfo.passDB;
import static model.Databaseinfo.userDB;

/**
 *
 * @author letan
 */
public class OrderTBL extends CarTBL implements Databaseinfo {

    private int oid;
    private int Hirecarid;
    private int tiencoc;
    private String ngaynhan;
    private String ngaytra;
    private String ghichu;
    private double tongtien;
    private double tienchuxe;
    private double fee;
    private String statusorder;

    public OrderTBL() {
    }

    public OrderTBL(int tongtien, String statusorder) {
        this.tongtien = tongtien;
        this.statusorder = statusorder;
    }

    public OrderTBL(String statusorder, int Hirecarid) {
        this.Hirecarid = Hirecarid;
        this.statusorder = statusorder;
    }

    public OrderTBL(int Hirecarid, double tongtien) {
        this.Hirecarid = Hirecarid;
        this.tongtien = tongtien;
    }

    public OrderTBL(int Hirecarid, double tongtien, double tienchuxe, double fee) {
        this.Hirecarid = Hirecarid;
        this.tongtien = tongtien;
        this.tienchuxe = tienchuxe;
        this.fee = fee;
    }

    public OrderTBL(int uid, String name, String phonenb, String bienso, String ngaynhan, String ngaytra, int tongtien) {
        super(uid, name, phonenb, bienso);
        this.ngaynhan = ngaynhan;
        this.ngaytra = ngaytra;
        this.tongtien = tongtien;
    }

    public String getStatusorder() {
        return statusorder;
    }

    public void setStatusorder(String statusorder) {
        this.statusorder = statusorder;
    }

//    public OrderTBL(int uid,int phonenb,int,int oid, int Hirecarid, Date startrent, Date endrent, int tiencoc, Date ngaynhan, Date ngaytra, String ghichu, int tongtien) {
//        
//        super(uid);
//        this.oid = oid;
//        this.Hirecarid = Hirecarid;
//        this.startrent = startrent;
//        this.endrent = endrent;
//        this.tiencoc = tiencoc;
//        this.ngaynhan = ngaynhan;
//        this.ngaytra = ngaytra;
//        this.ghichu = ghichu;
//        this.tongtien = tongtien;
//    }
    public int getOid() {
        return oid;
    }

    public void setOid(int oid) {
        this.oid = oid;
    }

    public int getHirecarid() {
        return Hirecarid;
    }

    public void setHirecarid(int Hirecarid) {
        this.Hirecarid = Hirecarid;
    }

    public int getTiencoc() {
        return tiencoc;
    }

    public void setTiencoc(int tiencoc) {
        this.tiencoc = tiencoc;
    }

    public String getNgaynhan() {
        return ngaynhan;
    }

    public void setNgaynhan(String ngaynhan) {
        this.ngaynhan = ngaynhan;
    }

    public String getNgaytra() {
        return ngaytra;
    }

    public void setNgaytra(String ngaytra) {
        this.ngaytra = ngaytra;
    }

    public String getGhichu() {
        return ghichu;
    }

    public void setGhichu(String ghichu) {
        this.ghichu = ghichu;
    }

    public void setFee(int fee) {
        this.fee = fee;
    }

    public double getTongtien() {
        return tongtien;
    }

    public void setTongtien(double tongtien) {
        this.tongtien = tongtien;
    }

    public double getTienchuxe() {
        return tienchuxe;
    }

    public void setTienchuxe(double tienchuxe) {
        this.tienchuxe = tienchuxe;
    }

    public double getFee() {
        return fee;
    }

    public void setFee(double fee) {
        this.fee = fee;
    }

    public void setTienchuxe(int tienchuxe) {
        this.tienchuxe = tienchuxe;
    }

    public OrderTBL(String statusorder) {
        this.statusorder = statusorder;
    }

    @Override
    public String toString() {
        return "OrderTBL{" + "statusorder=" + statusorder + '}';
    }

    public ArrayList<OrderTBL> checksttOrder(int id) { // userID
        ArrayList<OrderTBL> c = new ArrayList<>();
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select status,Hirecarid from OrderTBL where uid='" + id + "'");
            while (rs.next()) {
                c.add(new OrderTBL(rs.getString(1), rs.getInt(2)));
            }
            con.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return c;
    }

    public ArrayList<OrderTBL> getinfomationOrder(int id) { // userID
        ArrayList<OrderTBL> c = new ArrayList<>();
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select HirecarTBL.Hirecarid,OrderTBL.tongtien from HirecarTBL inner join OrderTBL on HirecarTBL.Hirecarid = OrderTBL.Hirecarid where HirecarTBL.Hirecarid in (select OrderTBL.Hirecarid from OrderTBL where uid='" + id + "')");
            while (rs.next()) {
                c.add(new OrderTBL(this.Hirecarid = rs.getInt(1), this.tongtien = rs.getDouble(2)));
            }
            con.close();
            return c;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
        public ArrayList<OrderTBL> getOrderList() {
        ArrayList<OrderTBL> c = new ArrayList<>();
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(" Select  UserTBL.uid ,UserTBL.fname,UserTBL.phonenb,HirecarTBL.bienso,ngaynhan,ngaytra,tongtien from OrderTBL inner join HirecarTBL on OrderTBL.hirecarid=HirecarTBL.hirecarid\n"
                    + " Inner join UserTBL on OrderTBL.uid=UserTBL.uid");
            while (rs.next()) {
                c.add(new OrderTBL(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7)));
            }
            con.close();
            return c;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
        OrderTBL od = new OrderTBL();
        ArrayList<OrderTBL> arr = new ArrayList<>();
        arr = od.checksttOrder(2);
        for (int i = 0; i < arr.size(); i++) {
            System.out.println("1" + arr.get(i).getStatusorder());
        }
    }

}
