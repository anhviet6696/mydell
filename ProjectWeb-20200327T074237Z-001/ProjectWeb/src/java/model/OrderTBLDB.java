/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import static model.Databaseinfo.dbURL;
import static model.Databaseinfo.driverName;
import static model.Databaseinfo.passDB;
import static model.Databaseinfo.userDB;

/**
 *
 * @author letan
 */
public class OrderTBLDB extends OrderTBL implements Databaseinfo {

    public boolean addOrder(int uid, int Hirecarid, double tongtien, double tienchuxe, double fee) {
        String hirecarStt = "Order";
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            PreparedStatement stmt = con.prepareStatement("Insert into OrderTBL(uid,Hirecarid,tongtien,tienchuxe,fee) values(?,?,?,?,?)");
            stmt.setInt(1, uid);
            stmt.setInt(2, Hirecarid);
            stmt.setDouble(3, tongtien);
            stmt.setDouble(4, tienchuxe);
            stmt.setDouble(5, fee);
            stmt.executeUpdate();
            PreparedStatement stm = con.prepareStatement("Update HirecarTBL set hirecarStt='Order' where Hirecarid='" + Hirecarid + "'");
            stm.executeUpdate();
            con.close();

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public int checkuid(int uid){
        int id=0;
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(" Select  OrderTBL.uid from OrderTBL");
            while (rs.next()) {
               id=rs.getInt(1);
               break;
            }
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }

public ArrayList<UserTBL> getUserLichSuThue(int uid) { // hirecarID
        ArrayList<UserTBL> c = new ArrayList<>();
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);

            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select UserTBL.fname,UserTBL.phonenb from UserTBL where uid in\n"
                    + " (select CarTBL.uid from HirecarTBL inner join CarTBL on HirecarTBL.bienso=CarTBL.bienso  where HirecarTBL.bienso \n"
                    + " in (select HirecarTBL.bienso from HirecarTBL inner join OrderTBL on HirecarTBL.Hirecarid = OrderTBL.Hirecarid where HirecarTBL.Hirecarid \n"
                    + "	in (select OrderTBL.Hirecarid from OrderTBL where uid='" + uid + "')))");
            while (rs.next()) {
                c.add(new UserTBL(rs.getString(1), rs.getString(2)));
            }
            con.close();
            return c;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
public ArrayList<UserTBL> getUserChoThue(int uid) { // hirecarID
        ArrayList<UserTBL> c = new ArrayList<>();
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);

            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select UserTBL.fname,UserTBL.phonenb from UserTBL where uid in\n"
                    + " (select CarTBL.uid from HirecarTBL inner join CarTBL on HirecarTBL.bienso=CarTBL.bienso  where HirecarTBL.bienso \n"
                    + " in (select HirecarTBL.bienso from HirecarTBL inner join OrderTBL on HirecarTBL.Hirecarid = OrderTBL.Hirecarid where HirecarTBL.Hirecarid \n"
                    + "	in (select OrderTBL.Hirecarid from OrderTBL where uid='" + uid + "')))");
            while (rs.next()) {
                c.add(new UserTBL(rs.getString(1), rs.getString(2)));
            }
            con.close();
            return c;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
  
   

    //-------------------------------------------------------
    public ArrayList<HirecarTBL> getCarLichSuThue(int id) { // hirecarID
        ArrayList<HirecarTBL> c = new ArrayList<>();
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select HirecarTBL.bienso,carTBL.hangxe,HirecarTBL.location,HirecarTBL.startrent,HirecarTBL.endrent from HirecarTBL inner join CarTBL on HirecarTBL.bienso=CarTBL.bienso  where HirecarTBL.bienso \n"
                    + "in (select HirecarTBL.bienso from HirecarTBL inner join OrderTBL on HirecarTBL.Hirecarid = OrderTBL.Hirecarid where HirecarTBL.Hirecarid in (select OrderTBL.Hirecarid from OrderTBL where uid='"+id+"'))");
            while (rs.next()) {
                c.add(new HirecarTBL(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5)));
            }
            con.close();
            return c;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    //-------------------------------------------------------
    public ArrayList<OrderTBL> getOrderLichSuThue(int id) { // userID
        ArrayList<OrderTBL> c = new ArrayList<>();
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select HirecarTBL.Hirecarid,OrderTBL.tongtien from HirecarTBL inner join OrderTBL on HirecarTBL.Hirecarid = OrderTBL.Hirecarid where HirecarTBL.Hirecarid in (select OrderTBL.Hirecarid from OrderTBL where uid='"+id+"')");
            while (rs.next()) {
                c.add(new OrderTBL(rs.getInt(1),rs.getDouble(2)));
            }
            con.close();
            return c;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //-------------------------------------------------------
    public ArrayList<UserTBL> getUserLichSuChoThue(int uid) { // userID
        ArrayList<UserTBL> c = new ArrayList<>();
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select UserTBL.fname,UserTBL.phonenb from UserTBL inner join OrderTBL on USerTBL.uid=OrderTBL.uid where UserTBL.uid='" + uid + "'");
            while (rs.next()) {
                c.add(new UserTBL(rs.getString(1), rs.getString(2)));
            }
            con.close();
            return c;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //-------------------------------------------------------
    public ArrayList<OrderTBL> getOrderLichSuChoThue(int id) { // hirecarID
        ArrayList<OrderTBL> c = new ArrayList<>();
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select OrderTBL.tienchuxe,OrderTBL.status from HirecarTBL inner join OrderTBL on HirecarTBL.Hirecarid = OrderTBL.Hirecarid where HirecarTBL.Hirecarid='" + id + "'");
            while (rs.next()) {
                c.add(new OrderTBL(rs.getInt(1), rs.getString(2)));
            }
            con.close();
            return c;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public static void main(String[] args) {
    }
}
