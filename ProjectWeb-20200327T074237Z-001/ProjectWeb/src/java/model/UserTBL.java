
package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;



public class UserTBL implements Databaseinfo{
    private int uid;
    private String sotaikhoan;
    private String phonenb;
    private String pass;
    private String fname;
    private String email;
    private String address;
    private String lock;
    private int tien;
    private int userRight;
public UserTBL(int uid, String sotaikhoan, String phonenb, String fname, String email, String address, String lock, int tien, int userRight) {
        this.uid = uid;
        this.sotaikhoan = sotaikhoan;
        this.phonenb = phonenb;
        this.fname = fname;
        this.email = email;
        this.address = address;
        this.lock = lock;
        this.tien = tien;
        this.userRight = userRight;
    }
 public UserTBL(int uid, String sotaikhoan, String phonenb, String pass, String fname, String email, String address, String lock, int tien, int userRight) {
        this.uid = uid;
        this.sotaikhoan = sotaikhoan;
        this.phonenb = phonenb;
        this.pass = pass;
        this.fname = fname;
        this.email = email;
        this.address = address;
        this.lock = lock;
        this.tien = tien;
        this.userRight = userRight;
    }

    public UserTBL(int uid, String sotaikhoan, String phonenb, String pass, String fname, String email, String address, String lock, int tien) {
        this.uid = uid;
        this.sotaikhoan = sotaikhoan;
        this.phonenb = phonenb;
        this.pass = pass;
        this.fname = fname;
        this.email = email;
        this.address = address;
        this.lock = lock;
        this.tien = tien;
    }
    
public UserTBL(int uid, String name, String phone) {
        this.uid = uid;
        this.fname = name;
        this.phonenb = phone;
    }
    public UserTBL(String phonenb, String fname, String email) {
        this.phonenb = phonenb;
        this.fname = fname;
        this.email = email;
    }

    public UserTBL(int uid, String phonenb, String fname, String address) {
        this.uid = uid;
        this.phonenb = phonenb;
        this.fname = fname;
        this.address = address;
    }




   public UserTBL(String phone) {
       UserTBLDB st = new UserTBLDB();
       UserTBL s = st.getUser(st.getuid(phone));
       UserTBL s2 = st.getUserRight(st.getuid(phone));
       if(s==null){
           
       }
        this.uid = s.uid;
        this.sotaikhoan = s.sotaikhoan;
        this.phonenb = s.phonenb;
        this.pass = s.pass;
        this.fname = s.fname;
        this.email = s.email;
        this.address = s.address;
        this.lock = s.lock;
        this.userRight = s2.userRight;
    }
    public UserTBL() {
    }

    public int getUid() {
        return uid;
    }
public UserTBL(String fname, String phone) {
        this.fname = fname;
        this.phonenb = phone;
    }
    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getSotaikhoan() {
        return sotaikhoan;
    }

    public void setSotaikhoan(String sotaikhoan) {
        this.sotaikhoan = sotaikhoan;
    }

    public String getPhonenb() {
        return phonenb;
    }

    public void setPhonenb(String phonenb) {
        this.phonenb = phonenb;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLock() {
        return lock;
    }

    public void setLock(String lock) {
        this.lock = lock;
    }

    public int getUserRight() {
        return userRight;
    }

    public void setUserRight(int userRight) {
        this.userRight = userRight;
    }


    public int getTien() {
        return tien;
    }

    public void setTien(int tien) {
        this.tien = tien;
    }

    @Override
    public String toString() {
        return "UserTBL{" + "uid=" + uid + ", sotaikhoan=" + sotaikhoan + ", phonenb=" + phonenb + ", pass=" + pass + ", fname=" + fname + ", email=" + email + ", address=" + address + ", lock=" + lock + ", tien=" + tien + '}';
    }

 
    //==============================================
     public UserTBL login(String phone, String pass){
      UserTBL s = new UserTBL(phone);
      if(s.pass.equals(pass)) return s;
      return null;
    }
     //================================================
         public   boolean changePass(String oldP, String newP){
        if(pass.equals(oldP)){
            pass=newP;
            UserTBLDB s= new UserTBLDB();
            return s.updateUser(this);
        }
        return false;
    }
         //=========================
public boolean changePhoneNb(String oldphoneNB,String newphoneNB){
        if(phonenb.equals(oldphoneNB)){
            email=newphoneNB;
            UserTBLDB s= new UserTBLDB();
            return s.updateUser(this);
        }
        return false;
    }
public boolean updateTien(int sotien){
             UserTBLDB user = new UserTBLDB();
             this.tien += sotien;
             user.updateUser(this);
             return true;
         }

    public static void main(String[] args) {
        UserTBL st = new UserTBL();
        System.out.println(st.login("123","123"));
    }
    
}
