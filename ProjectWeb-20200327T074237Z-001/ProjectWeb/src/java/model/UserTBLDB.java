/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Databaseinfo;
import model.UserTBL;

/**
 *
 * @author letan
 */
public class UserTBLDB implements  Databaseinfo{
    public  boolean addNewUser(String phonenb, String pass,String fullname, String email, String address){
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL,userDB,passDB);
            PreparedStatement stmt= con.prepareStatement("Insert into UserTBL(phonenb,pass,fname,email,address) values('"+phonenb+"','"+pass+"',N'"+fullname+"','"+email+"','"+address+"')");
            stmt.executeUpdate();
            con.close();
        } catch (Exception ex) {
            Logger.getLogger(UserTBLDB.class.getName()).log(Level.SEVERE, null, ex);
          return false;
        }
           return true;
    }
        public  boolean updateUser(UserTBL s){
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL,userDB,passDB);
            PreparedStatement stmt= con.prepareStatement("Update UserTBL set phonenb=?,fname=?,pass=?,email=?,address=?, lock=?,sotaikhoan=? where uid=?");
            stmt.setString(1, s.getPhonenb());
            stmt.setString(2, s.getFname());
            stmt.setString(3, s.getPass());
            stmt.setString(4, s.getEmail());
            stmt.setString(5, s.getAddress());
            stmt.setString(6, s.getLock()); 
            stmt.setString(7, s.getSotaikhoan()); 
            stmt.setInt(8, s.getUid());
            int rc=stmt.executeUpdate();
            con.close();
            return rc==1;
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return false;
    }
public static UserTBL getUser(int uid) {
        UserTBL s = null;
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("Select sotaikhoan,phonenb,fname,pass,email,address,lock,tien,userRight from UserTBL where uid='" + uid + "'");
            if (rs.next()) {
                String sotaikhoan = rs.getString(1);
                String phonenb = rs.getString(2);
                String fname = rs.getString(3);
                String pass = rs.getString(4);
                String email = rs.getString(5);
                String address = rs.getString(6);
                String lock = rs.getString(7);
                int tien = rs.getInt(8);
                int userRight = rs.getInt(9);
                s = new UserTBL(uid, sotaikhoan, phonenb, pass, fname, email, address, lock, tien, userRight);
            }
            con.close();
            return s;
        } catch (Exception ex) {
            Logger.getLogger(UserTBLDB.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
     public ArrayList<UserTBL> listAll() {
        ArrayList<UserTBL> v = new ArrayList<UserTBL>();

        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            PreparedStatement stmt = con.prepareStatement("Select uid,tien,sotaikhoan,phonenb,fname,email,address,lock,userRight from UserTBL");
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                int uid = rs.getInt(1);
                int tien = rs.getInt(2);
                String sotaikhoan = rs.getString(3);
                String phonenb = rs.getString(4);
                String fname = rs.getString(5);
                String email = rs.getString(6);
                String address = rs.getString(7);
                String lock = rs.getString(8);
                int userRight = rs.getInt(9);
                v.add(new UserTBL(uid, sotaikhoan, phonenb, fname, email, address, lock, tien, userRight));
            }
            con.close();
        } catch (Exception ex) {
            Logger.getLogger(UserTBL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return v;
    }

    public boolean updateUnLock(int uid) {
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            PreparedStatement stmt = con.prepareStatement("Update UserTBL set lock='Unlock' where uid='" + uid + "'");
            while (true) {
                int rc = stmt.executeUpdate();
                con.close();
                return rc == 1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
public UserTBL getUserRight(int uid){
         UserTBL s=null;
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL,userDB,passDB);
            Statement stmt= con.createStatement();
            ResultSet rs=stmt.executeQuery("Select sotaikhoan,phonenb,fname,pass,email,address,lock,tien,userRight from UserTBL where uid='"+uid+"'");
            if(rs.next()){
                String sotaikhoan = rs.getString(1);
                String phonenb = rs.getString(2);
                String fname=rs.getString(3);
                String pass=rs.getString(4);
                String email=rs.getString(5);
                String address=rs.getString(6);
                String lock=rs.getString(7);
                int tien=rs.getInt(8);
                int userRight = rs.getInt(9);
         
                s = new UserTBL(uid, sotaikhoan, phonenb, pass, fname, email, address, lock,tien,userRight);
            }
            con.close();
            return s;
        } catch (Exception ex) {
            Logger.getLogger(UserTBLDB.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    public boolean updateLock(int uid) {
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            PreparedStatement stmt = con.prepareStatement("Update UserTBL set lock='Lock' where uid='" + uid + "'");
            while (true) {
                int rc = stmt.executeUpdate();
                con.close();
                return rc == 1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
         public  int getuid(String phonenb){
        int uid=0;
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL,userDB,passDB);
            PreparedStatement stmt= con.prepareStatement("select uid from UserTBL where phonenb='"+phonenb+"'");
            ResultSet rs = stmt.executeQuery();
             while(rs.next()){
                 uid = rs.getInt(1);
             }
             con.close();
        } catch (Exception ex) {
            Logger.getLogger(UserTBLDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return uid;
    }
public ArrayList<UserTBL> getAllChuXe(String bienso) {
        ArrayList<UserTBL> c = new ArrayList<>();
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select fname ,phonenb from UserTBL where uid in\n"
                    + "(select CarTBL.uid from HirecarTBL inner join CarTBL on HirecarTBL.bienso=CarTBL.bienso  where HirecarTBL.bienso \n"
                    + "in (select HirecarTBL.bienso from HirecarTBL inner join OrderTBL on HirecarTBL.Hirecarid = OrderTBL.Hirecarid where HirecarTBL.bienso='" + bienso + "'))");
            while (rs.next()) {
                c.add(new UserTBL(rs.getString(1), rs.getString(2)));
            }
            con.close();
            return c;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
         public static void main(String[] args) {
        UserTBLDB us = new UserTBLDB();
//        System.out.println(us.addNewUser("0984535611","123","Lê Tấn Vỹ","abc@abc.com","Hương An"));
        System.out.println(us.getUser(1));
    }
}
