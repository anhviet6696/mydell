

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/includes/header.jsp" %>
<!doctype html>
<html>

    <head>
        <link href="fonts/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="styles/main.css" rel='stylesheet' type='text/css' media="all" />
    <body>
        <h1 class="w3ls">VINOTO Cùng bạn trên mọi hành trình</h1>
        <div class="content-w3ls">
            <div class="content-agile1">
                <h2 class="agileits1">Search</h2>
                <p class="agileits2">Write The Title Of Car You want search</p>
            </div>
            <div class="content-agile2">
                <form action="searchxe" method="POST">
                    <div class="form-control w3layouts">
                        <input type="text" id="Location" name="location" placeholder="Thành Phố ,Quận ,Huyện ,Địa chỉ..." title="Please enter Location" required="">
                    </div>

                    <div class="form-control w3layouts">
                        <input type="text" id="DateStart" name="startrent" placeholder="Start date Ex : 26/10/2019" title="Please enter start date" required="">
                        <select class="TimeStart" name="startrenthour" title="Please Enter Time Start">
                            <option name="startrenthour" value=" 0:00">00:00 </option>
                            <option class ="mauoption" name="startrenthour" value=" 00:30">00:30 </option>
                            <option class ="mauoption" name="startrenthour" value=" 01:00">01:00 </option>
                            <option class ="mauoption" name="startrenthour" value=" 01:30">01:30 </option>
                            <option class ="mauoption" name="startrenthour" value=" 02:00">02:00 </option>
                            <option class ="mauoption" name="startrenthour" value=" 02:30">02:30 </option>
                            <option class ="mauoption" name="startrenthour" value=" 03:00">03:00 </option>
                            <option class ="mauoption" name="startrenthour" value=" 03:30">03:30 </option>
                            <option class ="mauoption" name="startrenthour" value=" 04:00">04:00 </option>
                            <option class ="mauoption" name="startrenthour" value=" 04:30">04:30 </option>
                            <option class ="mauoption" name="startrenthour" value=" 05:00">05:00 </option>
                            <option class ="mauoption" name="startrenthour" value=" 05:30">05:30 </option>
                            <option class ="mauoption" name="startrenthour" value=" 06:00">06:00 </option>
                            <option class ="mauoption" name="startrenthour" value=" 06:30">06:30 </option>
                            <option class ="mauoption" name="startrenthour" value=" 07:00">07:00 </option>
                            <option class ="mauoption" name="startrenthour" value=" 07:30">07:30 </option>
                            <option class ="mauoption" name="startrenthour" value=" 08:00">08:00 </option>
                            <option class ="mauoption" name="startrenthour" value=" 08:30">08:30 </option>
                            <option class ="mauoption" name="startrenthour" value=" 09:00">09:00 </option>
                            <option class ="mauoption" name="startrenthour" value=" 09:30">09:30 </option>
                            <option class ="mauoption" name="startrenthour" value=" 10:00">10:00 </option>
                            <option class ="mauoption" name="startrenthour" value=" 10:30">10:30 </option>
                            <option class ="mauoption" name="startrenthour" value=" 11:00">11:00 </option>
                            <option class ="mauoption" name="startrenthour" value=" 11:30">11:30 </option>
                            <option class ="mauoption" name="startrenthour" value=" 12:00">12:00 </option>
                            <option class ="mauoption" name="startrenthour" value=" 12:30">12:30 </option>
                            <option class ="mauoption" name="startrenthour" value=" 13:00">13:00 </option>
                            <option class ="mauoption" name="startrenthour" value=" 13:30">13:30 </option>
                            <option class ="mauoption" name="startrenthour" value=" 14:00">14:00 </option>
                            <option class ="mauoption" name="startrenthour" value=" 14:30">14:30 </option>
                            <option class ="mauoption" name="startrenthour" value=" 15:00">15:00 </option>
                            <option class ="mauoption" name="startrenthour" value=" 15:30">15:30 </option>
                            <option class ="mauoption" name="startrenthour" value=" 16:00">16:00 </option>
                            <option class ="mauoption" name="startrenthour" value=" 16:30">16:30 </option>
                            <option class ="mauoption" name="startrenthour" value=" 17:00">17:00 </option>
                            <option class ="mauoption" name="startrenthour" value=" 17:30">17:30 </option>
                            <option class ="mauoption" name="startrenthour" value=" 18:00">18:00 </option>
                            <option class ="mauoption" name="startrenthour" value=" 18:30">18:30 </option>
                            <option class ="mauoption" name="startrenthour" value=" 19:00">19:00 </option>
                            <option class ="mauoption" name="startrenthour" value=" 19:30">19:30 </option>
                            <option class ="mauoption" name="startrenthour" value=" 20:00">20:00 </option>
                            <option class ="mauoption" name="startrenthour" value=" 20:30">20:30 </option>
                            <option class ="mauoption" name="startrenthour" value=" 21:00">21:00 </option>
                            <option class ="mauoption" name="startrenthour" value=" 21:30">21:30 </option>
                            <option class ="mauoption" name="startrenthour" value=" 22:00">22:00 </option>
                            <option class ="mauoption" name="startrenthour" value=" 22:30">22:30 </option>
                            <option class ="mauoption" name="startrenthour" value=" 23:00">23:00 </option>
                            <option class ="mauoption" name="startrenthour" value=" 23:30">23:30 </option>             
                        </select>
                    </div>

                    <div class="form-control agileinfo">
                        <input type="text" class="lock" name="endrent" title="Please enter end date" placeholder="End date Ex : 28/10/2019" id="DateEnd" required="">
                        <select class="TimeEnd" name="endrenthour" title="Please Enter Time End">
                            <option name="endrenthour" value=" 0:00">00:00 </option>
                            <option class ="mauoption" name="endrenthour" value=" 00:30">00:30 </option>
                            <option class ="mauoption" name="endrenthour" value=" 01:00">01:00 </option>
                            <option class ="mauoption" name="endrenthour" value=" 01:30">01:30 </option>
                            <option class ="mauoption" name="endrenthour" value=" 02:00">02:00 </option>
                            <option class ="mauoption" name="endrenthour" value=" 02:30">02:30 </option>
                            <option class ="mauoption" name="endrenthour" value=" 03:00">03:00 </option>
                            <option class ="mauoption" name="endrenthour" value=" 03:30">03:30 </option>
                            <option class ="mauoption" name="endrenthour" value=" 04:00">04:00 </option>
                            <option class ="mauoption" name="endrenthour" value=" 04:30">04:30 </option>
                            <option class ="mauoption" name="endrenthour" value=" 05:00">05:00 </option>
                            <option class ="mauoption" name="endrenthour" value=" 05:30">05:30 </option>
                            <option class ="mauoption" name="endrenthour" value=" 06:00">06:00 </option>
                            <option class ="mauoption" name="endrenthour" value=" 06:30">06:30 </option>
                            <option class ="mauoption" name="endrenthour" value=" 07:00">07:00 </option>
                            <option class ="mauoption" name="endrenthour" value=" 07:30">07:30 </option>
                            <option class ="mauoption" name="endrenthour" value=" 08:00">08:00 </option>
                            <option class ="mauoption" name="endrenthour" value=" 08:30">08:30 </option>
                            <option class ="mauoption" name="endrenthour" value=" 09:00">09:00 </option>
                            <option class ="mauoption" name="endrenthour" value=" 09:30">09:30 </option>
                            <option class ="mauoption" name="endrenthour" value=" 10:00">10:00 </option>
                            <option class ="mauoption" name="endrenthour" value=" 10:30">10:30 </option>
                            <option class ="mauoption" name="endrenthour" value=" 11:00">11:00 </option>
                            <option class ="mauoption" name="endrenthour" value=" 11:30">11:30 </option>
                            <option class ="mauoption" name="endrenthour" value=" 12:00">12:00 </option>
                            <option class ="mauoption" name="endrenthour" value=" 12:30">12:30 </option>
                            <option class ="mauoption" name="endrenthour" value=" 13:00">13:00 </option>
                            <option class ="mauoption" name="endrenthour" value=" 13:30">13:30 </option>
                            <option class ="mauoption" name="endrenthour" value=" 14:00">14:00 </option>
                            <option class ="mauoption" name="endrenthour" value=" 14:30">14:30 </option>
                            <option class ="mauoption" name="endrenthour" value=" 15:00">15:00 </option>
                            <option class ="mauoption" name="endrenthour" value=" 15:30">15:30 </option>
                            <option class ="mauoption" name="endrenthour" value=" 16:00">16:00 </option>
                            <option class ="mauoption" name="endrenthour" value=" 16:30">16:30 </option>
                            <option class ="mauoption" name="endrenthour" value=" 17:00">17:00 </option>
                            <option class ="mauoption" name="endrenthour" value=" 17:30">17:30 </option>
                            <option class ="mauoption" name="endrenthour" value=" 18:00">18:00 </option>
                            <option class ="mauoption" name="endrenthour" value=" 18:30">18:30 </option>
                            <option class ="mauoption" name="endrenthour" value=" 19:00">19:00 </option>
                            <option class ="mauoption" name="endrenthour" value=" 19:30">19:30 </option>
                            <option class ="mauoption" name="endrenthour" value=" 20:00">20:00 </option>
                            <option class ="mauoption" name="endrenthour" value=" 20:30">20:30 </option>
                            <option class ="mauoption" name="endrenthour" value=" 21:00">21:00 </option>
                            <option class ="mauoption" name="endrenthour" value=" 21:30">21:30 </option>
                            <option class ="mauoption" name="endrenthour" value=" 22:00">22:00 </option>
                            <option class ="mauoption" name="endrenthour" value=" 22:30">22:30 </option>
                            <option class ="mauoption" name="endrenthour" value=" 23:00">23:00 </option>
                            <option class ="mauoption" name="endrenthour" value=" 23:30">23:30 </option>     
                        </select>
                    </div>

                    <div class="form-control agileinfo">
                        <select id="soCho" name="socho">
                            <option class ="mauoption" name="socho" value="4">4 Chỗ </option>
                            <option class ="mauoption"name="socho" value="7">7 Chỗ</option>             
                        </select>
                    </div>

                    <input type="submit" class="Search" value="Tìm kiếm">
                </form>

                <ul class="social-agileinfo wthree2">
                    <p class="titlebottom">
                        Tìm kiếm lựa chọn địa điểm của bạn
                    </p>
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                </ul>
            </div>
            <div class="clear"></div>
        </div>

        <div id="gioithieu">
            <h2 class="titlehuongdan">HƯỚNG DẪN THUÊ XE</h2>
            <img class="step1" src="images/Step1ForRent.svg" width="300px" height=300px>
            <img class="step2" src="images/Step2ForRent.svg" width="300px" height=300px>
            <img class="step3" src="images/Step3ForRent.svg" width="300px" height=300px>
            <img class="step4" src="images/Step4ForRent.svg" width="300px" height=300px>

            <div class=titleStep>
                <p class="TitleStep1">Đặt xe với Vinoto</p>
                <p class="TitleStep2">Nhận xe hoặc giao xe tận nơi</p>
                <p class="TitleStep3">Trải Nghiệm chuyến đi</p>
                <p class="TitleStep4">Kết thúc Giao dịch</p>
            </div>

        </div>

        <div id="titlegioithieu">
            <h1 class="headtitlegioithieu">ĐÔI LỜI</h1>
            <p class="bodytitlegioithieu">
                Websize của chúng tôi sẽ là một phần mềm trung gian cho bạn có thể an tâm thuê xe hoặc đăng ký cho thuê xe, chúng tôi có đội ngũ chuyên nghiệp để sử lý mọi tình huống xảy ra nhằm đảm bảo tính an toàn và xác thưc cho các giao dịch, Cảm ơn Quý Khách đã
                sử dụng websize của chúng tôi ,Chúc Quý khách thượng lộ bình an.
            </p>
        </div>

        <div id="muondangkyxe">
            <div class="moichaodangky">
                <h1 class="Titlemoichaodangky">Bạn Có Muốn Đăng Ký Thuê Xe</h1>
                <p class="noidungmoichaodangky">Hãy trở thành đối tác của chúng tôi để có cơ hội thêm thu nhập hàng tháng</p>
            </div>

            <div class="ButtonCometoDangkyxe">
                <a href="sub-web/dangkyxechothue.html"><button class="DangKyXe">Đăng Ký Xe</button></a>
                <button class="Thingagain">Tìm Hiểu Thêm</button>
            </div>
        </div>

        <div id="quangcao">
            <div class="textquangcao">
                <h2>Ứng dụng cho điện thoại </h2>
                <p>Tải ngay ứng dụng tại App Store hoặc Google Play</p>
            </div>
            <div class="totalfunc">
                <a class="func1" href="#"><img class="responsive-img" src="images/appstore.png"></a>
                <a class="func2" href="#"><img class="responsive-img" src="images/gg.png"></a>
                <a class="anhDT"><img src="images/anhdt.png"></a>
            </div>
        </div>

    </div>
</body>

</html>
<%@ include file="/includes/footer.jsp" %>
