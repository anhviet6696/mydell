<%@page contentType="text/html" pageEncoding="utf-8" %>
<%@ include file="/includes/header_cus.jsp" %>

<!DOCTYPE html>
<html>
    <link rel="stylesheet" href="styles/Css.css">
    <head>
        <meta charset="utf-8">
    </head>
    <body class="Naptien_body" >
        <div class="content-text">
            <h1 id="h1_naptien" align="center">Nạp Tiền Vào Tài Khoản</h1>
            <div class="formcontrol">
                <form class="formnaptien" action="naptien" method="POST">
                    <label id="label_nap_tien">Phương thức nạp</label><input type="text" name="phuongthuc" value="The ATM" readonly><br>
                    <label id="label_nap_tien">Số tài khoản ngân hàng</label><input type="text" name="sotaikhoan" required><br>
                    <label id="label_nap_tien">Số tiền nạp</label><input type="text" name="sotien" required><br>
                    <label id="label_nap_tien">Mã PIN</label><input type="text" name="mapin" required><br>
                    <input id="Take" type="submit" value="Nạp tiền">
                </form>
            </div>
        </div>

    </body>
</html>
<%@ include file="/includes/footer.jsp" %>