<%@page import="model.CarTBL"%>
<%@page import="model.OrderTBL"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.HirecarTBL"%>
<%@page contentType="text/html" pageEncoding="utf-8" %>
<%@ include file="/includes/header_cus.jsp" %>
<jsp:useBean id="hirecar" class="model.HirecarTBL" scope="session" />
<jsp:setProperty name ="hirecar"  property="*"/>
<!DOCTYPE html>
<html>
    <link rel="stylesheet" href="styles/ordercar.css">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <div id="formthuexe">
            <%HirecarTBL hire = new HirecarTBL();
                String bienso = request.getParameter("bienso");
                ArrayList<HirecarTBL> hirec = new ArrayList<>();
                hirec = hire.getresulthirecarsearch(bienso);
                HttpSession s = request.getSession();
                long dat = (long) s.getAttribute("search");
                CarTBL cartbl = new CarTBL();
                String imgxe = cartbl.getimgxe(bienso);
                for (HirecarTBL car : hirec) {
                    double fee = hire.caculatefee(car.getGiaxechothue(), dat);
                    double money = hire.caculatemoney(car.getGiaxechothue(), dat);
                    double tienchuxe = money - fee;
            %>
            <div class="imgcar"><img src="imgxe/<%= imgxe%>" width="759px" height=300px></div>
            <div class="titlecar">
                <h2 class="tenhangxe"><%=car.getHangxe()%></h2>
                <div class="titlequangcao">
                    <label>Số Tự Động</label>
                    <label>Miễn Phí Giao Xe</label>
                </div>
                <div class="titlethongtin">
                    <p>Đặc Điểm</p>
                    <p class="titlemota">Mô Tả </p>
                    <p class="titletinhnang">Tính Năng</p>
                    <p class="titlegiayto">Giấy Tờ Thuê</p>
                    <p class="titletaisan">Tài Sản Thế Chấp</p>
                    <p class="titledieukhoan">Điều Khoản</p>
                    <p class="titleghichu">Ghi Chú</p>
                </div>

                <div class="dacdiem">
                    <p><img src="images/seat.png" width="18px" height="18px">Số Chỗ :<%=car.getSocho()%> Chỗ</p>
                    <p><img src="images/shift.png" width="18px" height="18px">Chuyền Động : <%=car.getHopso()%></p>
                    <p><img src="images/engine.png" width="18px" height="18px">Nhiên Liệu : <%=car.getLoainguyenlieu()%></p>
                </div>

                <div class="mota"><textarea></textarea></div>
            <div class="tinhnang">
                    <%if (car.getTinhnangxe().contains("BlueTooth")) {
                    %>
                <p><img src="images/bluetooth.png" width="18px" height="18px">Bluetooth</p><%}%>
                        <%if (car.getTinhnangxe().contains("GPRS")) {
                        %>
                <p><img src="images/placeholder.png" width="18px" height="18px">GPRS</p><%}%>
                        <%if (car.getTinhnangxe().contains("Khe Cắm USB")) {
                        %>
                <p><img src="images/usb.png" width="18px" height="18px">Khe Cắm USB</p><%}%>
                        <%if (car.getTinhnangxe().contains("Cửa Trần")) {
                        %>
                <p><img src="images/vehicle.png" width="18px" height="18px">Cửa Trần</p><%}%>
            </div>
            <div class="giaytoxe">
                <p><img src="images/id-card.png" width="18px" height="18px">CMND và GPLX</p>
                <p><img src="images/passport.png" width="18px" height="18px">Hộ Khẩu hoặc Passport</p>
            </div>
            <div class="taisanthuechap">
                <p><img src="images/money.png" width="18px" height="18px">15 tỉ tiền mặt</p>
            </div>
            <div class="dieukhoan">
                <p>
                    1. Chấp nhận Hộ khẩu Passport<br> 2. Tài sản đặt cọc : chỉ nhận tiền cọc .</p>
            </div>
            <div class="ghichu">
                <textarea></textarea>
                </div>
            </div>
            <form action="rentcar" method="POST">
                <div class="titlegiaca">
                    <%String giaxe = String.valueOf(car.getGiaxechothue());%>
                    <div class="giathue"><%=giaxe%>/Day</div>

                    <div class="daystart">
                        <h2 class="titlethoigian">Thời Gian Bắt đầu</h2>
                        <input class="inputday" value="${hirecar.startrent}">
                        <input class="inputtime" value="${hirecar.startrenthour}">
                    </div>

                    <div class="dayend">
                        <h2 class="titlethoigian">Thời Gian Kết Thúc</h2>
                        <input class="inputday" value="${hirecar.endrent}">
                        <input class="inputtime" value="${hirecar.endrenthour}">
                    </div>
                    <div class="diadiem">
                        <p class="titlediadiem">Địa Điểm Nhận Xe</p>
                        <p><%=car.getLocation()%></p>
                    </div>
                    <div class="chitietgia">
                        <p class="dongia">Đơn Giá Thuê :</p>
                        <p>Tiền Cọc :</p>
                        <hr>
                        <p>Tổng phí :</p>
                        <hr>
                        <p>Tổng Tiền :</p>
                    </div>
                    <div class="sotien">
                        <p><%=car.getGiaxechothue()%>/Day</p>
                        <p><%=car.getTiencoc()%></p>
                        <p name="fee" value="<%=fee%>"><%=fee%></p>
                        <p name="tongtien" value="<%=money%>"><%=money%></p>
                        <p name="bienso" value="${hirecar.bienso}"></p>
                        <p name="tienchuxe" value="<%=tienchuxe%>"></p>
                    </div>
                    <button type="submit">Rent Car</button>
                    <%  HttpSession sess = request.getSession();
                            sess.getAttribute("order");
                            OrderTBL oder = new OrderTBL();
                            int hid = hirecar.gethcid(bienso);
                            OrderTBL od = new OrderTBL(hid, money, tienchuxe, fee);
                            sess.setAttribute("order", od);
                            System.out.println(hid);
                        }

                    %>
                    <%  
                        ArrayList<HirecarTBL> chuxe = new ArrayList<>();
                        chuxe = hire.getresultchuxesearch(bienso);
                        for (HirecarTBL boss : chuxe) {
                    %>
                </div>
           </form>
            <div class="chuxe">
                    <h2 class ="lienhe">Liên Hệ Chủ Xe</h2>
                    <img class ="iconuser" src="images/iconuser.png" width="70px" height="70px">
                    <p><%=boss.getFname()%></p>
                    <p><%=boss.getEmail()%></p>
                    <p><%=boss.getPhonenb()%></p>
                </div>
                <%}%>
            
        </div>

    </body>
</html>
<%@ include file="/includes/footer.jsp" %>