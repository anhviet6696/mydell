<%@page contentType="text/html" pageEncoding="utf-8" %>
<%@ include file="/includes/header.jsp" %>

<!DOCTYPE html>
<html>
    <link rel="stylesheet" href="styles/Css.css">
    <head>
        <meta charset="utf-8">
    </head>
    <body class="body_register">
        <div id="register-box">
            <div class="left-box">
                <h1 id="register_h1"> Sign Up</h1>
                <form action="register" method="POST">
                    <input type="text" name="phonenb" placeholder="PhoneNumber" required  />
                    <input type="password" name="pass" placeholder="Password" required  />
                    <input type="text" name="fname" placeholder="FullName" required  />
                    <input type="email" name="email" placeholder="Email" required  />
                    <input type="text" name="address" placeholder="Address" required />
                    <input type="submit" value="Sign Up" />
                </form>
            </div>
            <div class="right-box">
                <span class="signinwith">Sign in with<br />Social Network </span>
                <button class="social face">Log in with Facebook</button>
                <button class="social twitter">Log in with Twitter</button>
                <button class="social google">Log in with Google+</button>


            </div>
            <div class="or">OR</div>
        </div>
    </body>
</html>
<%@ include file="/includes/footer.jsp" %>