<%@page contentType="text/html" pageEncoding="utf-8" %>
<%@ include file="/includes/header_cus.jsp" %>

<!DOCTYPE html>
<html>
    <link rel="stylesheet" href="styles/regiscar.css">
    <link href="fonts/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" />
    <head>
        <meta charset="utf-8">
    </head>
    <body>
      <div class="main">
            <h1>Đăng Ký Xe</h1>
            <form action="registerCar" method="post" enctype="multipart/form-data">
                <div class="lable">
                    <div class="col_1_of_2 span_1_of_2"> <input type="text" class="text" placeholder="Biển Số" name="bienso" required></div>
                    <div class="col_1_of_2 span_1_of_2">
                        <select id="Chosebox" name="socho">
                            <option class ="mauoption" name="socho" value="4">4 Chỗ </option>
                            <option class ="mauoption" name="socho" value="7">7 Chỗ</option>             
                        </select>
                    </div>
                    <div class="col_2_of_2 span_1_of_2"> <input type="number" min="100000" max="5000000" step="50000" class="nblabel1" placeholder="Giá cho thuê/ngày" name="tienchothue" required ></div>
                    <div class="col_2_of_2 span_1_of_2"> <input type="text" class="text" placeholder="Hãng Xe" name="hangxe" required ></div>
                    <div class="col_3_of_2 span_1_of_2">
                        <select id="Chosebox" name="loainguyenlieu">
                            <option class ="mauoption" name="loainguyenlieu" value="Xăng">Xăng </option>
                            <option class ="mauoption" name="loainguyenlieu" value="Dầu">Dầu</option>   
                            <option class ="mauoption" name="loainguyenlieu" value="Điện">Điện</option>          
                        </select>
                    </div>
                    <div class="col_3_of_2 span_1_of_2">
                        <select id="Chosebox" name="hopso">
                            <option class ="mauoption" name="hopso" value="Hộp số tay">Hộp số tay </option>
                            <option class ="mauoption" name="hopso" value="Hộp số tự động">Hộp số tự động</option>   
                            <option class ="mauoption" name="hopso" value="Hộp số ly hợp kép">Hộp số ly hợp kép</option>          
                            <option class ="mauoption" name="hopso" value="Hộp số vô cấp">Hộp số vô cấp</option>          
                        </select>
                    </div>
                    <div class="clear"> </div>
                </div>
                <div class="lable-2">
                    <input type="number" min="1000000" max="50000000" step="500000" class="nblb2" name="tiencoc" placeholder="Tiền đặt cọc" >
                    <input type="text" class="text" name="noiohientai" placeholder="Nơi ở Hiện Tại" required>
                    <input class="titlemota" type="text" class="text" name="mota" placeholder="Mô Tả" required>
                </div>
                <div class="checkbutton">
                    <h2 class="titletinhnang">TÍNH NĂNG XE :</h2>
                    <input type="checkbox" name="tinhnangxe" value="BlueTooth"> BlueTooth<br>
                    <input type="checkbox" name="tinhnangxe" value="GPRS"> GPRS<br>
                    <input type="checkbox" name="tinhnangxe" value="Cửa Trần"> Cửa Trần<br>
                    <input type="checkbox" name="tinhnangxe" value="Khe Cắm USB"> Khe Cắm USB<br>
                </div>

                <div class="buttonAddImages">
                    <div class="Imageplance1"><label>Add car Image</label></div>
                    <input class="buttonadd1" type="file" name="file" />
                    <div class="Imageplance2"><label>Add Cavet Image</label></div>
                    <input class="buttonadd2" type="file" name="file1" />
                </div>

                <h3><input type="checkbox" name="checkagre" value="checkagre" required >Bạn đã đồng ý điều khoản của chúng tôi ! </h3>
                <div class="submit">
                    <input type="submit" value="Register Car"/>
                </div>
                <div class="clear"></div>

            </form>
            <!-----//end-main---->
        </div>
    </body>
</html>
<%@ include file="/includes/footer.jsp" %>