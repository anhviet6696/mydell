<%@page contentType="text/html" pageEncoding="utf-8" %>

<!doctype html>

<html>
    <head>
        <meta charset="utf-8">
        <title>Vin Oto - Niềm tự hào của chúng tôi ! </title>
        <link rel="shortcut icon" href="images/favicon.ico">
        <link rel="stylesheet" href="styles/Css.css">

    </head>
    <% request.setCharacterEncoding("UTF-8");%>
    <body class="body_header">
        <header>
                <div class="Head">
        <div class=logo>
              <a href="Index_Cus.jsp"/>
            <img src="images/logo.png" width="120px" height=45px>
        </div>


        <div class=phoneIcon>
            <img src="images/phoneLogo.png" width="25px" height=25px>
        </div>

        <div class="textNumber">
            <p>0988888888</p>
        </div>


        <div class=email>
            <img src="images/mail.png" width="25px" height=25px>
        </div>

        <div class="textEmail">
            <p>vinOtosupport@gmail.com</p>
        </div>


        <div class=facebook>
            <a href="https://www.facebook.com/ertefsdfc"><img src="https://images.vexels.com/media/users/3/137253/isolated/preview/90dd9f12fdd1eefb8c8976903944c026-facebook-icon-logo-by-vexels.png" width="25px" height=25px></a>
        </div>

        <div class="textFacebook">
            <p>Vin OTO</p>
        </div>

        <div class="Menubar">
            <ul>
                <li>
                    <div class="iconmenu">
                        <a href="#"><img src="images/user.jpg" width="80px" height=50px>
                            <p>${CUSTOMER.fname}</p>
                        </a>
                    </div>
                    <ul class="sub-menu">
                        <li><a href="RegisterCar.jsp">Đăng Ký Xe</a></li>
                        <li><a href="UpStatus.jsp">Đăng Bài</a></li>
                        <li><a href="ManageUpStatus.jsp">Quản Lí Đăng Bài</a></li>
                        <li><a href="history_cus.jsp">Lịch Sử</a></li>
                        <li><a href="Managerentcar.jsp">Quản lí thuê xe</a></li>
                        <li><a href="NapTien.jsp">Nạp Tiền</a></li>
                        <li><a href="RutTien.jsp">Rút Tiền</a></li>
                        <li><a href="Index.jsp">LogOut</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-----//end-head---->
    </div>
</head>


        </header>
    </body>
</html>
