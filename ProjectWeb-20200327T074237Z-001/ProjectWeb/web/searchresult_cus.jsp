<%@page import="java.util.ArrayList"%>
<%@page import="model.HirecarTBL"%>
<%@page contentType="text/html" pageEncoding="utf-8" %>
<%@ include file="/includes/header_cus.jsp" %>
<jsp:useBean id="car" class="model.CarTBL" scope="session" />
<jsp:setProperty name ="car"  property="*"/>
<jsp:useBean id="hirecar" class="model.HirecarTBL" scope="session" />
<jsp:setProperty name ="hirecar"  property="*"/>
<!DOCTYPE html>
<html>
    <link rel="stylesheet" href="styles/search.css">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
       
        <%
            String location = request.getParameter("location");
            String socho = request.getParameter("socho");
            String startrent = request.getParameter("startrent");
            String startrenthour = request.getParameter("startrenthour"); 
            String endrent = request.getParameter("endrent");
            String endrenthour = request.getParameter("endrenthour");
            HirecarTBL c = new HirecarTBL();
            ArrayList<HirecarTBL> arr = new ArrayList<>();
            arr = c.searchCar(startrent, endrent, Integer.parseInt(socho), location);
            if(arr.size()<=0){
                HttpSession s=request.getSession();
                s.setAttribute("mess","Không có kết quả phù hợp với thời gian bạn đang tìm kiếm ! Vui lòng nhập tìm với một khoảng thời gian khác ! ");
                response.sendRedirect("Error.jsp"); 
            }
            for (HirecarTBL s : arr) {
                
%>
        <div id="bigdiv">
            

                <img name="pathxe" value="<%=s.getPathxe()%>" src="images/<%=s.getPathxe()%>" width="550px" height=350px class="ima">
                <a href="searchresult?bienso=<%=s.getBienso()%>"><h1 class="name" name="hangxe" value="<%=s.getHangxe()%>"><%=s.getHangxe()%></h1></a>
                <p class="giatien" name="tienchothue" value="<%=s.getGiaxechothue()%>"><%=s.getGiaxechothue()%>/ngày</p>
                <p class="socho" name="socho" value="<%=s.getSocho()%>">Số Chỗ :&emsp; <mark> <%=s.getSocho()%></mark></p>
                <p class="location" name="location" value="<%=s.getLocation()%>">Địa Điểm : &emsp;<mark><%=s.getLocation()%></mark></p>
                
                <p class="timestart" name="tiencoc" value="<%=s.getTiencoc()%>">Tiền Cọc &emsp; <mark><%=s.getTiencoc()%>K</mark></p>
                <p name="startrent" value="${hirecar.startrent}"></p>
                <p name="endrent" value="${hirecar.endrent}"></p>
                <p name="startrenthour" value="${hirecar.startrenthour}"></p>
                <p name="endrenthour" value="${hirecar.endrenthour}"></p>
                <hr>
            
        </div>
        <%}%>
        <br><br><br>
        <%
         HttpSession s=request.getSession();
         s.getAttribute("search");
         long day=c.caculateday(startrent, endrent);
         session.setAttribute("search",day);
        %>

    </body>
</html>
<%@ include file="/includes/footer.jsp" %>