<%-- 
    Document   : share
    Created on : Mar 3, 2020, 12:03:31 AM
    Author     : tranq
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Your Website Title</title>
        <!-- You can use open graph tags to customize link previews.
        Learn more: https://developers.facebook.com/docs/sharing/webmasters -->
        <meta property="og:url"           content="https://www.your-domain.com/your-page.html" />
        <meta property="og:type"          content="website" />
        <meta property="og:title"         content="Your Website Title" />
        <meta property="og:description"   content="Your description" />
        <meta property="og:image"         content="https://www.your-domain.com/path/image.jpg" />
    </head>
    <body>

        <!-- Load Facebook SDK for JavaScript -->
        <div id="fb-root"></div>
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0"></script>

        <!-- Your like button code -->
        <div class="fb-like" 
             data-href="https://www.your-domain.com/your-page.html" 
             data-layout="standard" 
             data-action="like" 
             data-show-faces="true">
        </div>

    </body>
</html>