package model;

import connection.Databaseinfo;
import static connection.Databaseinfo.dbURL;
import static connection.Databaseinfo.driverName;
import static connection.Databaseinfo.passDB;
import static connection.Databaseinfo.userDB;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tranq
 */
public class Contact implements Databaseinfo, Serializable {

    private String address, email, phone, openHours;

    public Contact() {
    }

    public Contact(String address, String email, String phone, String openHours) {
        this.address = address;
        this.email = email;
        this.phone = phone;
        this.openHours = openHours;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getOpenHours() {
        return openHours;
    }

    public void setOpenHours(String openHours) {
        this.openHours = openHours;
    }

    @Override
    public String toString() {
        return "Contact{" + "address=" + address + ", email=" + email + ", phone=" + phone + ", openHours=" + openHours + '}';
    }

    public ArrayList<Contact> getAllContact() {
        ArrayList<Contact> list = new ArrayList<>();
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select address,email,phone,openHours from Contact");
            while (rs.next()) {
                list.add(new Contact(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4)));
            }
            con.close();
            return list;
        } catch (Exception ex) {
            Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static void main(String[] args) {
        Contact c = new Contact();
        System.out.println(c.getAllContact());
    }
}
