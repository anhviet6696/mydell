
package model;

import connection.Databaseinfo;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tranq
 */
public class Menu implements Databaseinfo,Serializable{
    private String title,content,nameSushi;
    private double price;

    public Menu() {
    }

    public Menu(String title, String content, String nameSushi, double price) {
        this.title = title;
        this.content = content;
        this.nameSushi = nameSushi;
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getNameSushi() {
        return nameSushi;
    }

    public void setNameSushi(String nameSushi) {
        this.nameSushi = nameSushi;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Menu{" + "title=" + title + ", content=" + content + ", nameSushi=" + nameSushi + ", price=" + price + '}';
    }
    public ArrayList<Menu> getAllMenu(){
        ArrayList<Menu> listMenu=new ArrayList<>();
        try {
            Class.forName(driverName);
            Connection con=DriverManager.getConnection(dbURL,userDB,passDB);
            Statement stmt=con.createStatement();
            ResultSet rs=stmt.executeQuery("select title,content,sushiName,price from Menu");
            while(rs.next()){
                listMenu.add(new Menu(rs.getString(1),rs.getString(2),rs.getString(3),rs.getDouble(4)));
            }
            con.close();
            return listMenu;           
        } catch (Exception ex) {
            Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public static void main(String[] args) {
        Menu m=new Menu();
        System.out.println(m.getAllMenu().size());
    }
}
