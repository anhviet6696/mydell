
package model;

import connection.Databaseinfo;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import jdk.nashorn.internal.ir.Statement;

/**
 *
 * @author tranq
 */
public class Sushi implements Databaseinfo,Serializable{
    private String nameSushi,picture;

    public Sushi() {
    }

    public Sushi(String nameSushi, String picture) {
        this.nameSushi = nameSushi;
        this.picture = picture;
    }

    public String getNameSushi() {
        return nameSushi;
    }

    public void setNameSushi(String nameSushi) {
        this.nameSushi = nameSushi;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    @Override
    public String toString() {
        return "Sushi{" + "nameSushi=" + nameSushi + ", picture=" + picture + '}';
    }
    public ArrayList<Sushi> getAllSushi(){
        ArrayList<Sushi> list=new ArrayList<>();
        try {
            Class.forName(driverName);
            Connection con=DriverManager.getConnection(dbURL, userDB, passDB);
            java.sql.Statement stm = con.createStatement();
            ResultSet rs;
            rs = stm.executeQuery("Select sushiName,picture from Sushi");
            while(rs.next()){
                list.add(new Sushi(rs.getString(1),rs.getString(2)));
            }
            con.close();
            return list;
        } catch (Exception ex) {
            Logger.getLogger(Sushi.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public static void main(String[] args) {
        Sushi s=new Sushi();
        System.out.println(s.getAllSushi());
    }
}
