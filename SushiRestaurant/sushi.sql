create database SushiRestaurant
use SushiRestaurant

drop table Sushi
drop table Menu

select * from Sushi
select * from Menu
select * from Contact

create table Sushi (sushiName nvarchar(50) primary key not null,picture nvarchar(max) )
go
create table Menu (title nvarchar(max),content nvarchar(max),price decimal(6,2),
sushiName nvarchar(50) ,
FOREIGN KEY (sushiName) REFERENCES Sushi(sushiName) )

create table Contact(address nvarchar(max),email nvarchar(max),phone char(11),openHours nvarchar(max))

insert into Sushi( sushiName,picture) values (
'Nigiri Sushi','https://www.sashimihome.com/wp-content/uploads/nigiri-sushi-l%C3%A0-g%C3%AC.jpg')
insert into Sushi( sushiName,picture) values (
'Maki Sushi','https://www.sashimihome.com/wp-content/uploads/maki-sushi-l%C3%A0-g%C3%AC.jpg')
insert into Sushi( sushiName,picture) values (
'Uramaki','https://www.sashimihome.com/wp-content/uploads/Uramaki-sushi-sushi-ngh%C4%A9a-l%C3%A0-g%C3%AC.jpg')
insert into Sushi( sushiName,picture) values (
'Gunkan','https://www.sashimihome.com/wp-content/uploads/Gunkan-Sushi.jpg')

insert into Menu (title, content, sushiName,price) values (
'Menu 1', 'Canh kim chi + Cai thao muoi chua + Mi xao tuong den', 'Nigiri Sushi', 15.00)
insert into Menu (title, content, sushiName,price) values (
'Menu 2', 'Canh kim chi + Cai thao muoi chua + Mi xao tuong den', 'Maki Sushi', 20.00)
insert into Menu (title, content, sushiName,price) values (
'Menu 3', 'Canh kim chi + Cai thao muoi chua + Mi xao tuong den', 'Gunkan', 15.00)

insert into Contact (address, email, phone, openHours) values (
'02 Tien Son 9, Hoa Cuong Nam, Hai Chau, Da Nang, Viet Nam','viettqde130047@fpt.edu.vn','01202266696',
'Open 24/7 include holiday')